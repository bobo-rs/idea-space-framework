package consts

const (
	// AccountPrefix 账户前缀
	AccountPrefix   = `is_`
	MacSep          = `-`          // MAC地址分割符，从：替换符更换为-
	HeaderUserAgent = `User-Agent` // 请求头KEY
	SortAsc         = `asc`        // 排序升序
	SortDesc        = `desc`       // 排序降序
)
