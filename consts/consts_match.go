package consts

const (
	ChsDash          = `^[\x{4e00}-\x{9fa5}0-9a-zA-Z\s.,，!?;:\'\"-]+$`                                // 匹配中英文数字以及中英文标点符号
	ChsAlphaNum      = `^[\x{4e00}-\x{9fa5}0-9a-zA-Z]+$`                                              // 汉字、字母、数字
	AlphaNum         = `^[0-9a-zA-Z]+$`                                                               // 字母、数字
	AlphaDash        = `^[0-9a-zA-Z_-]+$`                                                             // 验证某个字段的值是否为字母和数字，下划线_及破折号-，例如
	MatchScanIsEmpty = `sql: no rows in result set`                                                   // 匹配Scan数据是否为空
	MatchSetParams   = `{\d+}`                                                                        // 短信模板参数替换
	MatchPhone       = `^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$` // 手机号
)
