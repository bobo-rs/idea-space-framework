package consts

import "time"

const (
	// CachePrefix 缓存前缀
	CachePrefix = `idea.space.`
)

// 设计空间
const (
	DesignIncBrowseLockId             = `design.inc.browse.id:`             // 设计空间浏览量增量叠加锁定
	CacheDesignTemplateMatchLabelHash = `design.template.match.label.hash:` // 设计模板匹配标签Hash值KEY
)

// JWT TOKEN
const (
	JWTTokenHash             = `jwt.token.hash:`               // JSON Web Tokens 缓存KEY
	LoginAuthExpire          = time.Minute * 30                // 登录授权缓存登录信息效期时间
	LoginAuthToken           = `login.auth.user.token:`        // 登录授权缓存Token：login.auth.user.token:{uid}-{token}
	LoginAdminAuthToken      = `login.admin.auth.user.token:`  // 管理员登陆TOKEN：login.admin.auth.user.token:{token}
	LoginAdminAuthPermMenu   = `login.admin.auth.perm.menu:`   // 登录管理员权限菜单：login.admin.auth.perm.menu:{token}
	LoginAdminAuthPermAction = `login.admin.auth.perm.action:` // 登录管理员操作权限：login.admin.auth.perm.action:{token}
	LoginAdminAuthPermId     = `login.admin.auth.perm.id:`     // 登录管理员权限ID集合：login.admin.auth.perm.id:{token}
)

// 系统业务配置
const (
	SysConfigKey = `sys.config.group.name:` // 系统业务配置缓存 sys.config.group.name:{group}-{name}或配置组sys.config.group.name:{group}
)

// SMS短信服务
const (
	SmsTemplateVarAlias = `sms.template.var.alias:` // 短信模板变量内容缓存
)

// 安全规则
const (
	// SafeSecretPrefix 密钥
	SafeSecretPrefix           = `safe.secret.`           // 安全密钥前缀
	SafeSecretTagIp            = `tag.ip:`                // 安全密钥缓存KEY idea.space.safe.secret.tag.mac:{tag}-{mac}
	SafeSecret1MinuteLockTagIp = `1minute.lock.tag.ip:`   // 同一Mac1分钟内锁定 idea.space.safe.secret.1minute.lock.tag.mac:{tag}-{mac}
	SafeSecret1HourLockTagIp   = `1hour.lock.num.tag.ip:` // 同一Mac1小时内刷新数量 idea.space.safe.secret.1hour.lock.tag.mac:{tag}-{mac}
	SafeSecret1MinuteTag       = `1minute.lock.num.tag:`  // 同个tag一分钟刷新数量，锁定半小时 idea.space.safe.secret.1minute.refresh.num.lock.tag：{tag}
	SafeSecret24HourTag        = `24hour.lock.num.tag:`   // 同个tag24小时刷新数量，锁定截止当天0点 idea.space.safe.secret.24hour.lock.tag：{tag}

	// SafeSmsCaptchaPrefix 短信验证码规则
	SafeSmsCaptchaPrefix            = `safe.sms.captcha.`         // 缓存前缀【CachePrefix + SafeSmsCaptchaPrefix】
	SafeSmsCaptchaSendMobile        = `send.mobile:`              // 短信验证码发送
	SafeSmsCaptcha1MinuteLockMobile = `mobile.1minute.send.lock:` // idea.space.safe.sms.captcha.mobile.1minute.send.lock:{mobile} // 同一手机号锁定一分钟
	SafeSmaCaptcha1MinuteLockIp     = `ip.1minute.send.lock:`     // 同一Mac锁定一分钟 idea.space.safe.sms.captcha.mac.1minute.send.lock:{mac}
	SafeSmsCaptchaHalfHourNumIp     = `ip.half.hour.send.num:`    // 同一Mac半小时发送数量
	SafeSmsCaptcha1HourNumMobile    = `mobile.1hour.send.num:`    // 同一手机号一小时发送数量 idea.space.safe.sms.captcha.mobile.1hour.send.num:{mobile}
	SafeSmsCaptcha1MinuterNum       = `1minute.send.num`          // 一分钟发送数量
	SafeSmsCaptcha1HourNum          = `1hour.send.num`            // 1小时发送数量
	SafeSmsCaptcha24HourNum         = `24hour.send.num`           // 24小时发送数量

	// SafeLoginPrefix 账户登录安全规则
	SafeLoginPrefix    = `safe.user.login.` // 账户安全缓存前缀
	SafeLoginIp        = `ip:`              // 用户登录MAC地址 mac:{mac}
	SafeLoginAccountIp = `mac.account:`     // 用户账户登录key mac.account:{account}-{mac}

	// 用户找回密码-安全规则
	SafeForgotPwdIp      = `safe.user.forgot.pwd.ip:`             // 用户找回密码：safe.user.forgot.pwd.mac:{ip}
	SafeForgotPwdCaptcha = `safe.user.forgot.pwd.captcha.mobile:` // 找回密码-验证短信验证码：safe.user.forgot.pwd.captcha.mobile:{mobile}

	// 用户修改密码-安全规则
	SafeUpdatePwdCaptcha = `safe.user.update.pwd.captcha.mobile:` // 修改密码-验证短信验证码：safe.user.update.pwd.captcha.mobile:{mobile}
)
