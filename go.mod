module gitee.com/bobo-rs/idea-space-framework

go 1.22.2

require (
	github.com/disintegration/imaging v1.6.2
	github.com/gogf/gf/contrib/drivers/mysql/v2 v2.7.1
	github.com/gogf/gf/contrib/nosql/redis/v2 v2.7.1
	github.com/gogf/gf/v2 v2.7.1
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/icza/gox v0.0.0-20230924165045-adcb03233bb5
	github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common v1.0.917
	github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/sms v1.0.917
	go.opentelemetry.io/otel/sdk v1.26.0
	golang.org/x/crypto v0.23.0
	golang.org/x/image v0.15.0
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/clbanning/mxj/v2 v2.7.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/fatih/color v1.17.0 // indirect
	github.com/fsnotify/fsnotify v1.7.0 // indirect
	github.com/go-logr/logr v1.4.1 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/go-sql-driver/mysql v1.8.1 // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/grokify/html-strip-tags-go v0.1.0 // indirect
	github.com/magiconair/properties v1.8.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/redis/go-redis/v9 v9.5.1 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	go.opentelemetry.io/otel v1.26.0 // indirect
	go.opentelemetry.io/otel/metric v1.26.0 // indirect
	go.opentelemetry.io/otel/trace v1.26.0 // indirect
	golang.org/x/net v0.25.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
