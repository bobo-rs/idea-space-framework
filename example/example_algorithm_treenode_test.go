package example

import (
	"encoding/json"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/pkg/algorithm/treenode"
	"gitee.com/bobo-rs/idea-space-framework/pkg/utils"
	"testing"
)

func TestTreeNode(t *testing.T) {
	data := []model.PermissionsTreeItem{
		{Id: 1, Pid: 0, Name: ``},
		{Id: 2, Pid: 0, Name: ``},
		{Id: 3, Pid: 0, Name: ``},
		{Id: 4, Pid: 1, Name: ``},
		{Id: 5, Pid: 1, Name: ``},
		{Id: 6, Pid: 1, Name: ``},
		{Id: 7, Pid: 2, Name: ``},
		{Id: 8, Pid: 3, Name: ``},
		{Id: 9, Pid: 4, Name: ``},
		{Id: 10, Pid: 0, Name: ``},
		{Id: 11, Pid: 9, Name: ``},
		{Id: 12, Pid: 9, Name: ``},
	}
	nodeTreeHandler := treenode.New()
	tree, err := nodeTreeHandler.BuildTree(data)
	//fmt.Println(tree, err)
	r, err := json.Marshal(tree)
	fmt.Println(string(r), err)
	// 生成树形结构并查找已勾选的数据
	tree, err = nodeTreeHandler.BuildFindTreeNode(
		data, []uint{1, 5, 6, 9},
	)
	r, err = json.Marshal(tree)
	fmt.Println(string(r), err)

	tree, err = nodeTreeHandler.BuildFindTreeNode(
		data, []uint{1, 5, 6, 9, 10},
	)
	r, err = json.Marshal(tree)
	fmt.Println(string(r), err)
}

func TestGetIp(t *testing.T) {
	//defer func() {
	//	if recover() != nil {
	//		fmt.Println("错了", recover())
	//	}
	//}()
	//ip := ghttp.RequestFromCtx(Ctx).GetClientIp()
	fmt.Println(utils.GetIp(Ctx))
	fmt.Println(`哈哈没有问题`)
}
