package example

import (
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/services/sms"
	"gitee.com/bobo-rs/idea-space-framework/pkg/sms/models"
	_ "github.com/gogf/gf/contrib/drivers/mysql/v2"
	"testing"
)

func TestSMS_SendCode(t *testing.T) {
	service, err := sms.New().Service()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(service.Send([]string{`13456789012`}, models.SendSmsInput{
		TemplateId:     `123123`,
		TemplateParams: []string{`345566`},
	}))
	sms.New().AfterCaptchaSendAsync(Ctx, `13456789012`)
}
