package example

import (
	"encoding/json"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/services/safety"
	"testing"
)

func TestAccountLogin(t *testing.T) {

	safe, err := service.Common().GetSign(Ctx, enums.SafeTagsLogin)
	if err != nil {
		fmt.Println(err)
		return
	}
	param := model.UserOauthAccountLoginItem{
		Account: `is_hhkhs-345`,
		Pwd:     safety.New().AESHandler(safe.Secret).MustEncryptString([]byte(`a1234567`)),
	}
	fmt.Println(service.User().Login(Ctx, model.UserOauthLoginInput{
		UserOauthAccountLoginItem: param,
		Oauth:                     enums.UserOauthAccount,
		UserOauthLoginSignItem: model.UserOauthLoginSignItem{
			Sign: safe.Sign,
		},
	}))
}

func TestMobileLogin_Captcha(t *testing.T) {
	safe, err := service.Common().GetSign(Ctx, enums.SafeTagsLogin)
	if err != nil {
		fmt.Println(err)
		return
	}
	mobile := safety.New().AESHandler(safe.Secret).MustEncryptString([]byte(`13456789012`))
	param := model.UserOauthSmsLoginItem{
		UserOauthSendSmsCaptchaItem: model.UserOauthSendSmsCaptchaItem{
			Mobile: mobile,
		},
		Code: `775594`,
	}
	fmt.Println(service.User().Login(Ctx, model.UserOauthLoginInput{
		UserOauthSmsLoginItem: param,
		Oauth:                 enums.UserOauthSmsCaptcha,
		UserOauthLoginSignItem: model.UserOauthLoginSignItem{
			Sign: safe.Sign,
		},
	}))
}

func TestSendMobile_Captcha(t *testing.T) {

	safe, err := service.Common().GetSign(Ctx, enums.SafeTagsSmsCaptcha)
	if err != nil {
		fmt.Println(err)
		return
	}
	mobile := safety.New().AESHandler(safe.Secret).MustEncryptString([]byte(`13456789012`))
	fmt.Println(service.User().UserLoginSendSmsCaptcha(Ctx, model.UserLoginSendSmsCaptchaInput{
		UserOauthSendSmsCaptchaItem: model.UserOauthSendSmsCaptchaItem{
			Mobile: mobile,
		},
		UserOauthLoginSignItem: model.UserOauthLoginSignItem{
			Sign: safe.Sign,
		},
	}))
	fmt.Println(mobile)
}

func TestGetSign_Login(t *testing.T) {
	safe, err := service.Common().GetSign(Ctx, enums.SafeTagsLogin)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(safe)
	// 加密
	fmt.Println(safety.New().AESHandler(safe.Secret).MustEncryptString([]byte(`a1234567`)))
	fmt.Println(safety.New().AESHandler(`66064e6a5151e88f8c2763beb7c508bc`).DecryptString(`00000000000000000000000000000000ac1081009ae6f189d4be054a90443c56`))
}

func TestCurrUserDeviceList(t *testing.T) {
	r, err := service.User().CurrUserLoginDeviceList(Ctx)
	if err != nil {
		fmt.Println(err)
		return
	}
	marshal, err := json.Marshal(r.Rows)
	if err != nil {
		return
	}
	fmt.Println(string(marshal))
}
