package example

import (
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/services/safety"
	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	"testing"
)

func TestSafety_GetSign(t *testing.T) {
	service := safety.New()
	item, err := service.GetSign(Ctx, enums.SafeTagsLogin)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(item)
	fmt.Println(`第一次验证`)
	fmt.Println(service.Verify(Ctx, item.Sign, enums.SafeTagsLogin))
	fmt.Println(`第二次验证销毁`)
	fmt.Println(service.Verify(Ctx, item.Sign, enums.SafeTagsLogin, true))
}

func TestSafety_Verify(t *testing.T) {
	service := safety.New()
	ciphertext := `0000000000000000000000000000000002a9bd5232e37050ec8cc3ef94b18b66fb151d5e79ddb3ec35b422b3331bfacc60b58a8702808600956045556b54d6e205b2e79e223aa5a4b7be301e1e4358f6ce61b1f489368d0bf50884f014ff19a7ec5fedd7b284bd631431b17444f5c2df9a78cbf4b4b4ade2c6f1444b25166ec4`
	fmt.Println(service.Verify(Ctx, ciphertext, enums.SafeTagsLogin))
}

func TestSafety_GetMacKey(t *testing.T) {
	key, err := safety.New().GetTagMacKey(Ctx, enums.SafeTagsUpload)
	fmt.Println(*key, err)
}
