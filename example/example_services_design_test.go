package example

import (
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/models"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/services/design"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/services/material"
	"github.com/disintegration/imaging"
	"math/rand"
	"testing"
	"time"
)

func TestSMaterial_MaterialDesignOverlays(t *testing.T) {
	underImg, _ := imaging.Open(`E:\www\so\idea-space\uploads\origin-2.png`)
	materials := []models.MaterialDesignOverlayItem{
		{
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			UnderImg:   underImg,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   false,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体00.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#fff`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\3.png`,
					Text:          `12.90`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#fff`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		}, {
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   true,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体01.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#fff`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\3.png`,
					Text:          `12.91`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#fff`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		},
		{
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   true,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体02.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#fff`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\3.png`,
					Text:          `12.92`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#fff`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		},
		{
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   true,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体03.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#fff`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\3.png`,
					Text:          `12.93`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#fff`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		},
		{
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   true,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体04.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#fff`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\3.png`,
					Text:          `12.94`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#fff`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		},
		{
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   true,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体05.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#fff`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\3.png`,
					Text:          `12.95`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#fff`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		},
		{
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   true,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体06.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\material\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: `#b6ff68`,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\material\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: `#ae813d`,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#fff`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\material\3.png`,
					Text:          `12.95`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: `#4cae3d`,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#fff`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		},
		{
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   true,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体07.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\material\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: `#3d96ae`,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\material\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: `#6b3dae`,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#fff`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\material\3.png`,
					Text:          `12.95`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: `#ae3da7`,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#fff`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		},
		{
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   true,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体08.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\material\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: `#d4d24c`,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\material\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: `#ec225b`,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#fff`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\material\3.png`,
					Text:          `12.95`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: `#ec225b`,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#fff`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		},
		{
			TemplateId: 1,
			UnderSrc:   `E:\www\Go\idea-space\uploads\origin-2.png`,
			FontPath:   `E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`,
			MaterialDesignSaveItem: models.MaterialDesignSaveItem{
				IsSave:   true,
				Dir:      `E:\www\Go\idea-space\uploads\design\`,
				Filename: `合成一体09.png`,
			},
			DrawMaterialWords: []models.MaterialWordsItem{
				{
					Src:           `E:\www\Go\idea-space\uploads\material\1.png`,
					Text:          `买2份加送 205g 牛肉酱1瓶`,
					MaterialX:     7,
					MaterialY:     699,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      23,
					TextFontColor: `#e41e2b`,
					TextX:         186,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\material\2.png`,
					Text:          `破损包退 多口味可选`,
					MaterialX:     0,
					MaterialY:     744,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      30,
					TextFontColor: `#174493`,
					TextX:         194,
					TextY:         0,
					TextType:      1,
				}, {
					Src:           `E:\www\Go\idea-space\uploads\material\3.png`,
					Text:          `12.95`,
					MaterialX:     0,
					MaterialY:     698,
					MaterialColor: ``,
					LayerPosition: 0,
					FontSize:      62,
					TextFontColor: `#72575f`,
					TextX:         0,
					TextY:         0,
					TextType:      2,
				},
			},
		},
	}

	fmt.Println(material.New().MaterialDesignOverlays(materials...))
}

func TestSDesign_MatchFindTemplates(t *testing.T) {
	var (
		templates = make([]models.DesignTemplateIdAndNumItem, 0, 1001)
		i         uint
	)

	for i = 1; i <= 1001; i++ {
		templates = append(templates, models.DesignTemplateIdAndNumItem{
			Id:  i,
			Num: i,
		})
	}
	designService := design.New()
	//fmt.Println(design.BinaryFindTemplate(templates, 7))
	//fmt.Println(design.BinaryFindTemplate(templates, 11))
	//fmt.Println(design.BinaryFindTemplate(templates, 14))
	fmt.Println(designService.BinaryFindTemplate(templates, 16))
	//fmt.Println(design.BinaryFindTemplate(templates, 18))
	//fmt.Println(design.BinaryFindTemplate(templates, 28))
	//fmt.Println(design.BinaryFindTemplate(templates, 24))

	fmt.Println(`匹配模板数量`)
	fmt.Println(designService.MatchCutTemplates(templates, 16, 202, 5))
	fmt.Println(designService.MatchSortByTemplates(templates, 16))
}

func TestSDesign_MatchTemplateWordsBySelling(t *testing.T) {
	var (
		words  = make([]models.DesignTemplateWordsItem, 0, 5)
		sell1  = make([]models.DesignTemplateWordsItem, 0, 5)
		sell2  = make([]models.DesignTemplateWordsItem, 0, 3)
		i      uint
		values []string
	)

	rand.NewSource(time.Now().UnixNano())

	for i = 1; i <= 5; i++ {
		item := models.DesignTemplateWordsItem{
			WordsId: i,
			Num:     uint(rand.Intn(20)),
		}
		words = append(words, item)
		sellItem := item
		if item.Num%2 == 0 {
			sellItem.Num += 3
		}

		ti := i
		sellItem.Value = fmt.Sprintf(`第%d页，卖点数%d，文本数%d`, ti, sellItem.Num, item.Num)
		sell1 = append(sell1, sellItem)
		if i <= 3 {
			sell2 = append(sell2, sellItem)
		}
		values = append(values, sellItem.Value)
	}

	designService := design.New()
	fmt.Println(`第一批`, designService.MatchTemplateWordsBySelling(words, sell1))
	fmt.Println(`第二批`, designService.MatchTemplateWordsBySelling(words, sell2))
	fmt.Println(`拆分`, designService.SplitSellingValues(values...))
}
