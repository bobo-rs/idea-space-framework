package example

import (
	"errors"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/pkg/exception"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
	"testing"
)

func TestNewCode(t *testing.T) {
	err := gerror.NewCode(gcode.New(10000, "", nil), "My Error")
	fmt.Println(err.Error())
	fmt.Println(gerror.Code(err))
	fmt.Println(err)

	fmt.Println(gcode.New(1230, `我来了`, g.Map{
		`detail`: `不好玩`,
	}))

	fmt.Println(gerror.NewCode(gcode.New(1000, "hhhhh", nil), `不可能`))
	fmt.Println(gerror.NewCode(gcode.New(-99, `请登录`, nil)))

	err = exception.NewCode(-99, `请登录`)
	fmt.Println(gerror.Code(err).Code(), gerror.Code(err).Message(), err)

	err = errors.New(`请登录`)

	fmt.Println(gerror.Code(err).Code(), gerror.Code(err).Message(), err)

}
