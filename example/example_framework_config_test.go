package example

import (
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"testing"
)

func TestConfig_RemoveCache(t *testing.T) {
	service.Config().RemoveConfigCache(Ctx, `sms_driver`)
}
