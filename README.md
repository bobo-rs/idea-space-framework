# idea-space-framework
创想空间核心版本库

## 安装或更新核心库命令
```
go get -u  github.com/bobo-rs/idea-space-framework@latest
```
## 文件菜单
1. consts：核心常量库、包括各种Regex规则
2. maps：map库，各属性库
3. example：示例目录
4. framework：操作数据库、业务逻辑核心库
```text
goframe命令：
生成Service层：gf gen service
生成Dao层：gf gen dao
目录解析：
dao：数据库操作层，命令管理
logic：业务逻辑层，用于操作数据库和服务层接口
model：数据管理层，各种接口输入输出数据类型定义管理
service：服务接口层，用于调用logic接口，命令管理
validate：自定义验证库
```   
5. hack：goframe框架gf gen 命令配置库[https://goframe.org/pages/viewpage.action?pageId=86187843]
6. pkg：各种工具库、redis操作、JWT操作、design设计库、算法库、格式库等
```text
config：业务文件配置操作库
core：核心操作库扩展库
encrypt：加密库
ifile：文件操作库
ifont：字体库
img：图片处理库，例如合成，裁剪，压缩，旋转等
jwt：JWT二次封装库
sms：第三方短信库
sredis：redis管理
utils：工具库
```
