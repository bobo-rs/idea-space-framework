package utils

// Abs 绝对数值
func Abs(x int) int {
	if x < 0 {
		x = -x
	}
	return x
}
