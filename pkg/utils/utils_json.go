package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
)

// Decode JSON 解析返回的数据
func Decode(buffer []byte, v interface{}) error {
	err := json.NewDecoder(bytes.NewBuffer(buffer)).Decode(v)
	if err != nil {
		return fmt.Errorf(`json Decode failed %s`, err.Error())
	}
	return nil
}

// DecodeTo JSON 转换数据格式
func DecodeTo(buffer []byte) interface{} {
	var v interface{}
	err := Decode(buffer, v)
	if err != nil {
		panic(err)
	}
	return v
}
