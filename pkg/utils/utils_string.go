package utils

import (
	"gitee.com/bobo-rs/idea-space-framework/pkg/encrypt"
	"github.com/gogf/gf/v2/errors/gerror"
	"regexp"
	"strings"
	"unicode"
	"unicode/utf8"
)

// IsSeparator 返回一个布尔值，表示rune是否为分隔符
func IsSeparator(r rune) bool {
	// 检查是否是逗号（中英文）、空格、制表符、回车符或换行符
	return r == ',' || r == '，' || unicode.IsSpace(r)
}

// SeparatorStringToArray 风格拆分字符串为数组字符串
func SeparatorStringToArray(s string) []string {
	// 分隔符
	slices := strings.FieldsFunc(s, IsSeparator)
	if len(slices) == 0 {
		return nil
	}
	return slices
}

// HashMd5 Md5加密
func HashMd5(slices ...string) (string, error) {
	if len(slices) == 0 {
		return "", gerror.New(`缺少Hash参数`)
	}
	// 拼接hash字符串
	return encrypt.NewM().MustEncryptString(strings.Join(slices, "")), nil
}

// HashMd5String Md5加密字符串
func HashMd5String(slices ...string) string {
	hash, _ := HashMd5(slices...)
	return hash
}

// CountRunes 统计字符串字符长度
func CountRunes(s string) int {
	return utf8.RuneCountInString(s)
}

// SliceAddPrefix 字符串数组元素值添加前缀
func SliceAddPrefix(slice []string, prefix string) []string {
	if len(slice) == 0 {
		return nil
	}
	// 填充前缀
	for i, s := range slice {
		slice[i] = prefix + s
	}
	return slice
}

// SplitNonCharWords 使用非字符（\W）作为分隔符来分割字符串，并去除结果中的空字符串
func SplitNonCharWords(s string) []string {
	words := regexp.MustCompile(`\W+`).Split(s, -1)
	if len(words) == 0 {
		return nil
	}

	// 去除空格
	newWords := make([]string, 0)
	for _, word := range words {
		if word == "" {
			continue
		}
		newWords = append(newWords, word)
	}
	return newWords
}

// SplitSymbolWords 使用标点符号分割字符串
func SplitSymbolWords(s string) []string {
	return regexp.MustCompile(`[\s；;,、]+`).Split(s, -1)
}

// Slugify 转换URL路径为搜索引擎友好的字符串
func Slugify(path string, seps ...string) string {
	sep := `-`
	if len(seps) > 0 {
		sep = seps[0]
	}
	// 转换地址符号并去除掉前后符
	return strings.Trim(strings.Replace(path, `/`, sep, -1), sep)
}
