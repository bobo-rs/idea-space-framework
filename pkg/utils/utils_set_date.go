package utils

import "time"

// ToDayZeroDiff 获取明天凌晨0点到当前时间的时间差
func ToDayZeroDiff() time.Duration {
	// 获取当前时间
	now := time.Now()

	// 设置明天时间
	toDay := now.AddDate(0, 0, 1)

	// 设置凌晨0点
	toDay = time.Date(toDay.Year(), toDay.Month(), toDay.Day(), 0, 0, 0, 0, time.Local)

	// 计算当前时间到明天凌晨零点时间差,并转换为分钟
	return time.Minute * (time.Duration(toDay.Unix()-now.Unix()) / 60)
}
