package utils

import (
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/consts"
	"runtime"
	"testing"
)

func TestSeparatorStringToArray(t *testing.T) {
	s := `hshhs, shfsf, jjj，上课还是分开，fhsfhhf
sfhskfhksdf
shfsjdfhj	sdfh ddddd ddd  d`
	fmt.Println(SeparatorStringToArray(s), len(SeparatorStringToArray(s)))
}

func TestHashMd5(t *testing.T) {
	fmt.Println(HashMd5(`淘宝`, `12`))
}

func TestAlphaNum(t *testing.T) {
	fmt.Println(AlphaNum(`手机手机放还减肥是减肥`))

	fmt.Println(AlphaNum(`fhjsfhjkshfjs12312312`))
}

func TestArrayColumns(t *testing.T) {
	slices := make([]map[string]interface{}, 0)
	slices = append(slices, map[string]interface{}{
		`id`:   4,
		`name`: `哈哈哈哈`,
	})
	fmt.Println(NewArray(slices).ArrayColumns("id"))

	sMap, _ := NewArray(slices).ColumnMap(`name`)
	sMap1, _ := NewArray(slices).ColumnMap(`id`)
	fmt.Println(sMap, sMap[`哈哈哈哈`])
	fmt.Println(sMap1, sMap1[4])
}

func TestNewArray(t *testing.T) {
	ImageExts := []string{`jpg`, `jpeg`, `png`, `webp`, `gif`}

	fmt.Println(NewArray(ImageExts).IsExists(`png`))
	fmt.Println(NewArray(ImageExts).MustIsExists(`p.ng`))
}

func TestGenCombinations(t *testing.T) {
	values := []string{`淘宝`, `美妆`, `双十一`, `周年庆`}
	fmt.Println(GenCombinationsString(values, `-`))
	fmt.Println(GenCombinationsString(values[0:2], `-`))
}

func TestAge(t *testing.T) {
	fmt.Println(Age(AgeItem{
		Birthday: `1993-01-05`,
	}))
	fmt.Println(Age(AgeItem{
		Birthday: `1993-11-15`,
	}))
	fmt.Println(Age(AgeItem{
		Birthday: `1900-11-15`,
	}))
}

func TestAddOrSub(t *testing.T) {
	fmt.Println(Add(100, 3200, 2), Add(100, 3200, 2, 900, 1000))
	fmt.Println(Sub(100, 3200, 2), Sub(100, 3200, 2, 900, 1000))
}

func TestGetMac(t *testing.T) {
	fmt.Println(GetMac())
}

func TestPwdOrVerify(t *testing.T) {
	pwdHash, _ := PasswordHash(`123456`)
	fmt.Println(pwdHash)
	fmt.Println(PasswordVerify(pwdHash, `123456`))

	pwdHash, _ = PasswordHash(`123456`, `aabbcc`)
	fmt.Println(pwdHash)
	fmt.Println(PasswordVerify(pwdHash, `123456`, `aabbcc`))

	fmt.Println(PasswordVerify(`$2y$13$GKGUxUw3En0pnWoHRnVR2.YOPCrOKdJ0of5LzAxmmoKDgkOdPu9Iu`, `a1234567!`, `TtaPfy`))
}

func TestMustMacString(t *testing.T) {
	fmt.Println(MustMacString())
}

func TestAlphaDash(t *testing.T) {
	sTrue, sFalse := `adch_123-45`, `abcd-er-123_哈哈哈`
	fmt.Println(AlphaDash(sTrue), AlphaDash(sFalse))
}

func TestSmsReplaceVal(t *testing.T) {
	var (
		s       = `尊敬用户{1},我来了{2},hhhhhh{10}, jjjsjs{88}, {9}`
		rplVal0 = []string{`r1`, `r2`}
		rplVal1 = []string{`r1`, `r2`, `r3`, `r4`, `r5`, `r6`, `r7`}
		rplVal2 = []string{`r1`, `r2`, `r3`, `r4`, `r5`}
	)
	fmt.Println(`不替换`, SmsReplaceVal(s))
	fmt.Println(`少匹配`, SmsReplaceVal(s, rplVal0...))
	fmt.Println(`多匹配`, SmsReplaceVal(s, rplVal1...))
	fmt.Println(`相匹配`, SmsReplaceVal(s, rplVal2...))

}

func TestRandInt(t *testing.T) {
	fmt.Println(RandInt(6))
	fmt.Println(RandomInt(100000, 999999))
	fmt.Println(SmsCaptchaCacheKey(consts.SafeSmsCaptcha24HourNum))
	fmt.Println(SafeLoginCacheKey(consts.SafeLoginIp))
	fmt.Println(LoginAuthTokenKey(1234, `hhhskahsjfjshdfjds`))
}

func TestNewAccount(t *testing.T) {
	fmt.Println(NewAccount())
}

func TestParseUserAgent(t *testing.T) {
	var (
		userAgentW = `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36`
		userAgentI = `Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1`
		userAgentH = `Mozilla/5.0 (HarmonyOS; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1`
	)

	fmt.Println(ParseUserAgentDevice(userAgentI))
	fmt.Println(ParseUserAgentDevice(userAgentW))
	fmt.Println(DeviceFrom(userAgentW))
	fmt.Println(DeviceFrom(userAgentI))
	fmt.Println(DeviceFrom(userAgentH))
	fmt.Println(IsPC(userAgentH))
	fmt.Println(IsPC(userAgentW))
	fmt.Println(runtime.GOOS)
}

func TestIsBase64(t *testing.T) {
	s := `dGVzdCBzdHJpbmc=`
	s1 := `5ae75e9b155ddd65094259e2b1c9ee6f3fd9773a5563eac3832b6a3d076c75da`
	fmt.Println(IsBase64(s), IsBase64(s1), IsBase64(`hhfsfsewww`))
}

func TestIsSha256Hash(t *testing.T) {
	var (
		s  = `dGVzdCBzdHJpbmc=`
		s1 = `5ae75e9b155ddd65094259e2b1c9ee6f3fd9773a5563eac3832b6a3d076c75da`
		s2 = `f7bc83f430538424b13298e6aa6fb143ef4d59a14946175997479dbc2d1a3cd8`
	)
	fmt.Println(IsSha256Hash(s), IsSha256Hash(s1), IsSha256Hash(s2))
	fmt.Println(IsHex(s1), IsHex(s1+s2), IsHex(s))
}

func TestPasswordHash(t *testing.T) {
	fmt.Println(PasswordHash(`a123456.`, `qwykvs`))
}

func TestSplitWords(t *testing.T) {
	str := `fsfsfsd; ; dddd,ssddf-sdd,我'的'天\\r\n\t好%好好`
	fmt.Println(SplitNonCharWords(str), len(SplitNonCharWords(str)))
	fmt.Println(SplitSymbolWords(str), len(SplitSymbolWords(str)))
}

func TestSlugify(t *testing.T) {
	var (
		p1 = `v1/user/login`
		p2 = `/v1/user/login`
		p3 = `/v1/user/login/`
		p4 = `/v1/user/login/mobile?id=10`
	)
	fmt.Println(Slugify(p1), Slugify(p2), Slugify(p3), Slugify(p4))
}
