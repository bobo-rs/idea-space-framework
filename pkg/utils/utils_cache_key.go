package utils

import (
	"gitee.com/bobo-rs/idea-space-framework/consts"
	"strconv"
	"strings"
)

const (
	keySep = `-` // key分割符
)

// CacheKey 缓存KEY前缀
func CacheKey(key string, keys ...string) string {
	if len(keys) > 0 {
		key += strings.Join(keys, keySep)
	}
	return consts.CachePrefix + key
}

// SmsCaptchaCacheKey 短信验证码缓存KEY
func SmsCaptchaCacheKey(key string, keys ...string) string {
	if len(keys) > 0 {
		key += strings.Join(keys, keySep)
	}
	return CacheKey(consts.SafeSmsCaptchaPrefix, key)
}

// SafeSecretCacheKey 安全密钥缓存KEY
func SafeSecretCacheKey(key string, keys ...string) string {
	if len(keys) > 0 {
		key += strings.Join(keys, keySep)
	}
	return CacheKey(consts.SafeSecretPrefix, key)
}

// SafeLoginCacheKey 账户登录安全密钥KEY
func SafeLoginCacheKey(key string, keys ...string) string {
	if len(keys) > 0 {
		key += strings.Join(keys, keySep)
	}
	return CacheKey(consts.SafeLoginPrefix, key)
}

// TokenCacheKey Token 缓存KEY
func TokenCacheKey(token string, subjects ...string) string {
	// 是否已加密
	if !IsSha256Hash(token) {
		token = ToSha256([]byte(token))
	}
	// 是否存在主题
	if len(subjects) > 0 {
		return CacheKey(consts.JWTTokenHash, subjects[0], token)
	}
	return CacheKey(consts.JWTTokenHash, token)
}

// LoginAuthTokenKey 登录授权TokenKEY
func LoginAuthTokenKey(uid uint, token string) string {
	return CacheKey(consts.LoginAuthToken, strconv.Itoa(int(uid)), token)
}
