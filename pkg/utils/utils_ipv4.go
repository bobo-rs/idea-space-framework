package utils

import (
	"errors"
	"fmt"
	"net"
	"strings"
)

// IMac Net网络Mac地址
type IMac struct {
	Mac  string
	Macs []string
}

// GetMac 获取MAC地址
func GetMac() (iMac *IMac, err error) {
	// 获取网络接口
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, fmt.Errorf("无法获取网络接口：%s", err.Error())
	}

	// 循环出网络接口
	iMac = &IMac{}

	for _, iface := range ifaces {
		mac := iface.HardwareAddr.String()
		if len(mac) == 0 {
			continue
		}

		// MAC地址转大写
		iMac.Macs = append(iMac.Macs, mac)
		// 忽略环回接口和虚拟接口
		if iface.Flags&net.FlagLoopback != 0 || iface.Flags&net.FlagUp == 0 {
			continue
		}

		// 当前正在使用的MAC地址
		iMac.Mac = mac
	}
	// 取出第一个MAC为默认MAC地址
	if len(iMac.Macs) == 0 {
		return nil, errors.New("没有可用的网络MAC地址")
	}
	// 当前没有网络在连接，默认第一个为默认MAC
	if len(iMac.Mac) == 0 {
		iMac.Mac = iMac.Macs[0]
	}
	return
}

// MacString 获取Mac地址字符串
func MacString(sep ...string) (string, error) {
	item, err := GetMac()
	if err != nil {
		return "", err
	}
	// 是否替换符号
	if len(sep) > 0 {
		item.Mac = strings.Replace(item.Mac, `:`, sep[0], -1)
	}
	return item.Mac, nil
}

// MustMacString 获取必须Mac地址
func MustMacString(sep ...string) string {
	mac, _ := MacString(sep...)
	return mac
}
