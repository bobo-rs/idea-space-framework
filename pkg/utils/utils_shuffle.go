package utils

import (
	"math"
	"math/rand"
	"time"
)

// RandInt 根据数值长度获取区域范围内数值随机整数
func RandInt(n int) int {
	// 设置随机因子
	rand.NewSource(time.Now().UnixNano())
	var (
		minVal = math.Pow(10, float64(n-1))
		maxVal = math.Pow(10, float64(n)) - minVal
	)
	// 最小值+随机最大值（最大值-最小值+1）避免出现超越边界长度数值
	return int(minVal) + rand.Intn(int(maxVal))
}

// RandomInt 自定义选取区域类随机数
func RandomInt(minVal, maxVal int) int {
	// 设置随机因子
	rand.NewSource(time.Now().UnixNano())
	return minVal + rand.Intn(maxVal-minVal)
}
