package utils

import (
	"gitee.com/bobo-rs/idea-space-framework/consts"
	"regexp"
	"strings"
)

// ScanIsEmpty 扫描数据验证是否为空
func ScanIsEmpty(errorMsg string) bool {
	re := regexp.MustCompile(consts.MatchScanIsEmpty)
	return re.MatchString(errorMsg)
}

// AlphaNum 验证是否字母和数字
func AlphaNum(s string) bool {
	re := regexp.MustCompile(consts.AlphaNum)
	return re.MatchString(s)
}

// ChsDash 匹配汉字、字母、数字特殊字符
func ChsDash(s string) bool {
	re := regexp.MustCompile(consts.ChsDash)
	return re.MatchString(s)
}

// ChsAlphaNum 匹配汉字、字母、数字
func ChsAlphaNum(s string) bool {
	re := regexp.MustCompile(consts.ChsAlphaNum)
	return re.MatchString(s)
}

// AlphaDash 字母和数字，下划线_及破折号-
func AlphaDash(s string) bool {
	return regexp.MustCompile(consts.AlphaDash).MatchString(s)
}

// SmsReplaceVal 替换短信模板值
func SmsReplaceVal(s string, rplVal ...string) string {
	slice := regexp.MustCompile(consts.MatchSetParams).Split(s, -1)
	if len(slice) == 0 {
		return ""
	}
	var (
		content string
		sepLen  = len(rplVal)
	)

	// 是否有拼接符
	if sepLen == 0 {
		return strings.Join(slice, "")
	}

	// 迭代拼接替换值
	sepLen -= 1
	for key, v := range slice {
		if key <= sepLen {
			v += rplVal[key] // 拼接替换值
		}
		content += v
	}
	return content
}

// IsMobile 验证是否手机号
func IsMobile(s string) bool {
	return regexp.MustCompile(consts.MatchPhone).MatchString(s)
}

// ParseUserAgentDevice 解析UserAgent设备消息
func ParseUserAgentDevice(s string) string {
	devices := regexp.MustCompile(`\(([^()]+)\)`).FindAllString(s, 1)
	if len(devices) == 0 {
		return ``
	}
	return strings.TrimRight(strings.TrimLeft(devices[0], `(`), `)`)
}

// DeviceFrom 设备平台，例如windows、iphone
func DeviceFrom(s string) string {
	device := ParseUserAgentDevice(s)
	if len(device) == 0 {
		return `other`
	}
	// 取出平台设备
	fromAll := regexp.MustCompile(`^[a-zA-Z]+`).FindAllString(device, 1)
	if len(fromAll) == 0 {
		return `other`
	}
	return strings.ToLower(fromAll[0])
}

// IsPC 是否PC端
func IsPC(s string) bool {
	if len(s) == 0 {
		return true
	}
	// 验证
	mobileDeviceAll := MobileDeviceAll()
	for _, v := range mobileDeviceAll {
		if regexp.MustCompile(v).MatchString(s) {
			// 移动端
			return false
		}
	}
	return true
}

// MobileDeviceAll 移动端设备
func MobileDeviceAll() []string {
	// 常见的移动设备User-Agent片段
	return []string{
		"Mobi",               // 通常用于移动设备
		"(iPhone|iPod|iPad)", // iOS设备
		"(Android|webOS|BlackBerry|IEMobile|Opera Mini|Windows Phone|Phone|Mobile|mobile|tablet|Tablet|iPad|PlayBook|silk)", // 其他移动设备
		"Windows CE",    // 老式Windows Mobile
		"Opera Mini",    // Opera Mini浏览器
		"BlackBerry",    // BlackBerry设备
		"IEMobile",      // IE Mobile浏览器
		"Symbian",       // Symbian系统
		"Palm",          // Palm设备
		"Kindle",        // Kindle电子书
		"Silk",          // Kindle Silk浏览器
		"Mobile Safari", // 移动Safari浏览器
		"MobileChrome",  // 移动Chrome浏览器
		"Opera Mobi",    // Opera Mobile浏览器
		"Opera Mini/i",
		"Fennec",                                // Firefox Mobile（Fennec）
		"UCWEB",                                 // UCWeb浏览器
		"Fennec/",                               // Firefox Mobile
		"Android",                               // Android系统
		"Windows Phone",                         // Windows Phone系统
		"Windows Mobile",                        // Windows Mobile系统
		"Mobile",                                // 移动设备通用标识
		"Mobile/",                               // 移动设备通用标识
		"tablet",                                // 平板电脑
		"Tablet",                                // 平板电脑
		"Xoom",                                  // Motorola Xoom平板电脑
		"Silk-Accelerated",                      // Kindle Silk加速版
		"AppleWebKit.+Mobile",                   // 移动浏览器
		"Mobile.+Safari",                        // 移动Safari浏览器
		"Windows CE; PPC;",                      // Windows Mobile
		"Windows CE; Smartphone;",               // Windows Mobile
		"Windows Phone OS",                      // Windows Phone
		"Windows Mobile OS",                     // Windows Mobile
		"Opera Mobi",                            // Opera Mobile
		"Opera Mini",                            // Opera Mini
		"Android 4.0.+; Tablet",                 // 安卓4.0+平板电脑
		"Android 3.0.+; Tablet",                 // 安卓3.0+平板电脑
		"BlackBerry Tablet OS",                  // 黑莓平板电脑
		"BB10;",                                 // 黑莓10系统
		"RIM Tablet OS",                         // RIM平板电脑系统
		"MobileSafari",                          // 移动Safari
		"Chrome/25.0.1364.1200.0 Mobile Safari", // Chrome Mobile
		"CriOS",                                 // Chrome iOS
		"CriOS Mobile",                          // Chrome iOS Mobile
		"Android.*Mobile Safari.*AppleWebKit*",  // 安卓移动设备上的Safari
		"HarmonyOS",                             // 鸿蒙系统
	}
}
