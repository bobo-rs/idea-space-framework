package utils

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"regexp"
)

// ToSha256 转换为Sha256十六进制字符串
func ToSha256(b []byte) string {
	// 实例Hash值
	hasher := sha256.New()
	if _, err := io.Copy(hasher, bytes.NewBuffer(b)); err != nil {
		return ""
	}
	return hex.EncodeToString(hasher.Sum(nil))
}

// IsSha256Hash 检测字符串是否HASH值
func IsSha256Hash(hash string) bool {
	return regexp.MustCompile(`^[a-fA-F0-9]{64}$`).MatchString(hash)
}

// IsHex 检测字符串是否十六进制
func IsHex(hex string) bool {
	return regexp.MustCompile(`^[a-fA-f0-9]*$`).MatchString(hex)
}
