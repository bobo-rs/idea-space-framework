package utils

import "encoding/base64"

// IsBase64 验证字符串是否Base64格式
func IsBase64(s string) bool {
	_, err := base64.StdEncoding.DecodeString(s)
	return err == nil
}
