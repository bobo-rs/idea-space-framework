package utils

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/util/gmeta"
	"reflect"
	"runtime/debug"
)

// Summary 接口描述摘要
func Summary(r *ghttp.Request) string {
	value := ParseHandlerFunc(r)
	if value == nil {
		return ""
	}
	// 转换接口类型
	return gmeta.Get(*value, `summary`).String()
}

// ParseHandlerFunc 解析g.Meta元数据函数
func ParseHandlerFunc(r *ghttp.Request) *reflect.Value {
	h := r.GetServeHandler().Handler
	if h.Info.Type == nil || h.Info.Type.NumIn() < 2 {
		return nil
	}

	// 反射解析值
	value := reflect.New(h.Info.Type.In(1))
	return &value
}

// GetIp 获取客户端IP
func GetIp(ctx context.Context) string {
	defer func() {
		if r := recover(); r != nil {
			// 非WEB端调用报错
			g.Log().Debug(ctx, `获取IP错误，非Web端调用`, r, debug.Stack())
		}
	}()
	return ghttp.RequestFromCtx(ctx).GetClientIp()
}
