package sredis

import (
	"context"
	"fmt"
	"github.com/gogf/gf/v2/database/gredis"
	"time"
)

type sRedisService struct {
	redis *gredis.Redis
}

// IncrEx 根据原子性增加数量+1
func (s *sRedisService) IncrEx(ctx context.Context, key string, expire time.Duration) error {
	// 原子增加数量+1
	if _, err := s.redis.Incr(ctx, key); err != nil {
		return err
	}
	// 设置效期
	if _, err := s.redis.Expire(ctx, key, int64(expire.Seconds())); err != nil {
		return fmt.Errorf(`set incr expire falied %s`, err.Error())
	}
	return nil
}
