package sredis

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/pkg/config"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/os/gcache"
)

// setAdapterCache 设置缓存Redis缓存
func setAdapterCache() *gcache.Cache {
	var (
		ctx   = context.Background()
		cache = gcache.New()
		err   error
	)
	// 拦截错误
	defer func() {
		if err != nil {
			panic(err)
		}
	}()

	// 读取Redis配置
	redisConfig := config.New().MustRedisConfigCache(ctx)

	// 实例Redis
	redis, err := gredis.New(redisConfig)
	if err != nil {
		return nil
	}

	// 注册Redis缓存
	cache.SetAdapter(gcache.NewAdapterRedis(redis))
	return cache
}

// setAdapterRedis 设置注册Redis
func setAdapterRedis() *gredis.Redis {
	var (
		err error
		ctx = context.Background()
	)
	// 拦截错误
	defer func() {
		if err != nil {
			panic(err)
		}
	}()

	// 读取配置
	redisConfig := config.New().MustRedisConfigDefault(ctx)

	// 实例Redis
	redis, err := gredis.New(redisConfig)
	if err != nil {
		return nil
	}
	return redis
}
