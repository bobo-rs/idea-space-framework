package sredis

import (
	"context"
	"fmt"
	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	_ "go.opentelemetry.io/otel/sdk/trace"
	"testing"
	"time"
)

var (
	Ctx = context.Background()
)

func TestRedis(t *testing.T) {
	var (
		redis = New()
		key   = `test.redis.key`
		value = `test——key`
	)
	fmt.Println(redis.Set(Ctx, key, value))
	fmt.Println(redis.Expire(Ctx, key, 60))
	fmt.Println(redis.Get(Ctx, key))
}

func TestCache(t *testing.T) {
	var (
		cache = NewCache()
		key   = `test.cache.key`
		value = `test-cache——key`
	)

	fmt.Println(cache.GetOrSet(Ctx, key, value, time.Minute))
	//fmt.Println(cache.Set(Ctx, key, value, 1000*60))
	//fmt.Println(cache.Get(Ctx, key))
}
