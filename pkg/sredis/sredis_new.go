package sredis

import (
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/os/gcache"
)

// New 实例Redis
func New() *gredis.Redis {
	return setAdapterRedis()
}

// NewRedis 实例重写Redis
func NewRedis() *sRedisService {
	return &sRedisService{
		redis: New(), // 实例服务
	}
}

// NewCache 实例缓存
func NewCache() *gcache.Cache {
	return setAdapterCache()
}
