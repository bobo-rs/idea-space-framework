package img

import (
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/pkg/ifont"
	"github.com/disintegration/imaging"
	"golang.org/x/image/font/opentype"
	"image"
	"path/filepath"
	"testing"
)

func TestIHandle_Overlay(t *testing.T) {

	image2 := New()
	image2.SetPath(`E:\www\Go\idea-space\uploads\origin-2.png`)
	images := []IOverlayItem{
		{
			Src:     `E:\www\Go\idea-space\uploads\text\1.png`,
			PointX:  7,
			PointY:  699,
			Opacity: 1,
		},
		{
			Src:     `E:\www\Go\idea-space\uploads\text\2.png`,
			PointX:  0,
			PointY:  744,
			Opacity: 1,
		},
		{
			Src:     `E:\www\Go\idea-space\uploads\text\3.png`,
			PointX:  0,
			PointY:  698,
			Opacity: 1,
		},
	}
	fmt.Println(image2.Overlay(images...))
	//// 锐化
	//fmt.Println(image2.Sharpen(0.5))
	// 保存图片
	image2.SetDir(`E:\www\Go\idea-space\uploads\text\`)
	image2.SetFilename(`合成.png`)
	fmt.Println(image2.Save())
}

func TestIHandle_OverlayDrawText(t *testing.T) {
	images := []IOverlayItem{
		{
			Src:     `E:\www\Go\idea-space\uploads\1.png`,
			PointX:  7,
			PointY:  699,
			Opacity: 1,
			DrawText: &DrawTextItem{
				Text:     `买2份加送 205g 牛肉酱1瓶`,
				ColorHex: `#e41e2b`,
				FontSize: 23,
				TextType: 1,
				TextX:    186,
			},
		},
		{
			Src:     `E:\www\Go\idea-space\uploads\2.png`,
			PointX:  0,
			PointY:  744,
			Opacity: 1,
			DrawText: &DrawTextItem{
				Text:     `破损包退 多口味可选`,
				ColorHex: `#fff`,
				FontSize: 30,
				TextType: 1,
				TextX:    194,
			},
		},
		{
			Src:     `E:\www\Go\idea-space\uploads\3.png`,
			PointX:  0,
			PointY:  698,
			Opacity: 1,
			DrawText: &DrawTextItem{
				Text:     `12.98元`,
				ColorHex: `#fff`,
				FontSize: 72,
				TextType: 2,
				TextX:    0,
				Sep:      `.`,
			},
		},
	}

	img := New()
	// 加载背景图
	img.SetPath(`E:\www\Go\idea-space\uploads\origin-2.png`)
	img.SetFontPath(`E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`)
	img.SetDir(`E:\www\Go\idea-space\uploads\text\`)
	img.SetFilename(`合成一体98元.png`)

	// 合成绘制
	fmt.Println(`合成图片`, img.Overlay(images...))
	fmt.Println(`保存图片`, img.Save())
	fmt.Println(`文件扩展`, img.Ext())
	fmt.Println(`Base64前缀`, img.Base64Prefix(), img.GetFilePath())
	//fmt.Println(img.ContentString())
}

func TestIHandle_Thumb(t *testing.T) {
	img := New()
	// 生成缩略图
	img.SetPath(`E:\www\Go\idea-space\uploads\text\合成一体.png`)
	//img.SetDir(`E:\www\Go\idea-space\uploads\text\`)
	fmt.Println(filepath.Dir(img.path))
	fmt.Println(img.Thumb(800, 800, imaging.Hamming))
	fmt.Println(`压缩800x800`, img.Save())
	//fmt.Println(img.Thumb(500, 400))
	//fmt.Println(`压缩500x400`, img.Save())
	//fmt.Println(img.Thumb(400, 300))
	//fmt.Println(`压缩400x300`, img.Save())
}

func TestIHandle_ModifyImageColor(t *testing.T) {
	img := New()
	suffix := `3.png`
	// 装换图片颜色
	img.SetPath(`E:\www\Go\idea-space\uploads\` + suffix)
	imgContent, err := img.Open()
	if err != nil {
		fmt.Println(err)
		return
	}
	// #68ffc3 绿色
	// #e41e2b 红色
	// #fff 白色
	copyImg, err := img.ImageColorToConvert(imgContent, `#fff`)
	if err != nil {
		fmt.Println(err)
		return
	}
	img.SetImg(copyImg)
	img.SetDir(`E:\www\Go\idea-space\uploads\material\`)
	img.SetFilename(suffix)
	fmt.Println(img.Save())
}

func TestIHandle_DrawText(t *testing.T) {
	type imgTestItem struct {
		suffix   string
		text     string
		colorHex string
		fontSize float64
		px       int
	}
	imageList := []imgTestItem{
		{
			suffix:   `1.png`,
			text:     `买2份加送 205g 牛肉酱1瓶 买2份加送 205g 牛肉酱1瓶 买2份加送 205g 牛肉酱1瓶 买2份加送 205g 牛肉酱1瓶 买2份加送 205g 牛肉酱1瓶`,
			colorHex: `#e41e2b`,
			fontSize: 23,
			px:       186,
		},
		{
			suffix:   `2.png`,
			text:     `破损包退 多口味可选`,
			colorHex: `#fff`,
			fontSize: 30,
			px:       194,
		},
		{
			suffix:   `3.png`,
			text:     `12.9`,
			colorHex: `#fff`,
			fontSize: 72,
			px:       0,
		},
	}

	im := New()
	ift := ifont.New()
	// 打开字体文件
	err := ift.LoadFont(`E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, item := range imageList {

		im.SetPath(`E:\www\Go\idea-space\uploads\` + item.suffix)
		// 绘制文字
		im.img, err = im.Open()
		if err != nil {
			fmt.Println(err)
			return
		}

		// 买2份加送 205g 牛肉酱1瓶 24 #e41e2b
		// 破损包退 多口味可选 42 #FFF
		// 12.9 72 #FFF
		// 文字居中至于为什么是乘以72然后除以96，这个查了一下资料，简单的说，字体的大小单位磅(points) 是1/72逻辑英寸，屏幕的分辨率是96DPI（96点每逻辑英寸），那么屏幕每个点就是72/96=0.75磅
		ift.SetFontItem(ifont.IFont{
			BgkImage: im.img,
			Point: image.Point{
				X: item.px,
			},
			FaceOptions: &opentype.FaceOptions{
				Size: item.fontSize,
			},
		})
		err = ift.DrawText(item.text, item.colorHex)
		if err != nil {
			fmt.Println(err, `绘制文字报错`)
		}

		// 重赋值图片属性
		//im.DstImage = ift.BgkImage

		fmt.Println(ift.Save(`E:\www\Go\idea-space\uploads\text\` + item.suffix))
	}
}
