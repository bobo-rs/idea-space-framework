package img

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"image"
	_ "image/gif"  // 导入gif解码器
	_ "image/jpeg" // 导入jpeg解码器
	_ "image/png"  // 导入png解码器
)

// ImageAttr 图片基础属性
type ImageAttr struct {
	Width  int
	Height int
	Size   int64
}

// IsValidImageExt 验证图片扩展
func IsValidImageExt(ext string) bool {
	switch ImageExt(ext) {
	case ImageExtGif, ImageExtJpeg, ImageExtPng, ImageExtJpg:
		return true
	default:
		return false
	}
}

// ParseContentToImage 解析图片内容并转换为图片
func ParseContentToImage(content string) (image.Image, error) {
	// 转换文本二进制
	buffer, err := base64.StdEncoding.DecodeString(content)
	if err != nil {
		return nil, errors.New(fmt.Sprintf(`内容转换文本二进制失败%s`, err.Error()))
	}
	var img image.Image
	if img, _, err = image.Decode(bytes.NewBuffer(buffer)); err != nil {
		return nil, err
	}
	return img, nil
}
