package bizcode

import "github.com/gogf/gf/v2/errors/gcode"

type (
	// BizCode 业务错误状态码
	BizCode struct {
		code    int
		message string
		detail  BizCodeDetail
	}

	// BizCodeDetail 业务处理详情
	BizCodeDetail struct {
		Code     string
		HttpCode int
	}
)

func (c BizCode) Code() int {
	return c.code
}

func (c BizCode) Message() string {
	return c.message
}

func (c BizCode) Detail() interface{} {
	return c.detail
}

func (c BizCode) BizDetail() BizCodeDetail {
	return c.detail
}

// New 实例业务状态码
func New(httpCode int, code, message string) gcode.Code {
	return BizCode{
		code:    0,
		message: message,
		detail: BizCodeDetail{
			Code:     code,
			HttpCode: httpCode,
		},
	}
}
