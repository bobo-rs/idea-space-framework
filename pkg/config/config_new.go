package config

import (
	"context"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// Get 读取配置
func (c *sConfig) Get(ctx context.Context, name string) (map[string]interface{}, error) {
	rawConfig, err := g.Cfg().Get(ctx, name)
	if err != nil {
		return nil, gerror.Wrapf(err, `Config read failed %s, %s`, name, err.Error())
	}
	return rawConfig.Map(), nil
}

// ConvString 转换配置字符串
func (c *sConfig) ConvString(rawConfig map[string]interface{}, field string) (string, error) {
	value, ok := rawConfig[field]
	if !ok {
		return "", gerror.Newf(`配置不存在%s`, field)
	}
	// 断言字符串
	if _, ok = value.(string); !ok {
		return "", gerror.Newf(`配置非字符串%+v`, value)
	}
	return value.(string), nil
}
