package config

import (
	"context"
)

const (
	ConfigSystem                 = `system`           // 系统配置
	ConfigSystemFontPath         = `fontPath`         // 字体路径
	ConfigSystemMaterialUnderImg = `materialUnderImg` // 素材默认底图
)

// GetSystem 获取系统配置
func (c *sConfig) GetSystem(ctx context.Context) (map[string]interface{}, error) {
	return c.Get(ctx, ConfigSystem)
}

// GetSystemValueString 获取系统配置指定值
func (c *sConfig) GetSystemValueString(ctx context.Context, name string) (string, error) {
	system, err := c.GetSystem(ctx)
	if err != nil {
		return "", err
	}
	return c.ConvString(system, name)
}

// GetFontPath 获取字体路径
func (c *sConfig) GetFontPath(ctx context.Context) (string, error) {
	return c.GetSystemValueString(ctx, ConfigSystemFontPath)
}

// GetMaterialUnderImg 获取素材底图
func (c *sConfig) GetMaterialUnderImg(ctx context.Context) (string, error) {
	return c.GetSystemValueString(ctx, ConfigSystemMaterialUnderImg)
}
