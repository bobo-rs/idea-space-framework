package config

import (
	"context"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/gconv"
)

const (
	ConfigRedis             = `redis`
	ConfigRedisGroupDefault = `default`
	ConfigRedisGroupCache   = `cache`
)

// GetRedis 获取Redis原始配置组集合
func (c *sConfig) GetRedisGroup(ctx context.Context) (map[string]gredis.Config, error) {
	rawRedisConfig, err := c.Get(ctx, ConfigRedis)
	if err != nil {
		return nil, gerror.Wrapf(err, `读取原始Redis配置失败%s`, err.Error())
	}
	redisConfigMap := make(map[string]gredis.Config)
	// 迭代转换Redis配置
	for key, conf := range rawRedisConfig {
		// 断言是否gredis配置
		var redisConfig gredis.Config
		if err = gconv.Struct(conf, &redisConfig); err != nil {
			return nil, gerror.Wrapf(err, `转换Redis Config 失败 %s`, err.Error())
		}
		redisConfigMap[key] = redisConfig
	}
	return redisConfigMap, nil
}

// GetRedisConfig 获取Redis配置
func (c *sConfig) GetRedisConfig(ctx context.Context, group string) (*gredis.Config, error) {
	// 读取原始配置
	redisConfigGroup, err := c.GetRedisGroup(ctx)
	if err != nil {
		return nil, err
	}
	redisConfig, ok := redisConfigGroup[group]
	if !ok {
		return nil, gerror.Newf(`缺少%s Redis Group配置`, group)
	}
	return &redisConfig, nil
}

// RedisConfigDefault 获取Redis默认配置
func (c *sConfig) RedisConfigDefault(ctx context.Context) (*gredis.Config, error) {
	// 读取Redis配置
	return c.GetRedisConfig(ctx, ConfigRedisGroupDefault)
}

// MustRedisConfigDefault 直接获取Redis默认配置
func (c *sConfig) MustRedisConfigDefault(ctx context.Context) *gredis.Config {
	conf, err := c.RedisConfigDefault(ctx)
	if err != nil {
		panic(err)
	}
	return conf
}

// RedisConfigCache 获取Redis缓存配置
func (c *sConfig) RedisConfigCache(ctx context.Context) (*gredis.Config, error) {
	// 读取Redis配置
	return c.GetRedisConfig(ctx, ConfigRedisGroupCache)
}

// MustRedisConfigCache 直接获取Redis缓存配置
func (c *sConfig) MustRedisConfigCache(ctx context.Context) *gredis.Config {
	conf, err := c.RedisConfigCache(ctx)
	if err != nil {
		panic(err)
	}
	return conf
}
