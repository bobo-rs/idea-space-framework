package config

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/pkg/jwt"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/gconv"
)

const (
	ConfigJWT             = `jwt`
	ConfigJWTGroupDefault = `default`
)

// GetJWTGroup 获取JWT配置组
func (c *sConfig) GetJWTGroup(ctx context.Context) (map[string]interface{}, error) {
	rawJWTGroup, err := c.Get(ctx, ConfigJWT)
	if err != nil {
		return nil, gerror.Wrapf(err, "读取JWT Group 配置失败%s", err.Error())
	}
	return rawJWTGroup, nil
}

// GetJWTConfig 获取JWT配置
func (c *sConfig) GetJWTConfig(ctx context.Context, group string) (jwt.ClaimsItem, error) {
	// JWT配置
	var JWTConfig jwt.ClaimsItem
	rawGroupConfig, err := c.GetJWTGroup(ctx)
	if err != nil {
		return JWTConfig, err
	}
	// 指定配置组不存在
	configJWT, ok := rawGroupConfig[group]
	if !ok {
		return JWTConfig, gerror.Newf(`JWT Group %s 不存在`, group)
	}

	if err = gconv.Struct(configJWT, &JWTConfig); err != nil {
		return JWTConfig, gerror.Wrapf(err, "转换JWT配置JWTConfigItem失败%s", err.Error())
	}

	return JWTConfig, nil
}

// GetJWTConfigDefault JWT默认配置
func (c *sConfig) GetJWTConfigDefault(ctx context.Context) (jwt.ClaimsItem, error) {
	return c.GetJWTConfig(ctx, ConfigJWTGroupDefault)
}
