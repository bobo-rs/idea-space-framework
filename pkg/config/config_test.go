package config

import (
	"context"
	"fmt"
	"testing"
)

var (
	Ctx = context.Background()
)

func TestSConfig_GetFontPath(t *testing.T) {
	fmt.Println(New().GetFontPath(Ctx))
}

func TestSConfig_GetUploadsConfig(t *testing.T) {
	fmt.Println(New().GetUploadsConfig(Ctx))
}
