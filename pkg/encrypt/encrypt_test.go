package encrypt

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/gogf/gf/v2/crypto/gmd5"
	"testing"
)

func TestMd5_Encrypt(t *testing.T) {
	s := `sjfhjsfhjsdhfs`
	fmt.Println(NewM([]byte(s)...).EncryptBytes())

	fmt.Println(NewM().MustEncryptString(s))

	fmt.Println(NewM().Encrypt(23))
	fmt.Println(NewM().Encrypt(123456))
	fmt.Println(gmd5.MustEncrypt(123456))
	fmt.Println(gmd5.MustEncryptString(`123456`))
	fmt.Println(NewM().EncryptString(`123456`))
}

func TestAESHandler_Encrypt(t *testing.T) {
	handler := NewAES()
	handler = handler.Cipher(CipherHandler{
		Secret: `14e1b600b1fd579f47433b88e8d85291`,
	})

	// 明文
	plaintext := `不可能，根本不可能`
	ciphertext, err := handler.Encrypt([]byte(plaintext))
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(`密文`, ciphertext, string(ciphertext))
	// 解密
	plaintextBytes, err := handler.Decrypt(ciphertext)
	if err != nil {
		fmt.Println(`解密`, err)
		return
	}
	fmt.Println(`解密`, string(plaintextBytes), plaintextBytes)
}

func TestAESHandler_EncryptString(t *testing.T) {
	handler := NewAES()
	handler = handler.Cipher(CipherHandler{
		//Secret: `14e1b600b1fd579f`, // CBC-128
		//Secret: `14e1b600b1fd579f47433b88`, // CBC-192
		Secret: `14e1b600b1fd579f47433b88e8d85291`, // CBC-256
		//Secret: `14e1b600b1fd579f47433b88e8d8529114e1b600b1fd579f47433b88e8d8529114e1b600b1fd579f47433b88e8d8529114e1b600b1fd579f47433b88e8d85291`,
		//Iv:     `14e1b600b1fd579f47433b88e8d85291`,
	})

	// 明文 14e1b600b1fd579f 47433b88e8d85291
	//plaintexts := []string{`不可能，根本不可能`, `你大爷`}
	//plaintexts := `不可能，根本不可能`
	//plaintexts := 19999991991
	key, _ := hex.DecodeString(`14e1b600b1fd579f47433b88e8d8529114e1b600b1fd579f47433b88e8d8529114e1b600b1fd579f47433b88e8d8529114e1b600b1fd579f47433b88e8d85291`)
	fmt.Println(len([]byte(handler.Secret)), string(key), len(string(key)), key)
	plaintexts := map[string]string{
		`name`: `张三`,
		`age`:  `78岁`,
	}
	plaintext, err := json.Marshal(plaintexts)
	if err != nil {
		fmt.Println(err)
		return
	}
	ciphertext, err := handler.EncryptString(plaintext)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(`十六进制密文`, ciphertext)

	// 解密
	plaintextBytes, err := handler.DecryptString(ciphertext)
	if err != nil {
		fmt.Println(`解密`, err)
		return
	}
	plaintextString := string(plaintextBytes)
	fmt.Println(`解密`, plaintextString, plaintextBytes)

	var v map[string]string
	fmt.Println(json.NewDecoder(bytes.NewReader(plaintextBytes)).Decode(&v))
	fmt.Println(v)

}
