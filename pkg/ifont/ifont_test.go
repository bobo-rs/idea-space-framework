package ifont

import (
	"fmt"
	"golang.org/x/image/font/opentype"
	"image"
	"testing"
)

func TestIFont_DrawText(t *testing.T) {
	type imgTestItem struct {
		suffix   string
		text     string
		colorHex string
		fontSize float64
		px       int
	}
	imageList := []imgTestItem{
		{
			suffix:   `1.png`,
			text:     `买2份加送 205g 牛肉酱1瓶 买2份加送 205g 牛肉酱1瓶 买2份加送 205g 牛肉酱1瓶 买2份加送 205g 牛肉酱1瓶 买2份加送 205g 牛肉酱1瓶`,
			colorHex: `#e41e2b`,
			fontSize: 23,
			px:       186,
		},
		{
			suffix:   `2.png`,
			text:     `破损包退 多口味可选`,
			colorHex: `#fff`,
			fontSize: 30,
			px:       194,
		},
		{
			suffix:   `3.png`,
			text:     `12.9`,
			colorHex: `#fff`,
			fontSize: 72,
			px:       0,
		},
	}
	ift := New()
	// 打开字体文件
	err := ift.LoadFont(`E:\www\Go\idea-space\idea-space-font\static\NotoSansSC-ExtraBold.ttf`)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, item := range imageList {
		ift.SetFontItem(IFont{
			Point: image.Point{
				X: item.px,
			},
			FaceOptions: &opentype.FaceOptions{
				Size: item.fontSize,
			},
		})
		// 打开图片
		ift.Open(`E:\www\Go\idea-space\uploads\` + item.suffix)
		// 买2份加送 205g 牛肉酱1瓶 24 #e41e2b
		// 破损包退 多口味可选 42 #FFF
		// 12.9 72 #FFF
		// 文字居中至于为什么是乘以72然后除以96，这个查了一下资料，简单的说，字体的大小单位磅(points) 是1/72逻辑英寸，屏幕的分辨率是96DPI（96点每逻辑英寸），那么屏幕每个点就是72/96=0.75磅

		err = ift.DrawText(item.text, item.colorHex)
		if err != nil {
			fmt.Println(err, `绘制文字报错`)
		}

		// 重赋值图片属性
		//im.DstImage = ift.BgkImage

		fmt.Println(ift.Save(`E:\www\Go\idea-space\uploads\text\` + item.suffix))
	}
}
