package ifile

import (
	"fmt"
	"github.com/gogf/gf/v2/os/gfile"
	"os"
	"path/filepath"
	"regexp"
	"testing"
)

func TestFileAttract_Attract(t *testing.T) {
	path := `E:\www\Go\idea-space\idea-space-admin\resource\uploads\20240402\d09fmfjxtei850bb3b.jpg`
	fs := New(path)
	fmt.Println(fs.MustContentBase64String())
	attract, _ := fs.Attract()
	fmt.Println(attract, len(attract.Hash))
}

func TestNormalizePath(t *testing.T) {
	linuxPath := `\www\Go\idea-space\uploads\material\under.png`
	winPath := `/www/Go/idea-space/uploads/material/under.png`

	fmt.Println(NormalizePath(linuxPath), NormalizePath(winPath))
	fmt.Println(os.Getwd())
	fmt.Println(gfile.Home())

	re := regexp.MustCompile(`^[a-zA-Z]:\\$`)
	fmt.Println(re.MatchString(`e:\www\Go\idea-space\`))
	fmt.Println(re.MatchString(`E:\`))
}

func TestRootPath(t *testing.T) {
	fmt.Println(MustRootPath())

	winPath := `\uploads\material\under.png`
	linuxPath := `/uploads/material/under.png`
	winDir := `E:\www`
	linuxDir := `/`

	fmt.Println(string(winPath[0]), winDir[:3])
	fmt.Println(filepath.IsAbs(winPath), filepath.IsAbs(linuxPath), filepath.IsAbs(winDir), filepath.IsAbs(linuxDir))
	fmt.Println(filepath.IsAbs(`.` + winPath))

	fmt.Println(`resource`, MustResourcePath(linuxPath), MustResourcePath(winPath))

	fmt.Println(`uploads`, MustUploadPath(linuxPath))

	fmt.Println(MustUploadPath(winPath))
}

func TestOptimizeUploadPath(t *testing.T) {

	linuxPath := `\www\Go\idea-space\uploads\material\under.png`
	linuxPath1 := `.\uploads\material\under.png`
	linuxPath2 := `.\uploads\material\under.png`

	winPath := `/www/Go/idea-space/uploads/material/under.png`
	winPath1 := `./uploads/material/under.png`
	winPath2 := `./resource/uploads/material/under.png`

	t.Run(`SlashUploadPath`, func(t *testing.T) {
		SlashUploadPath(linuxPath)
		IsWindowsPath(linuxPath2)
	})
	fmt.Println(SlashUploadPath(linuxPath), SlashUploadPath(winPath), SlashUploadPath(linuxPath2))
	fmt.Println(SlashUploadPath(linuxPath1), SlashUploadPath(winPath1), SlashUploadPath(winPath2))
}

func TestRelaDirPrefix(t *testing.T) {
	w1 := `\www\Go\idea-space\uploads\material\under.png`
	w2 := `.\uploads\material\under.png`
	w3 := `uploads\material\under.png`
	w4 := `E:\www\Go\idea-space\uploads\material\under.png`

	l1 := `/www/Go/idea-space/uploads/material/under.png`
	l2 := `./uploads/material/under.png`
	l3 := `resource/uploads/material/under.png`
	l4 := `/`
	fmt.Println(`windows`, SuppIndicator(w1), SuppIndicator(w2), SuppIndicator(w3), SuppIndicator(w4))
	fmt.Println(`linux`, SuppIndicator(l1), SuppIndicator(l2), SuppIndicator(l3), SuppIndicator(l4))
}

func TestUploadSubDir(t *testing.T) {
	fmt.Println(UploadSubDir(`material/dd`, false), GenFilename(`.png`))
	fmt.Println(UploadSubDir(`material/dd`, true), GenFilename(`dddd.png`))
}

func TestRootPath2(t *testing.T) {
	fmt.Println(RootPath())
	fmt.Println(os.Getenv(``))
	fmt.Println(os.Getwd())

	fmt.Println(MustUploadPath(`\material\example\`))
}
