package treenode

import "errors"

type sTreeNode struct {
	idColumn       string
	pidColumn      string
	childrenColumn string
	selectedColumn string // 选中字段：0未选择，1已选择，2部分选择
}

// selectedNode 选择查找节点属性
type selectedNode struct {
	count      int
	countChild int
	findNode   map[uint]bool
	id         uint
}

const (
	idColumnDefault            = `id`
	pidColumnDefault           = `pid`
	childrenColumnDefault      = `children`
	selectedColumnDefault      = `selected`
	selectedNot                = 0
	selectedUp                 = 1
	selectedPartial            = 2
	nodeRootPid           uint = 0 // 默认查找根节点
)

// SetIdColumn 设置ID字段
func (n *sTreeNode) SetIdColumn(column string) *sTreeNode {
	n.idColumn = column
	return n
}

// SetPidColumn 设置父级ID字段
func (n *sTreeNode) SetPidColumn(column string) *sTreeNode {
	n.pidColumn = column
	return n
}

// SetChildColumn 设置子级字段
func (n *sTreeNode) SetChildColumn(column string) *sTreeNode {
	n.childrenColumn = column
	return n
}

// SetSelectedColumn 设置选中字段
func (n *sTreeNode) SetSelectedColumn(column string) *sTreeNode {
	n.selectedColumn = column
	return n
}

// GetIdColumn 获取ID字段
func (n *sTreeNode) GetIdColumn() string {
	return n.idColumn
}

// GetPidColumn 获取父级ID字段
func (n *sTreeNode) GetPidColumn() string {
	return n.pidColumn
}

// GetChildColumn 获取子级字段
func (n *sTreeNode) GetChildColumn() string {
	return n.childrenColumn
}

// GetSelectedColumn 获取选中字段
func (n *sTreeNode) GetSelectedColumn() string {
	return n.selectedColumn
}

// IsField 字段检测是否存在
func (n *sTreeNode) IsField(node map[string]interface{}) error {
	if _, ok := node[n.idColumn]; !ok {
		return errors.New(`The required id field does not exist`)
	}
	if _, ok := node[n.pidColumn]; !ok {
		return errors.New(`The required pid field does not exist`)
	}
	return nil
}
