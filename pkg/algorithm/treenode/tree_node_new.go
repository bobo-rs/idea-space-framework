package treenode

// New 实例化构建树节点实例-可自定义如下字段：id，pid，children，selected
// 尤其是id和pid用于构建树的重要节点参数，不可或缺，默认：id，pid，children，selected
func New() *sTreeNode {
	// 初始化字段
	return &sTreeNode{
		idColumn:       idColumnDefault,
		pidColumn:      pidColumnDefault,
		childrenColumn: childrenColumnDefault,
		selectedColumn: selectedColumnDefault,
	}
}
