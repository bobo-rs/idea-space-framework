package jwt

import (
	"context"
	"fmt"
	"testing"
)

var (
	claims = ClaimsItem{
		Issuer:           `127.0.0.1`,
		Subject:          `testing`,
		Secret:           `14e1b600b1fd579f47433b88e8d85291`,
		SigningAlgorithm: `HS256`,
		ExpireUnit:       `h`,
		ExpireAt:         1,
		TokenLookup:      `header: Authori-zation`,
		TokenHeadName:    `Bearer`,
	}

	account = AccountDetail{
		Uid:      22,
		Account:  `test001`,
		Nickname: `张三`,
	}

	hook = HookHandler{
		Create: func(ctx context.Context, item AuthTokenItem) error {
			fmt.Println(`Create 此处实现自己的逻辑......`, item)
			return nil
		},
		Read: func(ctx context.Context, token string) (*AuthTokenItem, error) {
			fmt.Println(`Read 此处实现自己的逻辑......`, token)
			return nil, nil
		},
		Verify: func(ctx context.Context, token string) error {
			fmt.Println(`Verify 此处实现自己的逻辑......`, token)
			return nil
		},
		Remove: func(ctx context.Context, token string) error {
			fmt.Println(`Remove 此处实现自己的逻辑......`, token)
			return nil
		},
	}
)

func TestSJwt_CreateToken(t *testing.T) {
	j := New(claims)
	tokenClaims, err := j.CreateToken(account)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(tokenClaims, j.FillPrefixToken(tokenClaims.Token))
	// 解析
	fmt.Println(j.ParseToken(tokenClaims.Token))
}

func TestSJwt_ParseToken(t *testing.T) {
	token := `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVaWQiOjIyLCJBY2NvdW50IjoidGVzdDAwMSIsIk5pY2tuYW1lIjoi5byg5LiJIiwiaXNzIjoiMTI3LjAuMC4xIiwic3ViIjoidGVzdGluZyIsImV4cCI6MTcxNDM3NTEyNCwibmJmIjoxNzE0Mzc1MDY0LCJpYXQiOjE3MTQzNzUwNjR9.jnEkWPlZOV0Th2MshhujWVBLNfhphfNMXjoGKLg2YDM`
	fmt.Println(New(claims).ParseToken(token))
}

func TestSJWTHandler_Hook(t *testing.T) {
	handler := New(claims).Hook(hook)
	// 创建Token
	token, err := handler.CreateToken(account)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(token, handler.FillPrefixToken(token.Token))
	// 解析
	fmt.Println(handler.ParseToken(token.Token))
}
