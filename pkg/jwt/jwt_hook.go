package jwt

import "context"

type (
	createHook func(ctx context.Context, item AuthTokenItem) error             // 缓存创建的Token回调方法
	removeHook func(ctx context.Context, token string) error                   // 移除Token-退出登录使用
	readHook   func(ctx context.Context, token string) (*AuthTokenItem, error) // 读取缓存token
	verifyHook func(ctx context.Context, token string) error                   // 验证缓存Token是否存在

	// HookHandler JWT Hook自定义处理
	HookHandler struct {
		Create createHook // 创建缓存Token
		Read   readHook   // 读取缓存Token
		Remove removeHook // 移除缓存Token
		Verify verifyHook // 验证缓存Token
	}
)

// Hook 自定义Hook操作
// handler: 参数
// Read: 读取Token缓存
// Create：创建Token缓存
// Remove：移除Token缓存
// Verify：验证缓存Token是否存在
func (j *JWTHandler) Hook(handler HookHandler) *JWTHandler {
	j.HookHandler = handler
	return j
}

// RemoveHook 移除Token-刷新token后直接删除旧token或退出登录
func (j *JWTHandler) RemoveHook(token string) error {
	if j.HookHandler.Remove == nil {
		// 未注册移除Hook，正常处理，无需返回错误
		return nil
	}
	// 移除Token
	return j.HookHandler.Remove(j.ctx, j.FilterPrefix(token))
}

// ReadHook 读取缓存授权Token数据
func (j *JWTHandler) ReadHook(token string) (*AuthTokenItem, error) {
	if j.HookHandler.Read == nil {
		// 未注册读取Hook，正常处理，无需返回错误
		return nil, nil
	}
	return j.HookHandler.Read(j.ctx, j.FilterPrefix(token))
}

// VerifyHook 验证Token是否存在
func (j *JWTHandler) VerifyHook(token string) error {
	if j.HookHandler.Verify == nil {
		// 未注验证Hook，正常处理，无需返回错误
		return nil
	}
	return j.HookHandler.Verify(j.ctx, j.FilterPrefix(token))
}
