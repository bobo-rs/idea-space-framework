package models

type (
	// SendSmsInput 发送短信请求参数
	SendSmsInput struct {
		TemplateId     string   `json:"mark" dc:"模板ID"`
		Content        string   `json:"content" dc:"模板内容，适用于非模板ID模式"`
		TemplateParams []string `json:"params" dc:"模板参数"`
	}

	// PullSendStatusInput 拉取短信发送状态请求参数
	PullSendStatusInput struct {
		BeginTime uint64 `json:"begin_time" dc:"拉取起始时间，UNIX 时间戳（时间：秒）"`
		Offset    uint64 `json:"offset" dc:"偏移量"`
		Limit     uint64 `json:"limit" dc:"拉取最大条数，最多 100"`
		Mobile    string `json:"mobile" dc:"下发目的手机号码，依据 E.164 标准为：+[国家（或地区）码][手机号] ，示例如：+8613711112222"`
		EndTime   uint64 `json:"end_time" dc:"拉取截止时间，UNIX 时间戳（时间：秒）。示例值：1464624123"`
	}

	// AddSmsTemplateInput 添加或编辑短信模板请求参数
	AddSmsTemplateInput struct {
		TemplateId    string `json:"template_id" dc:"模板ID"`
		TemplateName  string `json:"template_name" dc:"模板名-验证码"`
		Content       string `json:"content" dc:"模板内容，变量{1},{2}"`
		SmsType       uint64 `json:"sms_type" dc:"短信类型，0验证码, 1消息通知, 2表示营销短信"`
		International uint64 `json:"international" dc:"是否国际/港澳台短信：0：表示国内短信。1：表示国际/港澳台短信"`
		Remark        string `json:"remark" dc:"应用场景备注，例如：业务验证码，注册验证码等"`
	}

	// SmsTemplateListInput 短信模板列表请求参数
	SmsTemplateListInput struct {
		International uint64   `json:"international" dc:"是否国际/港澳台短信：0：表示国内短信。1：表示国际/港澳台短信"`
		TemplateIdSet []uint64 `json:"template_id_set" dc:"模板ID集合"`
		Offset        uint64   `json:"offset" dc:"偏移量"`
		Limit         uint64   `json:"limit" dc:"拉取最大条数，最多 100"`
	}
)
