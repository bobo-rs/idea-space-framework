package models

type (
	// SendSmsResponse 短信发送响应结果
	SendSmsResponse struct {
		RequestId string              `json:"request_id" dc:"请求接口ID"`
		Results   []SendSmsResultItem `json:"results" dc:"短信发送结果集"`
	}

	// SendSmsResultItem 短信发送结果属性
	SendSmsResultItem struct {
		CommonSmsResultItem
		Code    string `json:"code" dc:"状态码"`
		Message string `json:"message" dc:"短信请求错误码描述"`
	}

	// PullSendResultItem 拉取短信发送结果
	PullSendResultItem struct {
		CommonSmsResultItem
		UserReceiveTime int    `json:"user_receive_time" dc:"用户实际接收到短信的时间，UNIX 时间戳（单位：秒）"`
		SendState       string `json:"send_state" dc:"发送状态"`
		Description     string `json:"description" dc:"发送状态描述"`
	}

	// AddSmsTemplateResponse 添加短信模板响应结果
	AddSmsTemplateResponse struct {
		TemplateId string      `json:"template_id" dc:"模板ID"`
		Extra      interface{} `json:"extra" dc:"额外参数"`
	}

	// SmsListResponse 短信模板列表响应
	SmsListResponse struct {
		Total int                  `json:"total" dc:"总数"`
		Rows  []*SmsListResultItem `json:"rows" dc:"短信模板列表"`
	}

	// SmsListResultItem 短信模板列表结果属性
	SmsListResultItem struct {
		TemplateId   string `json:"template_id" dc:"模板ID"`
		SmsTypeText  string `json:"sms_type_text" dc:"短信类型"`
		StatusText   string `json:"status_text" dc:"状态格式化"`
		Status       uint   `json:"status" dc:"审核模板状态：0审核中，1审核通过，2审核失败"`
		Reply        string `json:"reply" dc:"审核回复"`
		TemplateName string `json:"template_name" dc:"模板名称"`
		Content      string `json:"content" dc:"模板内容"`
		CreateAt     string `json:"create_at" dc:"创建模板时间"`
	}

	// CommonSmsResultItem 短信响应公共结果
	CommonSmsResultItem struct {
		SerialNo string `json:"serial_no" dc:"发送序列编号"`
		Mobile   string `json:"mobile" dc:"用户号码，普通格式，示例如：13711112222"`
	}
)
