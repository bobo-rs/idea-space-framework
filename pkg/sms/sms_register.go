package sms

import (
	"errors"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/enums"
)

// Driver 指定短信驱动服务
func (h *Handler) Driver(driver string) *Handler {
	h.driver = driver
	return h
}

// Services 注册短信服务
func (h *Handler) Services(services map[enums.SmsDriver]Service) *Handler {
	h.services = services
	return h
}

// Service 实例短信服务
func (h *Handler) Service() (Service, error) {
	service, ok := h.services[enums.SmsDriver(h.driver)]
	if !ok {
		return nil, errors.New(fmt.Sprintf(`sms service has not registered %s`, h.driver))
	}
	return service, nil
}
