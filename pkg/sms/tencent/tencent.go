package tencent

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/enums"
)

type (
	// Handler 腾讯服务处理
	Handler struct {
		ctx    context.Context // 上下文
		config ConfigItem      // 配置
	}

	// ConfigItem 腾讯SDK配置
	ConfigItem struct {
		SmsTencentSecretId   string            `json:"sms_tencent_secret_id"`   // 密钥ID
		SmsTencentSecretKey  string            `json:"sms_tencent_secret_key"`  // 密钥
		SmsTencentSignMethod string            `json:"sms_tencent_sign_method"` // 签名方式
		SmsTencentRegion     string            `json:"sms_tencent_region"`      // 地域|区域
		SmsTencentSdkAppid   string            `json:"sms_tencent_sdk_appid"`   // 短信APPID
		SmsTencentSignName   string            `json:"sms_tencent_sign_name"`   // 签名头
		SmsTencentSenderId   string            `json:"sms_tencent_sender_id"`   // 国内短信无需填写该项；国际/港澳台短信已申请独立 SenderId 需要填写该字段，默认使用公共 SenderId，无需填写该字段
		CountryCode          enums.CountryCode // 国家区号编号，例如：+86【中国】
	}
)

const (
	SmsSignMethod = `HmacSHA1`                // 签名
	SmsRegion     = `ap-guangzhou`            // 区域
	SmsEndpoint   = `sms.tencentcloudapi.com` // 就近域名
)
