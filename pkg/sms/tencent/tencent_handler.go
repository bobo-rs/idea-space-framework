package tencent

import (
	"gitee.com/bobo-rs/idea-space-framework/consts"
	"gitee.com/bobo-rs/idea-space-framework/pkg/sms/models"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	tencentSms "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/sms/v20210111"
	"strconv"
)

// credential 实例化一个认证对象，入参需要传入腾讯云账户密钥对secretId，secretKey
func (h *Handler) credential() *common.Credential {
	if len(h.config.SmsTencentSecretId) == 0 || len(h.config.SmsTencentSecretKey) == 0 {
		panic(`tencent credential SecretId or SecretKey empty`)
	}
	return common.NewCredential(h.config.SmsTencentSecretId, h.config.SmsTencentSecretKey)
}

// clientProfile 实例化一个客户端配置对象
func (h *Handler) clientProfile() *profile.ClientProfile {
	return &profile.ClientProfile{
		HttpProfile: &profile.HttpProfile{
			ReqMethod: `POST`,      // 默认POST
			Endpoint:  SmsEndpoint, // 域名
		},
		SignMethod: h.config.SmsTencentSignMethod,
		Language:   consts.LanguageChinese,
	}
}

// pullSendStatusHandler 拉取短信发送状态-所有
func (h *Handler) pullSendStatusHandler(limit uint64, client *tencentSms.Client) ([]*tencentSms.PullSmsSendStatus, error) {
	req := tencentSms.NewPullSmsSendStatusRequest()
	// 查询数量
	req.Limit = common.Uint64Ptr(limit)
	req.SmsSdkAppId = common.StringPtr(h.config.SmsTencentSdkAppid)

	// 请求接口
	response, err := client.PullSmsSendStatus(req)
	// 处理异常
	if err = h.commonErrorHandler(err); err != nil {
		return nil, err
	}
	// 处理数据
	return response.Response.PullSmsSendStatusSet, nil
}

// pullSendStatusByPhoneNumberHandler 拉取单一手机号发送状态
func (h *Handler) pullSendStatusByPhoneNumberHandler(in models.PullSendStatusInput, client *tencentSms.Client) (resp []*tencentSms.PullSmsSendStatus, err error) {
	// 实例拉取短信发送结果
	req := tencentSms.NewPullSmsSendStatusByPhoneNumberRequest()

	// 拉取限制数量
	req.Limit = common.Uint64Ptr(in.Limit)
	req.Offset = common.Uint64Ptr(in.Offset)

	// 手机号
	req.PhoneNumber = common.StringPtr(string(h.config.CountryCode) + in.Mobile)
	req.SmsSdkAppId = common.StringPtr(h.config.SmsTencentSdkAppid)

	// 拉取时间
	req.BeginTime = common.Uint64Ptr(in.BeginTime)

	// 截止时间
	if in.EndTime > 0 {
		req.EndTime = common.Uint64Ptr(in.EndTime)
	}

	// 请求接口
	response, err := client.PullSmsSendStatusByPhoneNumber(req)
	// 处理统一错误信息
	if err = h.commonErrorHandler(err); err != nil {
		return nil, err
	}

	// 处理数据
	return response.Response.PullSmsSendStatusSet, nil
}

// addSmsTemplateHandler 添加短信模板
func (h *Handler) addSmsTemplateHandler(in models.AddSmsTemplateInput, client *tencentSms.Client) (string, error) {
	// 实例添加模板请求
	req := tencentSms.NewAddSmsTemplateRequest()

	// 模板名
	req.TemplateName = common.StringPtr(in.TemplateName)

	// 模板内容
	req.TemplateContent = common.StringPtr(in.Content)

	// 短信类型：0表示普通短信, 1表示营销短信
	var smsType uint64
	if in.SmsType == 2 {
		smsType = 1
	}
	req.SmsType = common.Uint64Ptr(smsType)

	// 是否国际/港澳台短信：0国内，1国际/港澳台短信
	req.International = common.Uint64Ptr(in.International)

	// 短信备注
	req.Remark = common.StringPtr(in.Remark)

	// 请求接口
	response, err := client.AddSmsTemplate(req)
	if err = h.commonErrorHandler(err); err != nil {
		return "", err
	}
	return *response.Response.AddTemplateStatus.TemplateId, nil
}

// modifySmsTemplateHandler 修改短信模板内容-审核通过无法修改
func (h *Handler) modifySmsTemplateHandler(in models.AddSmsTemplateInput, client *tencentSms.Client) (string, error) {
	// 实例修改模板请求
	req := tencentSms.NewModifySmsTemplateRequest()

	// 模板ID
	req.TemplateId = common.Uint64Ptr(gconv.Uint64(in.TemplateId))

	// 模板名
	req.TemplateName = common.StringPtr(in.TemplateName)

	// 模板内容
	req.TemplateContent = common.StringPtr(in.Content)

	// 短信类型：0表示普通短信, 1表示营销短信
	var smsType uint64
	if in.SmsType == 2 {
		smsType = 1
	}
	req.SmsType = common.Uint64Ptr(smsType)

	// 是否国际/港澳台短信：0国内，1国际/港澳台短信
	req.International = common.Uint64Ptr(in.International)

	// 短信备注
	req.Remark = common.StringPtr(in.Remark)

	// 请求接口
	response, err := client.ModifySmsTemplate(req)
	if err = h.commonErrorHandler(err); err != nil {
		return "", err
	}
	return strconv.FormatUint(*response.Response.ModifyTemplateStatus.TemplateId, 10), nil
}

// describeSmsTemplateListHandler 短信模板状态查询
func (h *Handler) describeSmsTemplateListHandler(in models.SmsTemplateListInput, client *tencentSms.Client) (*models.SmsListResponse, error) {
	// 实例短信模板列表
	req := tencentSms.NewDescribeSmsTemplateListRequest()

	// 短信类型
	req.International = common.Uint64Ptr(in.International)

	// 模板ID
	switch len(in.TemplateIdSet) {
	case 0:
		req.Limit = common.Uint64Ptr(in.Limit)
		req.Offset = common.Uint64Ptr(in.Offset)
	default:
		req.TemplateIdSet = common.Uint64Ptrs(in.TemplateIdSet)
	}

	response, err := client.DescribeSmsTemplateList(req)
	if err = h.commonErrorHandler(err); err != nil {
		return nil, err
	}

	// 实例化
	return &models.SmsListResponse{
		Total: len(response.Response.DescribeTemplateStatusSet),
		Rows:  h.describeTemplateStatusSetResponse(response.Response.DescribeTemplateStatusSet),
	}, nil
}
