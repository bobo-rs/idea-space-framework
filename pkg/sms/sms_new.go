package sms

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/pkg/sms/models"
	"gitee.com/bobo-rs/idea-space-framework/pkg/sms/tencent"
)

type (
	// Handler 短信服务处理
	Handler struct {
		driver   string                      // 驱动
		services map[enums.SmsDriver]Service // 短信服务接口列表
	}

	// Service 短信服务接口列表
	Service interface {
		// Ctx 设置上下文
		Ctx(ctx context.Context)

		// Config 短信服务平台配置
		Config(config interface{}) error

		// Send 发送短信
		Send(mobile []string, params models.SendSmsInput) (*models.SendSmsResponse, error)

		// Apply 申请短信模板-添加和修改
		Apply(params models.AddSmsTemplateInput) (*models.AddSmsTemplateResponse, error)

		// Pull 查询拉取短信发送状态
		Pull(params models.PullSendStatusInput) ([]*models.PullSendResultItem, error)

		// List 获取短信列表
		List(params models.SmsTemplateListInput) (*models.SmsListResponse, error)
	}
)

// New 实例短信服务
func New() *Handler {
	return &Handler{
		// 实例化短信服务列表
		services: map[enums.SmsDriver]Service{
			enums.SmsDriverTencent: &tencent.Handler{}, // 腾讯服务
		},
	}
}
