package example

import (
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/pkg/sms"
	"gitee.com/bobo-rs/idea-space-framework/pkg/sms/models"
	"testing"
	"time"
)

var (
	tencentConfig = map[string]interface{}{
		`sms_tencent_secret_id`:  `xxxxx`,
		`sms_tencent_secret_key`: `xxxx`,
		`sms_tencent_sdk_appid`:  `xxxx`,
		`sms_tencent_sign_name`:  `xxxx`,
		`sms_tencent_region`:     `ap-guangzhou`,
	}
	tencentDriver = `sms_tencent`
)

func TestTencentSms_Send(t *testing.T) {

	// 发送短信
	fmt.Println(tencentClient().Send([]string{`17620456789`}, models.SendSmsInput{
		TemplateId:     `1900558`,
		TemplateParams: []string{`123456`},
	}))
}

func TestTencentSms_Pull(t *testing.T) {
	fmt.Println(tencentClient().Pull(models.PullSendStatusInput{
		Limit: 10,
	}))

	// 单一手机号
	fmt.Println(tencentClient().Pull(models.PullSendStatusInput{
		Limit:     10,
		Mobile:    `13456789012`,
		Offset:    0,
		BeginTime: uint64(time.Now().Add(-7).Unix()),
		EndTime:   uint64(time.Now().Unix()),
	}))
}

func TestTencentSms_Apply(t *testing.T) {
	params := models.AddSmsTemplateInput{
		TemplateName:  `验证码`,
		Content:       `您的验证码：{1}`,
		SmsType:       0, // 0验证码，1营销短信
		International: 0, // 0国内，1是否国际或港澳台
		Remark:        `注册验证码`,
	}
	// 添加
	fmt.Println(tencentClient().Apply(params))

	// 编辑
	params.TemplateId = `12323213`
	fmt.Println(tencentClient().Apply(params))
}

func TestTencentSms_List(t *testing.T) {
	fmt.Println(tencentClient().List(models.SmsTemplateListInput{
		International: 0,
		TemplateIdSet: []uint64{123123123},
		Limit:         20,
		Offset:        0,
	}))
}

func tencentClient() sms.Service {
	handler := sms.New().Driver(tencentDriver)
	service, err := handler.Service()
	if err != nil {
		panic(err)
	}
	// 设置配置
	err = service.Config(tencentConfig)
	if err != nil {
		panic(err)
	}
	return service
}
