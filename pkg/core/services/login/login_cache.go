package login

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/consts"
	"gitee.com/bobo-rs/idea-space-framework/pkg/sredis"
	"gitee.com/bobo-rs/idea-space-framework/pkg/utils"
	"time"
)

// GetLoginMacNum 获取同一个MAC登录次数
func (l *sLogin) GetLoginMacNum(ctx context.Context) (int, error) {
	key, err := l.GetLoginMacKey(ctx)
	if err != nil {
		return 0, err
	}
	r, err := sredis.New().Get(ctx, *key)
	if err != nil {
		return 0, err
	}
	return r.Int(), nil
}

// SetLoginMacNum 叠加设置同一个MAC登录次数
func (l *sLogin) SetLoginMacNum(ctx context.Context) error {
	// 读取KEY
	key, err := l.GetLoginMacKey(ctx)
	if err != nil {
		return err
	}
	// 原子增加
	if err = sredis.NewRedis().IncrEx(ctx, *key, time.Minute*30); err != nil {
		return err
	}
	return nil
}

// RemoveLoginMac 移除同一个MAC登录次数
func (l *sLogin) RemoveLoginMac(ctx context.Context) error {
	key, err := l.GetLoginMacKey(ctx)
	if err != nil {
		return err
	}
	if _, err = sredis.New().Del(ctx, *key); err != nil {
		return err
	}
	return nil
}

// GetLoginMacAccountNum 获取同一个登录账户MAC登录次数
func (l *sLogin) GetLoginMacAccountNum(ctx context.Context, account string) (int, error) {
	// 读取KEY
	key, err := l.GetLoginMacAccountKey(ctx, account)
	if err != nil {
		return 0, err
	}
	// 读取数量
	r, err := sredis.New().Get(ctx, *key)
	if err != nil {
		return 0, err
	}
	return r.Int(), nil
}

// SetLoginMacAccountNum 叠加同一个登录账户MAC登录次数
func (l *sLogin) SetLoginMacAccountNum(ctx context.Context, account string) error {
	// 读取KEY
	key, err := l.GetLoginMacAccountKey(ctx, account)
	if err != nil {
		return err
	}
	// 增加叠加数量
	if err = sredis.NewRedis().IncrEx(ctx, *key, time.Minute*30); err != nil {
		return err
	}
	return nil
}

// RemoveLoginMacAccount 删除登录账户MAC登录次数
func (l *sLogin) RemoveLoginMacAccount(ctx context.Context, account string) error {
	// 读取KEY
	key, err := l.GetLoginMacAccountKey(ctx, account)
	if err != nil {
		return err
	}
	// 删除
	if _, err = sredis.New().Del(ctx, *key); err != nil {
		return err
	}
	return nil
}

// GetLoginMacKey 获取当前用户登录MacKEY
func (l *sLogin) GetLoginMacKey(ctx context.Context) (*string, error) {
	key := utils.SafeLoginCacheKey(consts.SafeLoginIp, utils.GetIp(ctx))
	return &key, nil
}

// GetLoginMacAccountKey 获取登录MAC及账号KEY
func (l *sLogin) GetLoginMacAccountKey(ctx context.Context, account string) (*string, error) {
	key := utils.SafeLoginCacheKey(consts.SafeLoginAccountIp, account, utils.GetIp(ctx))
	return &key, nil
}
