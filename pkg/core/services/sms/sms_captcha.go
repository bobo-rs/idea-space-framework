package sms

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/consts"
	"log"
)

// CaptchaValidate 验证码阈值验证
func (s *sSms) CaptchaValidate(ctx context.Context, mobile string) error {
	// 验证缓存1分钟手机号发送数量验证码
	num, err := s.GetCaptcha1MinuteMobileNum(ctx, mobile)
	if err != nil {
		return err
	}
	if num > 0 {
		return errors.New(`验证码已发送`)
	}

	// 同一个手机号1小时发送量
	num, err = s.GetCaptcha1HourMobileNum(ctx, mobile)
	if err != nil {
		return err
	}
	if consts.SmsCaptcha1HourSendNumMobile < num {
		return fmt.Errorf(`同一手机号发送过于频繁%d，锁定一小时`, num)
	}

	// 同一个Mac半小时发送数量
	num, err = s.GetCaptchaHalfHourMacNum(ctx)
	if err != nil {
		return err
	}
	if consts.SmsCaptchaHalfHourSendNumMac < num {
		return fmt.Errorf(`同一Mac发送过于频繁%d，锁定半小时`, num)
	}

	// 验证码发送量
	num, err = s.GetCaptcha1MinuteNum(ctx)
	if err != nil {
		return err
	}
	if consts.SmsCaptcha1MinuteSendTotal < num {
		return fmt.Errorf(`验证码发送一分钟内系统繁忙[%d]，请稍后再试`, num)
	}

	// 验证码1小时发送量
	num, err = s.GetCaptcha1HourNum(ctx)
	if err != nil {
		return err
	}
	if consts.SmsCaptcha1HourSendTotal < num {
		return fmt.Errorf(`验证码发送超过阈值[%d]，自动锁定一小时`, num)
	}

	// 24小时内发送量
	num, err = s.GetCaptcha24HourNum(ctx)
	if err != nil {
		return err
	}
	if consts.SmsCaptcha24HourSendTotal < num {
		return fmt.Errorf(`当日验证码发送超过阈值[%d], 请稍后再来`, num)
	}
	return nil
}

// ValidateCode 通过手机号校验验证码
func (s *sSms) ValidateCode(ctx context.Context, mobile, code string) error {
	if len(code) == 0 {
		return errors.New(`请输入验证码`)
	}
	// 获取验证码
	captchaCode, err := s.GetCaptchaCode(ctx, mobile)
	if err != nil {
		return err
	}
	// 是否匹配
	if *captchaCode != code {
		return errors.New(`验证码验证不通过`)
	}
	// 验证通过，则销毁数据，避免重复使用
	s.RemoveCaptchaCode(ctx, mobile)
	return nil
}

// AfterCaptchaSendAsync 异步统计验证码发送量
func (s *sSms) AfterCaptchaSendAsync(ctx context.Context, mobile string) {
	// 设置手机号1分钟发送量
	err := s.SetCaptcha1MinuteMobileNum(ctx, mobile)
	// 设置同一个手机号发送量
	err = s.SetCaptcha1HourMobileNum(ctx, mobile)
	// 设置同一个MAC地址1分钟发送量
	err = s.SetCaptcha1MinuteMacSendNum(ctx)
	// 设置同一个MAC半小时发送量
	err = s.SetCaptchaHalfHourMacNum(ctx)
	// 设置1分钟内发送量
	err = s.SetCaptcha1MinuteNum(ctx)
	// 设置1小时发送量
	err = s.SetCaptcha1HourNum(ctx)
	// 设置24小时发送量
	err = s.SetCaptcha24HourNum(ctx)
	if err != nil {
		log.Println(`异步统计发送量`, err)
	}
}
