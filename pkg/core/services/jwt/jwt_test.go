package jwt

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/pkg/jwt"
	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	"testing"
	"time"
)

func TestService(t *testing.T) {
	service := New(context.Background())
	tokenItem, err := service.CreateToken(jwt.AccountDetail{
		Uid:      10001,
		Account:  `idea-test-001`,
		Nickname: `001号员工`,
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	// 输出结果
	fmt.Println(`TokenItemResult`, tokenItem)
	// 解析Token
	fmt.Println(`解析Token`)
	fmt.Println(service.ParseToken(tokenItem.Token))

	// 验证Token
	fmt.Println(`VerifyToken1`, service.Verify(context.Background(), tokenItem.Token))
	fmt.Println(`VerifyToken2`, service.VerifyHook(tokenItem.Token))
}

func TestService_Refresh(t *testing.T) {
	token := `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVaWQiOjEwMDAxLCJBY2NvdW50IjoiaWRlYS10ZXN0LTAwMSIsIk5pY2tuYW1lIjoiMDAx5Y-35ZGY5belIiwiUmVmcmVzaENvdW50IjowLCJpc3MiOiJsb2NhbGhvc3QiLCJzdWIiOiJhZG1pbiIsImV4cCI6MTcxNTA2Mzc4NywibmJmIjoxNzE0NDU4OTg3LCJpYXQiOjE3MTQ0NTg5ODd9.qHPGud7FxnaaSRUNHIxNGSyPCLFEMgv_pxl9WhUzv-k`
	fmt.Println(New(context.Background()).RefreshToken(token))
	expireDate := `2024-05-07 14:38:09`
	expire, _ := time.Parse(time.DateTime, expireDate)
	now := time.Now()
	fmt.Println(expire, now, expire.Sub(now))
	fmt.Println(fmt.Sprintf(`expire:%d；nowTime:%d, diff:%d`, expire.Unix(), now.Unix(), expire.Unix()-now.Unix()))

	seven := time.Hour * 24 * time.Duration(7)
	fmt.Println(seven, time.Now().Add(seven).Unix())
}

func TestService_Remove(t *testing.T) {
	token := `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVaWQiOjEwMDAxLCJBY2NvdW50IjoiaWRlYS10ZXN0LTAwMSIsIk5pY2tuYW1lIjoiMDAx5Y-35ZGY5belIiwiUmVmcmVzaENvdW50IjowLCJpc3MiOiJsb2NhbGhvc3QiLCJzdWIiOiJhZG1pbiIsImV4cCI6MTcxNTA2MjAzMywibmJmIjoxNzE0NDU3MjMzLCJpYXQiOjE3MTQ0NTcyMzN9.djoy2OwZwHJBa5YA5GjZ5pSgjs3SdsrpW45EULAivas`
	service := New(context.Background())
	fmt.Println(service.RemoveHook(token))
}

func TestService_Read(t *testing.T) {
	token := `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVaWQiOjEwMDAxLCJBY2NvdW50IjoiaWRlYS10ZXN0LTAwMSIsIk5pY2tuYW1lIjoiMDAx5Y-35ZGY5belIiwiUmVmcmVzaENvdW50IjowLCJpc3MiOiJsb2NhbGhvc3QiLCJzdWIiOiJhZG1pbiIsImV4cCI6MTcxNTA2MjAzMywibmJmIjoxNzE0NDU3MjMzLCJpYXQiOjE3MTQ0NTcyMzN9.djoy2OwZwHJBa5YA5GjZ5pSgjs3SdsrpW45EULAivas`
	service := New(context.Background())
	fmt.Println(service.ReadHook(token))
}

func TestService_Verify(t *testing.T) {
	token := `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVaWQiOjEwMDAxLCJBY2NvdW50IjoiaWRlYS10ZXN0LTAwMSIsIk5pY2tuYW1lIjoiMDAx5Y-35ZGY5belIiwiUmVmcmVzaENvdW50IjowLCJpc3MiOiJsb2NhbGhvc3QiLCJzdWIiOiJhZG1pbiIsImV4cCI6MTcxNTA2MjAzMywibmJmIjoxNzE0NDU3MjMzLCJpYXQiOjE3MTQ0NTcyMzN9.djoy2OwZwHJBa5YA5GjZ5pSgjs3SdsrpW45EULAivas`
	service := New(context.Background())
	fmt.Println(service.VerifyHook(token))
}
