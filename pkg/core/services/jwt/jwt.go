package jwt

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/pkg/config"
	"gitee.com/bobo-rs/idea-space-framework/pkg/jwt"
)

// New 实例JWT服务
func New(ctx context.Context) *jwt.JWTHandler {
	// 读取配置
	claims, err := config.New().GetJWTConfigDefault(ctx)
	if err != nil {
		panic(err)
	}
	// 调用JWT服务
	return jwt.New(claims).Ctx(ctx).Hook(jwt.HookHandler{
		Create: createHook,
		Read:   readHook,
		Remove: removeHook,
		Verify: verifyHook,
	})
}
