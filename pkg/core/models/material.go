package models

import "image"

type (
	// MaterialDesignOverlayItem 素材模板设计合成
	MaterialDesignOverlayItem struct {
		UnderImg   image.Image // 底图图片信息
		UnderSrc   string      // 底图路径
		TemplateId uint        // 匹配素材模板ID
		FontPath   string      // 字体路径
		MaterialDesignSaveItem
		DrawMaterialWords []MaterialWordsItem // 需要绘制素材文本列表
	}

	// MaterialDesignResultItem 素材设计图结果属性
	MaterialDesignResultItem struct {
		TemplateId    uint   `json:"template_id" dc:"模板ID"`    // 模板ID
		Src           string `json:"src" dc:"文件地址"`            // 设计完成保存图地址
		ContentPrefix string `json:"content_prefix" dc:"内容前缀"` // 设计图内容前缀（Base64）
		Content       string `json:"content" dc:"设计图Base64"`   // 设计图内容
	}

	// MaterialWordsItem 素材文本属性
	MaterialWordsItem struct {
		Src           string  // 素材地址
		Text          string  // 素材文本
		MaterialX     int     // 素材X坐标-底图
		MaterialY     int     // 素材Y坐标-底图
		MaterialColor string  // 素材色系-自动绘制
		LayerPosition uint    // 图层位置
		FontSize      float64 // 文本字体大小
		TextFontColor string  // 文本色系
		TextX         int     // 文本X坐标-素材图
		TextY         int     // 文本Y坐标-素材图
		TextType      uint    // 文本类型：0空白（无需绘制）, 1文本（无需分割），2分割文本（如：价格）
		Sep           string  // 文本分割符
	}

	// MaterialDesignSaveItem 素材设计保存属性
	MaterialDesignSaveItem struct {
		IsSave        bool   // 是否保存：false不保存，true保存
		DesignContent string // 设计图内容
		Dir           string // 保存文件附件地址
		Filename      string // 保存文件名
	}
)
