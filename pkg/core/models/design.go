package models

type (
	// MatchDesignTemplateItem 匹配后的模板数据
	MatchDesignTemplateItem struct {
		MatchTid  []uint                       // 匹配后模板ID
		MatchList []DesignTemplateIdAndNumItem // 匹配后的模板列表
	}

	// DesignTemplateIdAndNumItem 设计模板ID和数量
	DesignTemplateIdAndNumItem struct {
		Id  uint `json:"id" dc:"模板ID"`
		Num uint `json:"num" dc:"数量值（切词数量，卖点数量）"`
	}

	// DesignTemplateWordsItem 设计目标文本属性
	DesignTemplateWordsItem struct {
		WordsId uint   `json:"words_id" dc:"文本ID"`
		Num     uint   `json:"num" dc:"文本数量（每一个文本卖点长度）"`
		Value   string `json:"value" dc:"文本（卖点）"`
	}
)
