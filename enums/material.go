package enums

type (
	MaterialStatusEnums uint // 素材状态
	MaterialPositionEnums string // 素材位置
	MaterialTypeEnums uint // 素材类型
)

// 素材状态
const (
	MaterialStatusOk MaterialStatusEnums = iota
	MaterialStatusDiscard
)

// 素材类型
const (
	MaterialTypeMate MaterialTypeEnums = iota
	MaterialTypeMain
)

// 素材位置：main主图，market营销活动卖点，top顶部卖点，
// btm-main底部主卖点，btm-second底部次卖点，btm-single底部单独卖点，detail详情图卖点
const (
	MaterialPositionMain MaterialPositionEnums = `main`
	MaterialPositionMarket MaterialPositionEnums = `market`
	MaterialPositionTop MaterialPositionEnums = `top`
	MaterialPositionBtmMain MaterialPositionEnums = `btm-main`
	MaterialPositionBtmSecond MaterialPositionEnums = `btm-second`
	MaterialPositionBtmSingle MaterialPositionEnums = `btm-single`
	MaterialPositionDetail MaterialPositionEnums = `detail`
)


