package enums

type (
	PermAssocType           int
	PermDisabled            int
	PermShow                int
	PermMenuType            int
	PermCustomUpdateColumns int
	RoleStatus              int
	DepartStatus            int
	AdmCustomUpdateColumns  int
)

const (
	PermAssocTypeRoles PermAssocType = iota
	PermAssocTypeUser
)

const (
	PermDisabledOk PermDisabled = iota
	PermDisabledLock
)

const (
	PermShowOk PermShow = iota
	PermShowHide
)

const (
	PermMenuTypeMain PermMenuType = iota
	PermMenuTypeSub
	PermMenuTypeAction
	PermMenuTypeAuth
)

const (
	PermUpdateColumnsShow PermCustomUpdateColumns = iota
	PermUpdateColumnsDisabled
	PermUpdateColumnsSort
)

const (
	AdmUpdateColumnsStatus AdmCustomUpdateColumns = iota
	AdmUpdateColumnsIsSuper
)

const (
	RoleStatusOk RoleStatus = iota
	RoleStatusLock
)

const (
	DepartStatusOk DepartStatus = iota
	DepartStatusDisabled
)

func (t PermAssocType) Fmt() string {
	switch t {
	case PermAssocTypeRoles:
		return `角色关联`
	case PermAssocTypeUser:
		return `账户关联`
	default:
		return ``
	}
}

func (d PermDisabled) Fmt() string {
	switch d {
	case PermDisabledOk:
		return `正常`
	case PermDisabledLock:
		return `禁用`
	default:
		return ``
	}
}

func (s PermShow) Fmt() string {
	switch s {
	case PermShowOk:
		return `显示`
	case PermShowHide:
		return `隐藏`
	default:
		return ``
	}
}

func (t PermMenuType) Fmt() string {
	switch t {
	case PermMenuTypeMain:
		return `主菜单`
	case PermMenuTypeSub:
		return `子菜单`
	case PermMenuTypeAction:
		return `页面操作`
	case PermMenuTypeAuth:
		return `数据授权`
	default:
		return ``
	}
}

func (c PermCustomUpdateColumns) Fmt() string {
	switch c {
	case PermUpdateColumnsShow:
		return `is_show`
	case PermUpdateColumnsDisabled:
		return `is_disabled`
	case PermUpdateColumnsSort:
		return `sort`
	default:
		return ``
	}
}

func (c AdmCustomUpdateColumns) Fmt() string {
	switch c {
	case AdmUpdateColumnsStatus:
		return `manage_status`
	case AdmUpdateColumnsIsSuper:
		return `is_supper_manage`
	default:
		return ``
	}
}

func (s RoleStatus) Fmt() string {
	switch s {
	case RoleStatusOk:
		return `正常`
	case RoleStatusLock:
		return `锁定`
	default:
		return ``
	}
}

func (s DepartStatus) Fmt() string {
	switch s {
	case DepartStatusOk:
		return `正常`
	case DepartStatusDisabled:
		return `禁用`
	default:
		return ``
	}
}
