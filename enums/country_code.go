package enums

type CountryCode string

const (
	CountryCodeChina CountryCode = `+86` // 中国区号
)
