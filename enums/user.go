package enums

type (
	UserStatus uint
)

const (
	UserStatusOk UserStatus = iota
	UserStatusLock
	UserStatusLogout
)

func (s UserStatus) Fmt() string {
	switch s {
	case UserStatusOk:
		return `正常`
	case UserStatusLock:
		return `已锁定`
	case UserStatusLogout:
		return `待注销`
	default:
		return ``
	}
}
