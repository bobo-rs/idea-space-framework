package enums

type ErrorCode int

const (
	ErrorOK                  ErrorCode = 0
	ErrorUnify               ErrorCode = 10000
	ErrorNotLogoutIn         ErrorCode = 11000
	ErrorLogoutIn            ErrorCode = 11001
	ErrorLoginErrThree       ErrorCode = 11002
	ErrorLoginErrLock        ErrorCode = 11003
	ErrorNotAdmin            ErrorCode = 11004
	ErrorForgotNotCaptcha    ErrorCode = 11005
	ErrorUpdatePwdNotCaptcha ErrorCode = 11006
	ErrorForbidden           ErrorCode = 400003
)

func (c ErrorCode) Fmt() string {
	switch c {
	case ErrorOK:
		return `OK`
	case ErrorUnify:
		return `业务错误`
	case ErrorNotLogoutIn:
		return `请登录`
	case ErrorLogoutIn:
		return `已登录`
	case ErrorLoginErrThree:
		return `登录错误三次以上`
	case ErrorLoginErrLock:
		return `账户登录已被锁定`
	case ErrorNotAdmin:
		return `未授权系统管理员`
	case ErrorForgotNotCaptcha:
		return `找回密码-未校验验证码`
	case ErrorUpdatePwdNotCaptcha:
		return `修改密码-未校验验证码`
	case ErrorForbidden:
		return `权限不足，禁止访问`
	default:
		return `Not Code Description`
	}
}
