package enums

type (
	SmsType   uint
	SmsAlias  string
	SmsStatus int
)

const (
	SmsTypeCaptcha SmsType = iota
	SmsTypeMessage
	SmsTypeMarket
)

const (
	SmsAliasRegisterCaptcha  SmsAlias = `reg-captcha`
	SmsAliasForgotPwdCaptcha SmsAlias = `forgot-pwd-captcha`
	SmsAliasUptPwdCaptcha    SmsAlias = `upt-pwd-captcha`
)

const (
	SmsStatusOff SmsStatus = iota
	SmsStatusOk
)

// String 格式化短信类型
func (t SmsType) Fmt() string {
	switch t {
	case SmsTypeCaptcha:
		return `验证码`
	case SmsTypeMessage:
		return `消息通知`
	case SmsTypeMarket:
		return `营销短信`
	default:
		return ``
	}
}

// String 格式化短信别名
func (a SmsAlias) Fmt() string {
	switch a {
	case SmsAliasRegisterCaptcha:
		return `注册验证码`
	case SmsAliasForgotPwdCaptcha:
		return `找回密码`
	case SmsAliasUptPwdCaptcha:
		return `修改密码`
	default:
		return ``
	}
}

// String SMS模板状态
func (s SmsStatus) Fmt() string {
	switch s {
	case SmsStatusOff:
		return `关闭`
	case SmsStatusOk:
		return `开启`
	default:
		return ``
	}
}
