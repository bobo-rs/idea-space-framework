// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/framework/model"

	"gitee.com/bobo-rs/idea-space-framework/pkg/config"
)

type (
	IUpload interface {
		// ProcessLocalUpload 处理本地上传文件
		ProcessLocalUpload(ctx context.Context, in model.UploadFileInput, item *model.UploadAttachmentItem) (err error)
		// FileUpload 文件上传
		FileUpload(ctx context.Context, in model.UploadFileInput) (out *model.UploadFileOutput, err error)
		// GetConfig 读取文件上传配置
		GetConfig(ctx context.Context) (rawConfig *config.UploadConfigItem, err error)
	}
)

var (
	localUpload IUpload
)

func Upload() IUpload {
	if localUpload == nil {
		panic("implement not found for interface IUpload, forgot register?")
	}
	return localUpload
}

func RegisterUpload(i IUpload) {
	localUpload = i
}
