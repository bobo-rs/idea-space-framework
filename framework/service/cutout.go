// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

type (
	ICutout interface{}
)

var (
	localCutout ICutout
)

func Cutout() ICutout {
	if localCutout == nil {
		panic("implement not found for interface ICutout, forgot register?")
	}
	return localCutout
}

func RegisterCutout(i ICutout) {
	localCutout = i
}
