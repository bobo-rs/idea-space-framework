// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/logic/base"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
)

type (
	ISmsTpl interface {
		// CurrSyncOutFromTemplateStatus 同步当前SMS平台模板状态
		CurrSyncOutFromTemplateStatus(ctx context.Context) error
		// CurrApplyOutFromTemplate 同步当前SMS服务模板到外部平台
		CurrApplyOutFromTemplate(ctx context.Context, tid uint) error
		// CurrTemplateByAlias 通过模板变量获取当前模板信息-模板ID和模板内容（发送短信）
		CurrTemplateByAlias(ctx context.Context, varAlias enums.SmsAlias) (*model.CurrTemplateItem, error)
		// CurrTemplateIdByAlias 通过模板变量获取当前配置SMS服务模板ID
		CurrTemplateIdByAlias(ctx context.Context, varAlias enums.SmsAlias) (*string, error)
		// CurrTemplateOutMapByAlias 通过模板变量获取当前SMS平台模板MAP
		CurrTemplateOutMapByAlias(ctx context.Context, varAlias ...string) (map[string]*entity.SmsTemplateOut, error)
		// CurrFromSmsDriver 获取短信平台SMS驱动
		CurrFromSmsDriver(ctx context.Context) (*string, error)
		// SaveRecord 保存短信记录
		SaveRecord(ctx context.Context, records ...entity.SmsRecord) error
		// GetRecordList 获取短信发送记录
		GetRecordList(ctx context.Context, in model.SmsRecordListInput) (out *model.SmsRecordListOutput, err error)
		// ProcessRecordScanByMobiles 通过手机号获取短信记录
		ProcessRecordScanByMobiles(ctx context.Context, mobile []string, scan interface{}) error
		// ProcessRecordNewByMobile 通过手机号获取最新发送记录
		ProcessRecordNewByMobile(ctx context.Context, mobile string) (*model.SmsSendRecordNewItem, error)
		// SendCaptcha 发送短信验证码
		SendCaptcha(ctx context.Context, mobile string, varAlias enums.SmsAlias) error
		// ValidateCode 短信验证码检验
		ValidateCode(ctx context.Context, mobile, code string) error
		// SendSmsMessage 短信发送-消息通知
		SendSmsMessage(ctx context.Context, item model.SendSmsTemplateItem, mobile ...string) error
		// SendSmsMarket 短信发送-营销短信
		SendSmsMarket(ctx context.Context, item model.SendSmsTemplateItem, mobile ...string) error
		// GetTemplateList 获取SMS模板列表
		GetTemplateList(ctx context.Context, in model.SmsTemplateListInput) (out *model.SmsTemplateListOutput, err error)
		// GetTemplateDetail 获取SMS短信模板详情
		GetTemplateDetail(ctx context.Context, id uint) (out *model.SmsTemplateDetailOutput, err error)
		// SaveTemplate 保存短信模板数据
		SaveTemplate(ctx context.Context, tmpl entity.SmsTemplate) error
		// DeleteTemplate 通过模板ID删除短信模板数据
		DeleteTemplate(ctx context.Context, tid uint) error
		// GetTemplateOutList 获取外部SMS平台模板列表
		GetTemplateOutList(ctx context.Context, in model.SmsTemplateOutListInput) (out *model.SmsTemplateOutListOutput, err error)
		// SaveTemplateOut 保存平台SMS短信模板数据
		SaveTemplateOut(ctx context.Context, out entity.SmsTemplateOut) error
		// DeleteTemplateOutByVarAlias 根据短信模板变量上传外部平台模板数据
		DeleteTemplateOutByVarAlias(ctx context.Context, varAlias enums.SmsAlias) error
		// ApplyOutFromTemplate 同步模板到外部平台
		ApplyOutFromTemplate(ctx context.Context, tid uint, fromType string) error
		// SyncOutFromTemplateStatus 同步更新外部平台模板状态
		SyncOutFromTemplateStatus(ctx context.Context, fromType string) error
		// SendSmsOutFrom 发送短信-提交短信到第三方平台
		SendSmsOutFrom(ctx context.Context, in model.SendOutFromSmsItem) error
		// ProcessTemplateIdByAliasAndFrom 通过变量别名和平台类型获取模板ID
		ProcessTemplateIdByAliasAndFrom(ctx context.Context, varAlias, fromType string) (*string, error)
		// ProcessTemplateOutMapByAlias 通过模板变量获取外部模板数据MAP
		ProcessTemplateOutMapByAlias(ctx context.Context, fromType string, varAlias ...string) (map[string]*entity.SmsTemplateOut, error)
		// ProcessTemplateDetailById 通过模板ID获取SMS模板详情
		ProcessTemplateDetailById(ctx context.Context, id uint) (*entity.SmsTemplate, error)
		// CheckTemplateExistsById 通过模板ID检测模板是否存在
		CheckTemplateExistsById(ctx context.Context, tid uint) (bool, error)
		// GetTemplateByAlias 根据模板变量获取短信模板-发送短信模板内容
		GetTemplateByAlias(ctx context.Context, varAlias enums.SmsAlias) (*model.CurrTemplateItem, error)
		// RemoveVarAlias 根据SMS变量删除SMS模板数据
		RemoveVarAlias(ctx context.Context)
		// RecordModel 短信记录model查询
		RecordModel() *base.TblBaseService
		// TemplateModel 短信模板model查询
		TemplateModel() *base.TblBaseService
		// OutModel 外部短信模板Model查询
		OutModel() *base.TblBaseService
		// TemplateWhere 短信模板查询条件
		TemplateWhere(in model.SmsTemplateWhereItem) map[string]interface{}
		// RecordWhere 短信记录查询条件
		RecordWhere(in model.SmsRecordWhereItem) map[string]interface{}
		// FormatTemplate 格式化短信模板
		FormatTemplate(detail *model.SmsTemplateItem)
		// FormatRecord 格式化短信记录
		FormatRecord(detail *model.SmsRecordDetailItem)
	}
)

var (
	localSmsTpl ISmsTpl
)

func SmsTpl() ISmsTpl {
	if localSmsTpl == nil {
		panic("implement not found for interface ISmsTpl, forgot register?")
	}
	return localSmsTpl
}

func RegisterSmsTpl(i ISmsTpl) {
	localSmsTpl = i
}
