// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/models"
)

type (
	ICommon interface {
		// CommonMaps 公共map列表
		CommonMaps(ctx context.Context, mapKey ...string) (map[string]interface{}, error)
		// GetSign 获取安全规则签名
		GetSign(ctx context.Context, tag enums.SafeTags) (*models.SafeSignItem, error)
		// Verify 验证安全规则签名属性
		Verify(ctx context.Context, sign string, tag enums.SafeTags, isRemove ...bool) (*models.SignItem, error)
		// EncryptExampleValue 安全签名-加密示例值（例如：登录密码、手机号加密等）
		EncryptExampleValue(ctx context.Context, text, secret string) (string, error)
	}
)

var (
	localCommon ICommon
)

func Common() ICommon {
	if localCommon == nil {
		panic("implement not found for interface ICommon, forgot register?")
	}
	return localCommon
}

func RegisterCommon(i ICommon) {
	localCommon = i
}
