// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
)

type (
	IAttachment interface {
		// ProcessAttachmentListAndTotal 处理并获取附件列表和总数
		ProcessAttachmentListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, list interface{}, total *int) error
		// ProcessAttachmentDetailByHasher 处理并通过Hash值获取附件详情
		ProcessAttachmentDetailByHasher(ctx context.Context, hasher string, detail interface{}) error
		// ProcessAttachmentDetailByAttachId 处理并通过附件ID值获取附件详情
		ProcessAttachmentDetailByAttachId(ctx context.Context, attachId string, detail interface{}) error
		// ProcessAttachmentDetail 处理并通过值获取附件详情
		ProcessAttachmentDetail(ctx context.Context, where interface{}, detail interface{}) error
		// CheckAttachmentExistsByHasher 通过hasher检测附件是否存在
		CheckAttachmentExistsByHasher(ctx context.Context, hasher string) bool
		// CheckAttachmentExistsByAttachId 通过附件ID检测附件是否存在
		CheckAttachmentExistsByAttachId(ctx context.Context, attachId string) bool
		// CheckAttachmentExists 检测附件是否存在
		CheckAttachmentExists(ctx context.Context, where interface{}) (bool, error)
		// ProcessListAndTotal 获取文件列表和数量
		ProcessListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, list interface{}, total *int) error
		// ProcessScan 扫描文件数据
		ProcessScan(ctx context.Context, where, scan interface{}, sort ...string) error
		// ProcessDetailById 通过文件ID获取文件详情
		ProcessDetailById(ctx context.Context, id uint, detail interface{}) error
		// ProcessDetailByAttachId 通过附件ID获取文件详情
		ProcessDetailByAttachId(ctx context.Context, attachId string, detail interface{}) error
		// ProcessTotal 获取附件总数
		ProcessTotal(ctx context.Context, where interface{}) (int, error)
		// CheckExists 检查附件是否存在
		CheckExists(ctx context.Context, where interface{}) bool
		// CheckExistsById 通过文件ID检查附件是否存在
		CheckExistsById(ctx context.Context, id uint) bool
		// CheckExistsByAttachId 通过附件ID检查附件是否存在
		CheckExistsByAttachId(ctx context.Context, attachId string) bool
		// ProcessAttachUrlByAttachId 通过附件ID获取附件地址
		ProcessAttachUrlByAttachId(ctx context.Context, attachId string) (string, error)
		// SaveAttachment 保存文件信息
		SaveAttachment(ctx context.Context, attachs ...entity.Attachment) (attachId uint, err error)
	}
)

var (
	localAttachment IAttachment
)

func Attachment() IAttachment {
	if localAttachment == nil {
		panic("implement not found for interface IAttachment, forgot register?")
	}
	return localAttachment
}

func RegisterAttachment(i IAttachment) {
	localAttachment = i
}
