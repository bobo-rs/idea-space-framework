// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/framework/model"
)

type (
	ISpace interface {
		// FormatSpaceDetail 格式化设计空间详情
		FormatSpaceDetail(detail *model.SpaceImageItem)
		// FormatSpaceImageWhere 格式化设计空间查询条件
		FormatSpaceImageWhere(ctx context.Context, in model.SpaceListWhereItem) (where map[string]interface{})
		// GetSpaceImageList 获取设计空间图片列表
		GetSpaceImageList(ctx context.Context, in model.SpaceListInput) (out *model.SpaceListOutput, err error)
		// GetSpaceImageDetail 获取设计空间图片详情
		GetSpaceImageDetail(ctx context.Context, spaceId uint) (out *model.SpaceDetailOutput, err error)
		// GetSpaceDesignList 获取设计空间列表
		GetSpaceDesignList(ctx context.Context, in model.SpaceDesignListInput) (out *model.SpaceDesignListOutput, err error)
		// GetSpaceDesignDetail 获取设计空间详情
		GetSpaceDesignDetail(ctx context.Context, spaceId uint) (out *model.SpaceDetailItem, err error)
		// SaveSpaceImage 保存设计图片数据
		SaveSpaceImage(ctx context.Context, design model.SpaceDetailItem) (sid int64, err error)
		// IncSpaceDesignBrowse 设计空间增量浏览量
		IncSpaceDesignBrowse(ctx context.Context, id uint) error
		// IncSpaceDesign 增量设计空间字段数值
		IncSpaceDesign(ctx context.Context, id []uint, columns string, amount ...int) error
		// DecSpaceDesign 设计空间减量字段数值
		DecSpaceDesign(ctx context.Context, id []uint, columns string, amount ...int) error
		// IncOrDec 增量或者减量处理
		IncOrDec(ctx context.Context, in model.CommonIncOrDecByIdItem) error
		// ProcessAttrScan 扫描图片空间属性数据
		ProcessAttrScan(ctx context.Context, where, scan interface{}) error
		// ProcessAttrByAttachId 通过附件ID获取设计图片数据
		ProcessAttrByAttachId(ctx context.Context, attachId string, detail interface{}) error
		// ProcessAttrBySid 通过设计图片ID获取设计图片属性
		ProcessAttrBySid(ctx context.Context, sid uint, detail interface{}) error
		// CheckSpaceImageDetailExists 检测图片设计详情是否存在
		CheckSpaceImageDetailExists(ctx context.Context, where interface{}) (bool, error)
		// CheckSpaceImageDetailExistsBySpaceId 根据图片ID检测图片设计详情是否存在
		CheckSpaceImageDetailExistsBySpaceId(ctx context.Context, spaceId uint) bool
		// CheckSpaceImageDetailExistsByTid 根据素材模板ID检测图片设计详情是否存在
		CheckSpaceImageDetailExistsByTid(ctx context.Context, tid uint) bool
		// ProcessSpaceImageDetailScan 扫描设计图片空间数据
		ProcessSpaceImageDetailScan(ctx context.Context, where, scan interface{}) error
		// ProcessSpaceImageDetailDetailByTid 根据模板ID获取图片详情
		ProcessSpaceImageDetailDetailByTid(ctx context.Context, tid uint, detail interface{}) error
		// ProcessDetailDetailById 根据设计空间ID获取详情
		ProcessDetailDetailById(ctx context.Context, id uint, detail interface{}) error
		// ProcessSpaceImageListAndTotal 获取设计空间图片列表和总数
		ProcessSpaceImageListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, list interface{}, total *int) error
		// ProcessSpaceImageScan 获取设计空间图片扫描数据
		ProcessSpaceImageScan(ctx context.Context, where, scan interface{}) error
		// ProcessSpaceImageList 获取设计空间图片列表
		ProcessSpaceImageList(ctx context.Context, where, list interface{}) error
		// ProcessSpaceImageDetail 获取设计空间图片详情
		ProcessSpaceImageDetail(ctx context.Context, where, detail interface{}) error
		// ProcessSpaceImageDetailById 根据DI获取设计空间图片详情
		ProcessSpaceImageDetailById(ctx context.Context, id uint, detail interface{}) error
		// ProcessSpaceImageDetailWith 设计空间图片详情-模型关联
		ProcessSpaceImageDetailWith(ctx context.Context, where interface{}, item *model.SpaceDetailItem) error
		// CheckSpaceImageExists 检测图片设计基础谢谢是否存在
		CheckSpaceImageExists(ctx context.Context, where interface{}) (bool, error)
		// CheckSpaceImageExistsById 根据图片ID检测图片设计基础谢谢是否存在
		CheckSpaceImageExistsById(ctx context.Context, id uint) bool
	}
)

var (
	localSpace ISpace
)

func Space() ISpace {
	if localSpace == nil {
		panic("implement not found for interface ISpace, forgot register?")
	}
	return localSpace
}

func RegisterSpace(i ISpace) {
	localSpace = i
}
