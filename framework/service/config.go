// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
	"github.com/gogf/gf/v2/frame/g"
)

type (
	IConfig interface {
		// SaveConfig 保存配置数据
		SaveConfig(ctx context.Context, params ...entity.Config) error
		// RemoveConfigCache 清除配置缓存
		RemoveConfigCache(ctx context.Context, keys ...string)
		// GetVar 读取业务配置
		// 获取后台配置业务配置，例如：系统设置，基础配置，短信配置，第三方授权配置，支付配置等
		// Example：
		// New().GetVar(ctx, `basic`, `keywords`) // 配置名+配置组模式：获取基础配置类型下，关键词配置
		// New().GetVar(ctx, `keywords`) // 配置名模式：直接获取关键词配置（注：确保配置名唯一）
		GetVar(ctx context.Context, names ...string) (*g.Var, error)
		// GetGroupVar 获取业务配置组
		// 获取业务后台配置的配置组，例如：基础配置，短信配置，支付配置等
		// Example：
		// New().GetGroupVar(ctx, `basic`) // 获取所有基础配置
		// New().GetGroupVar(ctx, `sms_tencent`) // 腾讯短信配置
		// Output：&{`appid`:`xxxx`, `secret`:`xxxx`....}
		GetGroupVar(ctx context.Context, groupName string) (*g.Var, error)
		// MustGetGroupVar 获取配置组
		MustGetGroupVar(ctx context.Context, groupName string) *g.Var
		// MustGetVar 获取业务配置Var类型
		MustGetVar(ctx context.Context, names ...string) *g.Var
		// String 获取配置强制转换String类型
		String(ctx context.Context, names ...string) string
		// Uint 获取配置值并强制为Uint类型
		Uint(ctx context.Context, names ...string) uint
		// Int 配置内容强制转换为Int类型
		Int(ctx context.Context, names ...string) int
		// Map 获取配置组并转换为Map类型
		Map(ctx context.Context, groupName string) map[string]interface{}
		// GroupTreeList 获取配置组树形列表
		GroupTreeList(ctx context.Context, in model.ConfigGroupTreeListInput) (treeList []model.ConfigGroupTreeItem, err error)
		// GroupDetail 获取配置详情
		GroupDetail(ctx context.Context, id uint) (detail *model.ConfigGroupCategoryItem, err error)
		// SaveConfigGroup 保存配置组
		SaveConfigGroup(ctx context.Context, params ...entity.ConfigGroup) error
		// ProcessGroupScan 扫描配置组数据
		ProcessGroupScan(ctx context.Context, where, scan interface{}) error
		// ProcessGroupGroupNameById 通过配置组ID获取配置组关联名
		ProcessGroupGroupNameById(ctx context.Context, id ...uint) (map[uint]string, error)
		// ProcessGroupTotal 获取配置组总数
		ProcessGroupTotal(ctx context.Context, where interface{}) (int, error)
		// ProcessGroupDetailById 获取配置组详情
		ProcessGroupDetailById(ctx context.Context, id uint, detail interface{}) error
		// ProcessGroupDetailByGroupName 根据配置组名获取配置详情
		ProcessGroupDetailByGroupName(ctx context.Context, groupName string, detail interface{}) error
		// ProcessGroupExists 验证配置组是否存在
		ProcessGroupExists(ctx context.Context, where interface{}) (bool, error)
		// ProcessGroupExistsById 根据配置组ID验证配置组是否存在
		ProcessGroupExistsById(ctx context.Context, id uint) bool
		// ProcessGroupExistsByGroupName 根据配置组组名验证配置组是否存在
		ProcessGroupExistsByGroupName(ctx context.Context, name string) bool
		// List 获取配置列表
		List(ctx context.Context, in model.ConfigListInput) (rows []model.ConfigDetailItem, err error)
		// ProcessConfigScan 处理并扫描业务配置
		ProcessConfigScan(ctx context.Context, where, scan interface{}) error
		// ProcessConfigByGroup 根据配置组名获取配置组数据
		ProcessConfigByGroup(ctx context.Context, name string, group interface{}) error
		// ProcessConfigMapByGroup 根据配置组获取配置组MAP
		ProcessConfigMapByGroup(ctx context.Context, name string) (map[string]model.ConfigNameValueItem, error)
		// ProcessTotal 统计配置总数
		ProcessTotal(ctx context.Context, where interface{}) (int, error)
		// ProcessExists 检测配置是否存在
		ProcessExists(ctx context.Context, where interface{}) (bool, error)
		// ProcessExistsByGroupName 根据配置组名检测配置是否存在
		ProcessExistsByGroupName(ctx context.Context, groupName string) bool
		// ProcessExistsByGroupId 根据配置组ID检测配置是否存在
		ProcessExistsByGroupId(ctx context.Context, groupId uint) bool
		// ProcessConfigValueByNameAndGroup 根据配置名和配置组获取配置值
		ProcessConfigValueByNameAndGroup(ctx context.Context, name, groupName string) (*string, error)
		// ProcessConfigIdByNameAndGroup 通过配置名和配置组ID获取配置ID
		ProcessConfigIdByNameAndGroupId(ctx context.Context, name []string, groupId []uint) (map[string]uint, error)
		// ProcessConfigDetailMapByName 通过配置名获取配置详情MAP
		ProcessConfigDetailMapByName(ctx context.Context, name []string) (map[string]model.ConfigIdByNameAndGroupItem, error)
	}
)

var (
	localConfig IConfig
)

func Config() IConfig {
	if localConfig == nil {
		panic("implement not found for interface IConfig, forgot register?")
	}
	return localConfig
}

func RegisterConfig(i IConfig) {
	localConfig = i
}
