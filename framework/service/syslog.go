// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/logic/base"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
)

type (
	ISysLog interface {
		// LogModel 系统日志Model
		LogModel() *base.TblBaseService
		// SaveSystemLog 保存系统日志
		SaveSystemLog(ctx context.Context, log entity.SystemLog) error
		// RecordLog 记录系统日志数据
		RecordLog(ctx context.Context, op enums.LogOperateType, item model.RecordLogItem) error
		// GetSystemLogList 获取系统日志列表
		GetSystemLogList(ctx context.Context, in model.SystemLogGetListInput) (out *model.SystemLogGetListOutput, err error)
		// GetSystemLogDetail 获取系统日志详情
		GetSystemLogDetail(ctx context.Context, id uint) (out *model.SystemLogGetDetailOutput, err error)
		// SystemLogWhere 系统日志条件
		SystemLogWhere(in model.SystemLogWhereItem) map[string]interface{}
		// FormatSystemLog 格式化系统日志
		FormatSystemLog(detail *model.SystemLogDetailItem)
	}
)

var (
	localSysLog ISysLog
)

func SysLog() ISysLog {
	if localSysLog == nil {
		panic("implement not found for interface ISysLog, forgot register?")
	}
	return localSysLog
}

func RegisterSysLog(i ISysLog) {
	localSysLog = i
}
