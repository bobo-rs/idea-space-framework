// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"image"

	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/models"
	"github.com/gogf/gf/v2/net/ghttp"
)

type (
	IDesign interface {
		// OverlayDesignImage 合成并生成设计图片
		OverlayDesignImage(ctx context.Context, in model.OverlayDesignImageInput) (out *model.OverlayDesignImageOutput, err error)
		// DownloadDesignImage 下载并保存设计图片
		DownloadDesignImage(ctx context.Context, in model.DesignDownloadInput) (out *model.DesignDownloadOutput, err error)
		// ProcessDownloadDesignImage 处理并下载设计图片数据
		ProcessDownloadDesignImage(ctx context.Context, in model.DesignDownloadInput) (item *model.DesignImageDownloadItem, err error)
		// GenDesignOverlaysList 生成设计合成列表图片数据
		GenDesignOverlaysList(ctx context.Context, templateList []model.MatchDesignTemplateItem, in model.DesignOverlaysParamItem) (overlays []models.MaterialDesignOverlayItem, err error)
		// ParseUnderFileImage 解析底图文件内容
		ParseUnderFileImage(file *ghttp.UploadFile) (img image.Image, err error)
		// SplitSelling 根据分割符拆分卖点
		SplitSelling(selling string) []string
	}
)

var (
	localDesign IDesign
)

func Design() IDesign {
	if localDesign == nil {
		panic("implement not found for interface IDesign, forgot register?")
	}
	return localDesign
}

func RegisterDesign(i IDesign) {
	localDesign = i
}
