// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"github.com/gogf/gf/v2/net/ghttp"
)

type (
	IMiddleware interface {
		// Auth 用户授权认证-第一层
		Auth(r *ghttp.Request)
		// AuthWithe 白名单-用户授权认证（某些特定场景下，允许用户在不登录的情况下访问接口）
		AuthWithe(r *ghttp.Request)
		// AdminAuth 管理员认证-必须在Auth中间件之后（第二层）
		AdminAuth(r *ghttp.Request)
		// AdminAuthApi 管理员接口鉴权-必须在AdminAuth中间件之后（第三层）
		AdminAuthApi(r *ghttp.Request)
		// WriteOperateLog 写入操作日志
		WriteOperateLog(r *ghttp.Request)
		// GetToken 获取Token
		GetToken(r *ghttp.Request) (string, error)
		// Ctx 上下文自定义初始化
		Ctx(r *ghttp.Request)
		// ResponseHandler 响应资源处理
		ResponseHandler(r *ghttp.Request)
		// 销毁登录缓存用户信息-用于修改用户资料之后，更新缓存信息
		RemoveLoginCache(r *ghttp.Request)
		// RemoveAdminAuthTokenCache 操作管理员状态，移除管理员缓存Token
		RemoveAdminAuthTokenCache(r *ghttp.Request)
	}
)

var (
	localMiddleware IMiddleware
)

func Middleware() IMiddleware {
	if localMiddleware == nil {
		panic("implement not found for interface IMiddleware, forgot register?")
	}
	return localMiddleware
}

func RegisterMiddleware(i IMiddleware) {
	localMiddleware = i
}
