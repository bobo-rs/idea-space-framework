// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/framework/model"
)

type (
	IMaterial interface {
		// FormatMaterialDetail 格式化素材详情
		FormatMaterialDetail(detail *model.MaterialDetailItem)
		// FormatMaterialTemplateDetail 格式化素材模板详情
		FormatMaterialTemplateDetail(detail *model.MaterialTemplateItem)
		// FormatMaterialTemplateWhere 格式化素材模板基础查询条件
		FormatMaterialTemplateWhere(ctx context.Context, in model.MaterialTemplateListWhereItem) (where map[string]interface{})
		// GetMaterialList 获取素材列表
		GetMaterialList(ctx context.Context, in model.MaterialListInput) (out *model.MaterialListOutput, err error)
		// GetMaterialDetail 获取素材详情
		GetMaterialDetail(ctx context.Context, materialId uint) (out *model.MaterialDetailOutput, err error)
		// ProcessMaterialListAndTotal 获取并处理素材列表和总数
		ProcessMaterialListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, list interface{}, total *int) error
		// ProcessMaterialDetail 获取素材详情
		ProcessMaterialDetail(ctx context.Context, where interface{}, detail interface{}) error
		// CheckMaterialExists 检测素材是否存在
		CheckMaterialExists(ctx context.Context, where interface{}) (bool, error)
		// ProcessMaterialList 获取素材列表
		ProcessMaterialList(ctx context.Context, where interface{}, list interface{}) error
		// ProcessMaterialDetailById 根据素材ID获取素材详情
		ProcessMaterialDetailById(ctx context.Context, materialId uint, detail interface{}) error
		// CheckMaterialExistsByAttachId 根据附件ID检测素材是否存在
		CheckMaterialExistsByAttachId(ctx context.Context, attachId string) (bool, error)
		// CheckMaterialExistsById 根据素材ID检测素材是否存在
		CheckMaterialExistsById(ctx context.Context, materialId uint) (bool, error)
		// ProcessMaterialMapById 通过素材ID处理并获取是素材MAP
		ProcessMaterialMapById(ctx context.Context, materialId ...uint) (materialMap map[uint]model.MaterialBasicItem, err error)
		// SaveMaterial 保存素材详情（添加|编辑）
		SaveMaterial(ctx context.Context, in model.MaterialSaveInput) (out *model.MaterialSaveOutput, err error)
		// DeleteMaterial 删除素材
		DeleteMaterial(ctx context.Context, materialId uint) (err error)
		// GetMaterialTemplateList 获取素材模板列表
		GetMaterialTemplateList(ctx context.Context, in model.MaterialTemplateListInput) (out *model.MaterialTemplateListOutput, err error)
		// GetMaterialTemplateDetail 获取素材模板详情
		GetMaterialTemplateDetail(ctx context.Context, tid uint) (out *model.MaterialTemplateDetailOutput, err error)
		// GetMatchTemplateList 获取匹配成功模板列表
		GetMatchTemplateList(ctx context.Context, tid ...uint) (out []model.MatchDesignTemplateItem, err error)
		// DesignTemplateMatchCacheList 设计模板匹配列表
		DesignTemplateMatchCacheList(ctx context.Context, in model.DesignTemplateMatchCacheListInput) (out *model.DesignTemplateMatchGetListOutput, err error)
		// IncMaterialTemplateUseNum 模板使用量叠加
		IncMaterialTemplateUseNum(ctx context.Context, tid []uint, amount ...uint) error
		// IncMaterialTemplateMatchNum 模板匹配量叠加
		IncMaterialTemplateMatchNum(ctx context.Context, tid []uint, amount ...uint) error
		// ProcessMaterialTemplateListAndTotal 获取素材模板列表和总数
		ProcessMaterialTemplateListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, list interface{}, total *int) error
		// ProcessMaterialTemplateDetail 获取素材模板详情
		ProcessMaterialTemplateDetail(ctx context.Context, where interface{}, detail interface{}) error
		// CheckMaterialTemplateExists 检测素材模板是否存在
		CheckMaterialTemplateExists(ctx context.Context, where interface{}) (bool, error)
		// ProcessMaterialTemplateDetailById 通过素材ID获取素材模板详情
		ProcessMaterialTemplateDetailById(ctx context.Context, tid uint, detail interface{}) error
		// CheckMaterialTemplateExistsById 通过模板ID检测素材模板是否存在
		CheckMaterialTemplateExistsById(ctx context.Context, tid uint) bool
		// ProcessMaterialTemplateScan 获取素材模板数据
		ProcessMaterialTemplateScan(ctx context.Context, where, scan interface{}, orderBy ...string) error
		// ProcessMaterialTemplateTotal 统计模板总数数量
		ProcessMaterialTemplateTotal(ctx context.Context, where interface{}) (total int, err error)
		// SaveMaterialTemplate 保存-添加|编辑素材模板信息
		SaveMaterialTemplate(ctx context.Context, req model.MaterialTemplateSaveInput) (tid uint, err error)
		// DeleteMaterialTemplate 删除素材模板信息
		DeleteMaterialTemplate(ctx context.Context, tid uint) error
		// AuditOrOffMaterialTemplate 审核或下架关闭素材模板信息
		AuditOrOffMaterialTemplate(ctx context.Context, in model.MaterialTemplateAuditOrOffInput) error
		// RefreshMaterialTemplateExampleImg 刷新素材模板示例图
		RefreshMaterialTemplateExampleImg(ctx context.Context, tid uint) error
		// GetMaterialConfigRaw 获取素材配置内容
		GetMaterialConfigRaw(ctx context.Context) (item *model.MaterialConfigItem, err error)
		// ProcessTemplateWordsList 获取素材模板列表
		ProcessTemplateWordsList(ctx context.Context, where, list interface{}) error
		// ProcessTemplateWordsListByTid 根据模板ID获取模板文案列表
		ProcessTemplateWordsListByTid(ctx context.Context, tid uint, list interface{}) error
		// ProcessTemplateWordsDetail 获取模板文案详情
		ProcessTemplateWordsDetail(ctx context.Context, where, detail interface{}) error
		// ProcessTemplateWordsDetailById 根据文案ID获取模板文案详情
		ProcessTemplateWordsDetailById(ctx context.Context, id uint, detail interface{}) error
		// CheckTemplateWordsExists 检测素材模板文案是否存在
		CheckTemplateWordsExists(ctx context.Context, where interface{}) (bool, error)
		// CheckTemplateWordsExistsById 通过素材文案ID检测素材文案是否存在
		CheckTemplateWordsExistsById(ctx context.Context, id uint) bool
		// CheckTemplateWordsExistsByTemplateId 通过素材模板ID检测素材文案是否存在
		CheckTemplateWordsExistsByTemplateId(ctx context.Context, tid uint) bool
		// CheckTemplateWordsExistsByMaterialId 通过素材ID检测素材文案是否存在
		CheckTemplateWordsExistsByMaterialId(ctx context.Context, mid uint) bool
		// ProcessTemplateWordsWithList 模板素材文案关联查询素材信息
		ProcessTemplateWordsWithList(ctx context.Context, where interface{}) (out []model.MaterialTemplateWordsDetailItem, err error)
		// ProcessMaterialTemplateWordsScan 获取素材模板文本数据
		ProcessMaterialTemplateWordsScan(ctx context.Context, where, scan interface{}) error
		// ProcessMaterialTemplateWordsListByTid 根据模板ID获取素材文本列表
		ProcessMaterialTemplateWordsListByTid(ctx context.Context, tid uint, list interface{}) error
		// ProcessMaterialTemplateWordsTotal 获取素材模板文本总数
		ProcessMaterialTemplateWordsTotal(ctx context.Context, where interface{}) (int, error)
		// CheckMaterialTemplateWordsExists 检查素材模板文本是否存在
		CheckMaterialTemplateWordsExists(ctx context.Context, where interface{}) (bool, error)
		// CheckMaterialTemplateWordsExistsByTid 根据素材模板ID验证素材文本是否存在
		CheckMaterialTemplateWordsExistsByTid(ctx context.Context, tid uint) bool
		// ProcessMaterialTemplateWordsMapsByTid 根据模板ID获取素材模板文本MAP
		ProcessMaterialTemplateWordsMapsByTid(ctx context.Context, tid ...uint) (words map[uint]model.MatchTemplateWordsItem, err error)
		// SaveMaterialTemplateWords 保存素材模板文案-关键词
		SaveMaterialTemplateWords(ctx context.Context, tid uint, words ...model.MaterialTemplateWordsSaveItem) error
		// DeleteMaterialTemplateWords 删除素材模板文案词
		DeleteMaterialTemplateWords(ctx context.Context, tid uint, wordsId ...uint) error
	}
)

var (
	localMaterial IMaterial
)

func Material() IMaterial {
	if localMaterial == nil {
		panic("implement not found for interface IMaterial, forgot register?")
	}
	return localMaterial
}

func RegisterMaterial(i IMaterial) {
	localMaterial = i
}
