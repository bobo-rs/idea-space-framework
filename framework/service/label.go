// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
)

type (
	ILabel interface {
		// SaveLabelIdx 保存标签索引信息
		SaveLabelIdx(ctx context.Context, cid, cType uint, value string) error
		// DeleteLabelIdx 删除标签索引
		DeleteLabelIdx(ctx context.Context, cid, cType uint, hash ...string) error
		// LabelIdxValueHash 生成标签Hash值和标签值
		LabelIdxValueHash(values []string) []model.IdxValueHashItem
		// GenIdxHash 生成标签索引Hash值
		GenIdxHash(values []string) []string
		// GenHash 生成Hash值
		GenHash(value string) string
		// ProcessLabelIdxList 处理并获取标签索引列表
		ProcessLabelIdxList(ctx context.Context, where, list interface{}) error
		// CheckLabelIdxExists 检测标签索引是否存在
		CheckLabelIdxExists(ctx context.Context, where interface{}) (bool, error)
		// CheckLabelIdxExistsByCidAndType 通过关联ID和类型检测标签索引是否存在
		CheckLabelIdxExistsByCidAndType(ctx context.Context, cid, cType uint) (bool, error)
		// ProcessLabelIdxHashAndIdMap 处理并获取标签索引ID和Hash值MAP
		ProcessLabelIdxHashAndIdMap(ctx context.Context, cid, cType uint, hash ...string) (map[string]uint, error)
		// ProcessLabelIdxHashByCid 根据索引类型和Hash值获取关联ID
		ProcessLabelIdxHashByCid(ctx context.Context, cType enums.CorrelateTypeEnums, hash ...string) ([]uint, error)
		// ProcessLabelIdxTypeAndHashByCid 通过关联类型和Hash值获取标签值关联ID-切片数组
		ProcessLabelIdxTypeAndHashByCid(ctx context.Context, cType enums.CorrelateTypeEnums, value string) ([]uint, error)
		// ProcessIdxTypeAndHashByCid 通过关联类型和Hash值获取标签值关联ID-单Hash值
		ProcessIdxTypeAndHashByCid(ctx context.Context, cType enums.CorrelateTypeEnums, value string) ([]uint, error)
		// GetLabelList 获取标签列表
		GetLabelList(ctx context.Context, in model.LabelGetListInput) (out *model.LabelGetListOutput, err error)
		// GetLabelInfo 获取标签详情信息
		GetLabelInfo(ctx context.Context, in model.LabelGetDetailInput) (out *model.LabelGetDetailOutput, err error)
		// GetLabelShowList 获取标签展示列表
		GetLabelShowList(ctx context.Context) (out []model.LabelShowListItem, err error)
		// GetLabelListAndTotal 获取标签列表和数量
		GetLabelListAndTotal(ctx context.Context, in model.LabelQueryListAndTotalInput, rows interface{}, total *int) error
		// GetLabelDetail 获取标签详情信息
		GetLabelDetail(ctx context.Context, where interface{}, detail interface{}) error
		// ProcessLabelList 处理并获取标签列表
		ProcessLabelList(ctx context.Context, where, list interface{}) error
		// GetLabelDetailById 通过标签ID查询获取标签详情
		GetLabelDetailById(ctx context.Context, labelId uint, detail interface{}) error
		// CheckLabelExists 检测标签是否存在
		CheckLabelExists(ctx context.Context, where interface{}) (bool, error)
		// ProcessLabelScan 扫描标签数据-单条|列表
		ProcessLabelScan(ctx context.Context, where, scan interface{}) error
		// GetLabelDetail 获取标签详情信息
		ProcessLabelDetail(ctx context.Context, where interface{}, detail interface{}) error
		// ProcessLabelDetailById 通过标签ID查询获取标签详情
		ProcessLabelDetailById(ctx context.Context, labelId uint, detail interface{}) error
		// LabelSave 保存标签库标签
		LabelSave(ctx context.Context, in model.LabelSaveInput) (out *model.LabelSaveOutput, err error)
		// GetLabelValuesList 获取标签值列表
		GetLabelValuesList(ctx context.Context, in model.LabelValuesGetListInput) (out *model.LabelValuesGetListOutput, err error)
		// DeleteLabelValues 删除标签值(或恢复数据)
		DeleteLabelValues(ctx context.Context, in model.LabelValueDelInput) error
		// ProcessGetLabelValueList 处理并获取标签值列表
		ProcessGetLabelValueList(ctx context.Context, where interface{}, rows interface{}) error
		// CheckLabelValueExists 检测标签值是否存在
		CheckLabelValueExists(ctx context.Context, where interface{}) (bool, error)
		// ProcessLabelValueCount 获取标签数量
		ProcessLabelValueCount(ctx context.Context, where interface{}) int
		// ProcessLabelValueDetailByHash 以hash值获取标签值详情
		ProcessLabelValueDetailByHash(ctx context.Context, hash string, detail interface{}) error
		// ProcessLabelValueDetailByValueAndLabelId 通过标签ID和标签值获取标签值详情
		ProcessLabelValueDetailByValueAndLabelId(ctx context.Context, where model.LabelValueItem, detail interface{}) error
		// ProcessLabelValueDetail 处理并获取标签值详情
		ProcessLabelValueDetail(ctx context.Context, where interface{}, detail interface{}) error
		// ProcessLabelValueMapByLabelId 根据标签ID获取并处理标签值MAP
		ProcessLabelValueMapByLabelId(ctx context.Context, labelId ...uint) (map[uint]string, error)
		// SaveLabelValues 保存标签值-批量或单条保存
		SaveLabelValues(ctx context.Context, in model.LabelValuesSaveInput) (out *model.LabelValuesSaveOutput, err error)
	}
)

var (
	localLabel ILabel
)

func Label() ILabel {
	if localLabel == nil {
		panic("implement not found for interface ILabel, forgot register?")
	}
	return localLabel
}

func RegisterLabel(i ILabel) {
	localLabel = i
}
