// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"github.com/gogf/gf/v2/net/ghttp"
)

type (
	IBizCtx interface {
		// Init 初始化自定义上下文
		Init(r *ghttp.Request, customCtx *model.Context)
		// Get 获取自定义上下文
		Get(ctx context.Context) *model.Context
		// SetUser 设置登录用户信息
		SetUser(ctx context.Context, user *model.UserAccountItem)
		// SetData 设置其他参数
		SetData(ctx context.Context, data map[enums.ContextDataKey]interface{})
		// SetDataValue 设置自定义参数值
		SetDataValue(ctx context.Context, key enums.ContextDataKey, value interface{})
		// GetUser 获取用户信息
		GetUser(ctx context.Context) *model.UserAccountItem
		// GetData 获取自定义数据
		GetData(ctx context.Context) map[enums.ContextDataKey]interface{}
		// GetUid 获取当前用户ID
		GetUid(ctx context.Context) uint
		// GetValue 获取自定义参数值
		GetValue(ctx context.Context, key enums.ContextDataKey) interface{}
	}
)

var (
	localBizCtx IBizCtx
)

func BizCtx() IBizCtx {
	if localBizCtx == nil {
		panic("implement not found for interface IBizCtx, forgot register?")
	}
	return localBizCtx
}

func RegisterBizCtx(i IBizCtx) {
	localBizCtx = i
}
