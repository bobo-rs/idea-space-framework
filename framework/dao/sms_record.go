// =================================================================================
// This is auto-generated by GoFrame CLI tool only once. Fill this file as you wish.
// =================================================================================

package dao

import (
	"gitee.com/bobo-rs/idea-space-framework/framework/dao/internal"
)

// internalSmsRecordDao is internal type for wrapping internal DAO implements.
type internalSmsRecordDao = *internal.SmsRecordDao

// smsRecordDao is the data access object for table is_sms_record.
// You can define custom methods on it to extend its functionality as you wish.
type smsRecordDao struct {
	internalSmsRecordDao
}

var (
	// SmsRecord is globally public accessible object for table is_sms_record operations.
	SmsRecord = smsRecordDao{
		internal.NewSmsRecordDao(),
	}
)

// Fill with you ideas below.
