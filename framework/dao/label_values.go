// =================================================================================
// This is auto-generated by GoFrame CLI tool only once. Fill this file as you wish.
// =================================================================================

package dao

import (
	"gitee.com/bobo-rs/idea-space-framework/framework/dao/internal"
)

// internalLabelValuesDao is internal type for wrapping internal DAO implements.
type internalLabelValuesDao = *internal.LabelValuesDao

// labelValuesDao is the data access object for table is_label_values.
// You can define custom methods on it to extend its functionality as you wish.
type labelValuesDao struct {
	internalLabelValuesDao
}

var (
	// LabelValues is globally public accessible object for table is_label_values operations.
	LabelValues = labelValuesDao{
		internal.NewLabelValuesDao(),
	}
)

// Fill with you ideas below.
