// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// CutoutCategoryDao is the data access object for table is_cutout_category.
type CutoutCategoryDao struct {
	table   string                // table is the underlying table name of the DAO.
	group   string                // group is the database configuration group name of current DAO.
	columns CutoutCategoryColumns // columns contains all the column names of Table for convenient usage.
}

// CutoutCategoryColumns defines and stores column names for table is_cutout_category.
type CutoutCategoryColumns struct {
	Id          string // 抠图分类ID
	Name        string // 抠图分类名
	BriefName   string // 简短名
	Synopsis    string // 简介内容
	AdvImageUrl string // 广告图URL
	Rule        string // 抠图规则
	IsPromotion string // 是否推广：0否，1是
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// cutoutCategoryColumns holds the columns for table is_cutout_category.
var cutoutCategoryColumns = CutoutCategoryColumns{
	Id:          "id",
	Name:        "name",
	BriefName:   "brief_name",
	Synopsis:    "synopsis",
	AdvImageUrl: "adv_image_url",
	Rule:        "rule",
	IsPromotion: "is_promotion",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewCutoutCategoryDao creates and returns a new DAO object for table data access.
func NewCutoutCategoryDao() *CutoutCategoryDao {
	return &CutoutCategoryDao{
		group:   "default",
		table:   "is_cutout_category",
		columns: cutoutCategoryColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *CutoutCategoryDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *CutoutCategoryDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *CutoutCategoryDao) Columns() CutoutCategoryColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *CutoutCategoryDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *CutoutCategoryDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *CutoutCategoryDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
