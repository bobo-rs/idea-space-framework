// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// MaterialDao is the data access object for table is_material.
type MaterialDao struct {
	table   string          // table is the underlying table name of the DAO.
	group   string          // group is the database configuration group name of current DAO.
	columns MaterialColumns // columns contains all the column names of Table for convenient usage.
}

// MaterialColumns defines and stores column names for table is_material.
type MaterialColumns struct {
	Id           string // 素材ID
	MaterialName string // 素材名称
	MaterialType string // 素材类型：0素材配图，1素材主图（未修饰过的图）
	ChannelId    string // 素材渠道：0平台，1企业，2用户
	Position     string // 素材位置：main主图，market营销活动卖点，top顶部卖点，btm-main底部主卖点，btm-second底部次卖点，btm-single底部单独卖点，detail详情图卖点
	Width        string // 素材宽度
	Height       string // 素材高度
	Url          string // 素材地址
	AttachId     string // 附件ID
	CreatorId    string // 创建人ID
	Creator      string // 创建人
	Modified     string // 修改人
	Status       string // 素材状态：0正常，1废弃
	UpdateAt     string // 更新时间
	CreateAt     string // 创建时间
}

// materialColumns holds the columns for table is_material.
var materialColumns = MaterialColumns{
	Id:           "id",
	MaterialName: "material_name",
	MaterialType: "material_type",
	ChannelId:    "channel_id",
	Position:     "position",
	Width:        "width",
	Height:       "height",
	Url:          "url",
	AttachId:     "attach_id",
	CreatorId:    "creator_id",
	Creator:      "creator",
	Modified:     "modified",
	Status:       "status",
	UpdateAt:     "update_at",
	CreateAt:     "create_at",
}

// NewMaterialDao creates and returns a new DAO object for table data access.
func NewMaterialDao() *MaterialDao {
	return &MaterialDao{
		group:   "default",
		table:   "is_material",
		columns: materialColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *MaterialDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *MaterialDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *MaterialDao) Columns() MaterialColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *MaterialDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *MaterialDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *MaterialDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
