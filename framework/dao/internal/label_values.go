// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// LabelValuesDao is the data access object for table is_label_values.
type LabelValuesDao struct {
	table   string             // table is the underlying table name of the DAO.
	group   string             // group is the database configuration group name of current DAO.
	columns LabelValuesColumns // columns contains all the column names of Table for convenient usage.
}

// LabelValuesColumns defines and stores column names for table is_label_values.
type LabelValuesColumns struct {
	Id       string // 标签值ID
	LabelId  string // 标签ID
	Value    string // 标签值
	IsDel    string // 是否删除：0正常，1删除
	Hash     string // 标签值Hash
	UpdateAt string // 更新时间
	CreateAt string // 创建时间
}

// labelValuesColumns holds the columns for table is_label_values.
var labelValuesColumns = LabelValuesColumns{
	Id:       "id",
	LabelId:  "label_id",
	Value:    "value",
	IsDel:    "is_del",
	Hash:     "hash",
	UpdateAt: "update_at",
	CreateAt: "create_at",
}

// NewLabelValuesDao creates and returns a new DAO object for table data access.
func NewLabelValuesDao() *LabelValuesDao {
	return &LabelValuesDao{
		group:   "default",
		table:   "is_label_values",
		columns: labelValuesColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *LabelValuesDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *LabelValuesDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *LabelValuesDao) Columns() LabelValuesColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *LabelValuesDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *LabelValuesDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *LabelValuesDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
