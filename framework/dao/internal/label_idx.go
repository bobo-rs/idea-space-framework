// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// LabelIdxDao is the data access object for table is_label_idx.
type LabelIdxDao struct {
	table   string          // table is the underlying table name of the DAO.
	group   string          // group is the database configuration group name of current DAO.
	columns LabelIdxColumns // columns contains all the column names of Table for convenient usage.
}

// LabelIdxColumns defines and stores column names for table is_label_idx.
type LabelIdxColumns struct {
	Id            string // 标签索引ID
	CorrelateId   string // 关联ID
	CorrelateType string // 关联类型：0素材模板，1设计图
	LabelValue    string // 标签值(例如：淘宝/天猫-美妆-双十一，淘宝/天猫-美妆-双十二等)
	LabelHash     string // 标签Hash值(例如：淘宝/天猫-美妆-双十一，淘宝/天猫-美妆-双十二等，md5加密)
	UpdateAt      string // 更新时间
	CreateAt      string // 创建时间
}

// labelIdxColumns holds the columns for table is_label_idx.
var labelIdxColumns = LabelIdxColumns{
	Id:            "id",
	CorrelateId:   "correlate_id",
	CorrelateType: "correlate_type",
	LabelValue:    "label_value",
	LabelHash:     "label_hash",
	UpdateAt:      "update_at",
	CreateAt:      "create_at",
}

// NewLabelIdxDao creates and returns a new DAO object for table data access.
func NewLabelIdxDao() *LabelIdxDao {
	return &LabelIdxDao{
		group:   "default",
		table:   "is_label_idx",
		columns: labelIdxColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *LabelIdxDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *LabelIdxDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *LabelIdxDao) Columns() LabelIdxColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *LabelIdxDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *LabelIdxDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *LabelIdxDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
