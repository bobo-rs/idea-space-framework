// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// CutoutImagesAttrDao is the data access object for table is_cutout_images_attr.
type CutoutImagesAttrDao struct {
	table   string                  // table is the underlying table name of the DAO.
	group   string                  // group is the database configuration group name of current DAO.
	columns CutoutImagesAttrColumns // columns contains all the column names of Table for convenient usage.
}

// CutoutImagesAttrColumns defines and stores column names for table is_cutout_images_attr.
type CutoutImagesAttrColumns struct {
	CutImageId  string // 抠图图片ID
	AttachId    string // 附件ID
	ImageUrl    string // 图片地址
	ImagePath   string // 图片路径
	Filename    string // 文件名
	Suffix      string // 图片后缀
	StorageType string // 存储类型：local本地存储，qiniu七牛
	Size        string // 图片大小内存
	Width       string // 图片宽度
	Height      string // 图片高度
	UpdateAt    string // 更新时间
}

// cutoutImagesAttrColumns holds the columns for table is_cutout_images_attr.
var cutoutImagesAttrColumns = CutoutImagesAttrColumns{
	CutImageId:  "cut_image_id",
	AttachId:    "attach_id",
	ImageUrl:    "image_url",
	ImagePath:   "image_path",
	Filename:    "filename",
	Suffix:      "suffix",
	StorageType: "storage_type",
	Size:        "size",
	Width:       "width",
	Height:      "height",
	UpdateAt:    "update_at",
}

// NewCutoutImagesAttrDao creates and returns a new DAO object for table data access.
func NewCutoutImagesAttrDao() *CutoutImagesAttrDao {
	return &CutoutImagesAttrDao{
		group:   "default",
		table:   "is_cutout_images_attr",
		columns: cutoutImagesAttrColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *CutoutImagesAttrDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *CutoutImagesAttrDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *CutoutImagesAttrDao) Columns() CutoutImagesAttrColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *CutoutImagesAttrDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *CutoutImagesAttrDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *CutoutImagesAttrDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
