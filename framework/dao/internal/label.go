// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// LabelDao is the data access object for table is_label.
type LabelDao struct {
	table   string       // table is the underlying table name of the DAO.
	group   string       // group is the database configuration group name of current DAO.
	columns LabelColumns // columns contains all the column names of Table for convenient usage.
}

// LabelColumns defines and stores column names for table is_label.
type LabelColumns struct {
	Id         string // 标签类ID
	Name       string // 标签类名
	IsRequired string // 是否必须勾选：0否，1是
	Creator    string // 最先创建人
	Modified   string // 最后修改人
	UpdateAt   string // 更新时间
	CreateAt   string // 创建时间
}

// labelColumns holds the columns for table is_label.
var labelColumns = LabelColumns{
	Id:         "id",
	Name:       "name",
	IsRequired: "is_required",
	Creator:    "creator",
	Modified:   "modified",
	UpdateAt:   "update_at",
	CreateAt:   "create_at",
}

// NewLabelDao creates and returns a new DAO object for table data access.
func NewLabelDao() *LabelDao {
	return &LabelDao{
		group:   "default",
		table:   "is_label",
		columns: labelColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *LabelDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *LabelDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *LabelDao) Columns() LabelColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *LabelDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *LabelDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *LabelDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
