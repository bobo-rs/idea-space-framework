// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SmsRecordDao is the data access object for table is_sms_record.
type SmsRecordDao struct {
	table   string           // table is the underlying table name of the DAO.
	group   string           // group is the database configuration group name of current DAO.
	columns SmsRecordColumns // columns contains all the column names of Table for convenient usage.
}

// SmsRecordColumns defines and stores column names for table is_sms_record.
type SmsRecordColumns struct {
	Id         string // 短信发送记录编号
	Mobile     string // 接受短信的手机号
	Content    string // 短信内容
	Code       string // 验证码
	Ip         string // 添加记录ip
	TemplateId string // 短信模板ID
	SmsType    string // 短信类型：0验证码，1消息通知，2营销短信
	SerialId   string // 发送记录id
	Results    string // 发送结果
	UpdateAt   string // 更新时间
	CreateAt   string // 创建时间
}

// smsRecordColumns holds the columns for table is_sms_record.
var smsRecordColumns = SmsRecordColumns{
	Id:         "id",
	Mobile:     "mobile",
	Content:    "content",
	Code:       "code",
	Ip:         "ip",
	TemplateId: "template_id",
	SmsType:    "sms_type",
	SerialId:   "serial_id",
	Results:    "results",
	UpdateAt:   "update_at",
	CreateAt:   "create_at",
}

// NewSmsRecordDao creates and returns a new DAO object for table data access.
func NewSmsRecordDao() *SmsRecordDao {
	return &SmsRecordDao{
		group:   "default",
		table:   "is_sms_record",
		columns: smsRecordColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SmsRecordDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SmsRecordDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SmsRecordDao) Columns() SmsRecordColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SmsRecordDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SmsRecordDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SmsRecordDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
