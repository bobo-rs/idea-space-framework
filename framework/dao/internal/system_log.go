// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SystemLogDao is the data access object for table is_system_log.
type SystemLogDao struct {
	table   string           // table is the underlying table name of the DAO.
	group   string           // group is the database configuration group name of current DAO.
	columns SystemLogColumns // columns contains all the column names of Table for convenient usage.
}

// SystemLogColumns defines and stores column names for table is_system_log.
type SystemLogColumns struct {
	Id           string // 日志id
	ModuleName   string // 操作模块名
	Content      string // 操作详情
	Ip           string // 操作IP地址
	OperateType  string // 日志操作类型：0操作日志，1登录操作，2系统日志
	ApiUrl       string // api URL
	UserId       string // 用户ID
	ManageId     string // 账户id
	ManageName   string // 账户名
	MerchantId   string // 商户ID
	ResponseText string // 资源响应
	ParamText    string // 请求资源
	UpdateAt     string // 更新时间
	CreateAt     string // 创建时间
}

// systemLogColumns holds the columns for table is_system_log.
var systemLogColumns = SystemLogColumns{
	Id:           "id",
	ModuleName:   "module_name",
	Content:      "content",
	Ip:           "ip",
	OperateType:  "operate_type",
	ApiUrl:       "api_url",
	UserId:       "user_id",
	ManageId:     "manage_id",
	ManageName:   "manage_name",
	MerchantId:   "merchant_id",
	ResponseText: "response_text",
	ParamText:    "param_text",
	UpdateAt:     "update_at",
	CreateAt:     "create_at",
}

// NewSystemLogDao creates and returns a new DAO object for table data access.
func NewSystemLogDao() *SystemLogDao {
	return &SystemLogDao{
		group:   "default",
		table:   "is_system_log",
		columns: systemLogColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SystemLogDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SystemLogDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SystemLogDao) Columns() SystemLogColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SystemLogDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SystemLogDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SystemLogDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
