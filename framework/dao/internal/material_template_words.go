// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// MaterialTemplateWordsDao is the data access object for table is_material_template_words.
type MaterialTemplateWordsDao struct {
	table   string                       // table is the underlying table name of the DAO.
	group   string                       // group is the database configuration group name of current DAO.
	columns MaterialTemplateWordsColumns // columns contains all the column names of Table for convenient usage.
}

// MaterialTemplateWordsColumns defines and stores column names for table is_material_template_words.
type MaterialTemplateWordsColumns struct {
	Id             string // 素材模板文案ID
	MaterialId     string // 素材ID
	TemplateId     string // 素材模板ID
	ExampleWords   string // 示例文案
	FontFamily     string // 字体系列，例如：微软雅黑
	WordsType      string // 文案类型：0空白，1文字，2.价格
	WordsFont      string // 文案字体字号
	WordsFontColor string // 文按字体颜色
	WordsX         string // 切词文案卖点X坐标
	WordsY         string // 切词文案卖点Y坐标
	LayerPosition  string // 图层位置，切词优先词
	WordsNum       string // 文本数量
	MaterialX      string // 素材X坐标
	MaterialY      string // 素材Y坐标
	MaterialColor  string // 素材色系
	Url            string // 素材URL
	UpdateAt       string // 更新时间
	CreateAt       string // 创建时间
}

// materialTemplateWordsColumns holds the columns for table is_material_template_words.
var materialTemplateWordsColumns = MaterialTemplateWordsColumns{
	Id:             "id",
	MaterialId:     "material_id",
	TemplateId:     "template_id",
	ExampleWords:   "example_words",
	FontFamily:     "font_family",
	WordsType:      "words_type",
	WordsFont:      "words_font",
	WordsFontColor: "words_font_color",
	WordsX:         "words_x",
	WordsY:         "words_y",
	LayerPosition:  "layer_position",
	WordsNum:       "words_num",
	MaterialX:      "material_x",
	MaterialY:      "material_y",
	MaterialColor:  "material_color",
	Url:            "url",
	UpdateAt:       "update_at",
	CreateAt:       "create_at",
}

// NewMaterialTemplateWordsDao creates and returns a new DAO object for table data access.
func NewMaterialTemplateWordsDao() *MaterialTemplateWordsDao {
	return &MaterialTemplateWordsDao{
		group:   "default",
		table:   "is_material_template_words",
		columns: materialTemplateWordsColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *MaterialTemplateWordsDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *MaterialTemplateWordsDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *MaterialTemplateWordsDao) Columns() MaterialTemplateWordsColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *MaterialTemplateWordsDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *MaterialTemplateWordsDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *MaterialTemplateWordsDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
