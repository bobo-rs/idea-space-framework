// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SpaceImagesAttrDao is the data access object for table is_space_images_attr.
type SpaceImagesAttrDao struct {
	table   string                 // table is the underlying table name of the DAO.
	group   string                 // group is the database configuration group name of current DAO.
	columns SpaceImagesAttrColumns // columns contains all the column names of Table for convenient usage.
}

// SpaceImagesAttrColumns defines and stores column names for table is_space_images_attr.
type SpaceImagesAttrColumns struct {
	SpaceId     string // 设计空间图片ID
	StorageType string // 存储类型：local本地，qiniu七牛等
	AttachId    string // 附件ID
	Size        string // 内存大小
	Width       string // 宽度
	Height      string // 高度
	Precision   string // 精度：0-9，越高精度越高
	RbgChannel  string // RBG通道
	UpdateAt    string // 更新时间
}

// spaceImagesAttrColumns holds the columns for table is_space_images_attr.
var spaceImagesAttrColumns = SpaceImagesAttrColumns{
	SpaceId:     "space_id",
	StorageType: "storage_type",
	AttachId:    "attach_id",
	Size:        "size",
	Width:       "width",
	Height:      "height",
	Precision:   "precision",
	RbgChannel:  "rbg_channel",
	UpdateAt:    "update_at",
}

// NewSpaceImagesAttrDao creates and returns a new DAO object for table data access.
func NewSpaceImagesAttrDao() *SpaceImagesAttrDao {
	return &SpaceImagesAttrDao{
		group:   "default",
		table:   "is_space_images_attr",
		columns: spaceImagesAttrColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SpaceImagesAttrDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SpaceImagesAttrDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SpaceImagesAttrDao) Columns() SpaceImagesAttrColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SpaceImagesAttrDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SpaceImagesAttrDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SpaceImagesAttrDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
