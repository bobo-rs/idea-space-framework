// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SpaceImagesDetailDao is the data access object for table is_space_images_detail.
type SpaceImagesDetailDao struct {
	table   string                   // table is the underlying table name of the DAO.
	group   string                   // group is the database configuration group name of current DAO.
	columns SpaceImagesDetailColumns // columns contains all the column names of Table for convenient usage.
}

// SpaceImagesDetailColumns defines and stores column names for table is_space_images_detail.
type SpaceImagesDetailColumns struct {
	SpaceId      string // 设计空间图片ID
	TemplateId   string // 模板ID
	Synopsis     string // 图片简介
	LabelValues  string // 标签值
	FullCutWords string // 完整切词卖点
	Creator      string // 创建人
	Modified     string // 修改人
	UpdateAt     string // 更新时间
}

// spaceImagesDetailColumns holds the columns for table is_space_images_detail.
var spaceImagesDetailColumns = SpaceImagesDetailColumns{
	SpaceId:      "space_id",
	TemplateId:   "template_id",
	Synopsis:     "synopsis",
	LabelValues:  "label_values",
	FullCutWords: "full_cut_words",
	Creator:      "creator",
	Modified:     "modified",
	UpdateAt:     "update_at",
}

// NewSpaceImagesDetailDao creates and returns a new DAO object for table data access.
func NewSpaceImagesDetailDao() *SpaceImagesDetailDao {
	return &SpaceImagesDetailDao{
		group:   "default",
		table:   "is_space_images_detail",
		columns: spaceImagesDetailColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SpaceImagesDetailDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SpaceImagesDetailDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SpaceImagesDetailDao) Columns() SpaceImagesDetailColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SpaceImagesDetailDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SpaceImagesDetailDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SpaceImagesDetailDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
