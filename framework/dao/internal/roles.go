// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// RolesDao is the data access object for table is_roles.
type RolesDao struct {
	table   string       // table is the underlying table name of the DAO.
	group   string       // group is the database configuration group name of current DAO.
	columns RolesColumns // columns contains all the column names of Table for convenient usage.
}

// RolesColumns defines and stores column names for table is_roles.
type RolesColumns struct {
	Id          string // 角色ID
	Name        string // 角色名
	Description string // 详情
	RoleStatus  string // 角色状态：0正常，1冻结
	Sort        string // 排序：0-255，默认255
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// rolesColumns holds the columns for table is_roles.
var rolesColumns = RolesColumns{
	Id:          "id",
	Name:        "name",
	Description: "description",
	RoleStatus:  "role_status",
	Sort:        "sort",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewRolesDao creates and returns a new DAO object for table data access.
func NewRolesDao() *RolesDao {
	return &RolesDao{
		group:   "default",
		table:   "is_roles",
		columns: rolesColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *RolesDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *RolesDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *RolesDao) Columns() RolesColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *RolesDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *RolesDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *RolesDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
