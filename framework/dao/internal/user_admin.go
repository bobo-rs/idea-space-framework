// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// UserAdminDao is the data access object for table is_user_admin.
type UserAdminDao struct {
	table   string           // table is the underlying table name of the DAO.
	group   string           // group is the database configuration group name of current DAO.
	columns UserAdminColumns // columns contains all the column names of Table for convenient usage.
}

// UserAdminColumns defines and stores column names for table is_user_admin.
type UserAdminColumns struct {
	Id            string // 管理员ID
	UserId        string // 用户ID
	ManageName    string // 管理名称
	ManageNo      string // 管理员编号
	IsSuperManage string // 是否超管：0普管，1超管
	ManageStatus  string // 管理员状态：0正常，1冻结，2离职，3违规，4注销
	LogoffAt      string // 注销时间
	UpdateAt      string // 更新时间
	CreateAt      string // 穿件时间
}

// userAdminColumns holds the columns for table is_user_admin.
var userAdminColumns = UserAdminColumns{
	Id:            "id",
	UserId:        "user_id",
	ManageName:    "manage_name",
	ManageNo:      "manage_no",
	IsSuperManage: "is_super_manage",
	ManageStatus:  "manage_status",
	LogoffAt:      "logoff_at",
	UpdateAt:      "update_at",
	CreateAt:      "create_at",
}

// NewUserAdminDao creates and returns a new DAO object for table data access.
func NewUserAdminDao() *UserAdminDao {
	return &UserAdminDao{
		group:   "default",
		table:   "is_user_admin",
		columns: userAdminColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *UserAdminDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *UserAdminDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *UserAdminDao) Columns() UserAdminColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *UserAdminDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *UserAdminDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *UserAdminDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
