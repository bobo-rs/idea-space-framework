// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// UserDetailDao is the data access object for table is_user_detail.
type UserDetailDao struct {
	table   string            // table is the underlying table name of the DAO.
	group   string            // group is the database configuration group name of current DAO.
	columns UserDetailColumns // columns contains all the column names of Table for convenient usage.
}

// UserDetailColumns defines and stores column names for table is_user_detail.
type UserDetailColumns struct {
	UserId          string // 用户ID
	Sex             string // 性别：0男，1女
	Birthday        string // 生日
	PwdHash         string // 密码hash值
	PwdSalt         string // 加密盐
	Avatar          string // 头像
	AttachId        string // 附件ID
	RegIp           string // 注册IP
	RegIpLocation   string // 注册归属地
	LoginIp         string // 登录IP
	LoginIpLocation string // 登录归属地
	LogoutAt        string // 注销时间
	SignNum         string // 签到总数
	ConSign         string // 连续签到时间
	UpdateAt        string // 更新时间
}

// userDetailColumns holds the columns for table is_user_detail.
var userDetailColumns = UserDetailColumns{
	UserId:          "user_id",
	Sex:             "sex",
	Birthday:        "birthday",
	PwdHash:         "pwd_hash",
	PwdSalt:         "pwd_salt",
	Avatar:          "avatar",
	AttachId:        "attach_id",
	RegIp:           "reg_ip",
	RegIpLocation:   "reg_ip_location",
	LoginIp:         "login_ip",
	LoginIpLocation: "login_ip_location",
	LogoutAt:        "logout_at",
	SignNum:         "sign_num",
	ConSign:         "con_sign",
	UpdateAt:        "update_at",
}

// NewUserDetailDao creates and returns a new DAO object for table data access.
func NewUserDetailDao() *UserDetailDao {
	return &UserDetailDao{
		group:   "default",
		table:   "is_user_detail",
		columns: userDetailColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *UserDetailDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *UserDetailDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *UserDetailDao) Columns() UserDetailColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *UserDetailDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *UserDetailDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *UserDetailDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
