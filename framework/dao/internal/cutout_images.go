// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// CutoutImagesDao is the data access object for table is_cutout_images.
type CutoutImagesDao struct {
	table   string              // table is the underlying table name of the DAO.
	group   string              // group is the database configuration group name of current DAO.
	columns CutoutImagesColumns // columns contains all the column names of Table for convenient usage.
}

// CutoutImagesColumns defines and stores column names for table is_cutout_images.
type CutoutImagesColumns struct {
	Id           string // 抠图ID
	CutoutCateId string // 抠图分类ID
	CutoutName   string // 抠图名
	ImageUrl     string // 图片地址
	DownloadNum  string // 下载量
	BrowseNum    string // 浏览数量
	ChannelId    string // 渠道ID
	CreatorId    string // 创建人ID
	Creator      string // 创建人
	Modified     string // 修改人
	UpdateAt     string // 更新时间
	CreateAt     string // 创建时间
}

// cutoutImagesColumns holds the columns for table is_cutout_images.
var cutoutImagesColumns = CutoutImagesColumns{
	Id:           "id",
	CutoutCateId: "cutout_cate_id",
	CutoutName:   "cutout_name",
	ImageUrl:     "image_url",
	DownloadNum:  "download_num",
	BrowseNum:    "browse_num",
	ChannelId:    "channel_id",
	CreatorId:    "creator_id",
	Creator:      "creator",
	Modified:     "modified",
	UpdateAt:     "update_at",
	CreateAt:     "create_at",
}

// NewCutoutImagesDao creates and returns a new DAO object for table data access.
func NewCutoutImagesDao() *CutoutImagesDao {
	return &CutoutImagesDao{
		group:   "default",
		table:   "is_cutout_images",
		columns: cutoutImagesColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *CutoutImagesDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *CutoutImagesDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *CutoutImagesDao) Columns() CutoutImagesColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *CutoutImagesDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *CutoutImagesDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *CutoutImagesDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
