// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SpaceImagesDao is the data access object for table is_space_images.
type SpaceImagesDao struct {
	table   string             // table is the underlying table name of the DAO.
	group   string             // group is the database configuration group name of current DAO.
	columns SpaceImagesColumns // columns contains all the column names of Table for convenient usage.
}

// SpaceImagesColumns defines and stores column names for table is_space_images.
type SpaceImagesColumns struct {
	Id          string // 空间图片ID
	SpaceName   string // 空间图片名
	ImageUrl    string // 图片URL
	VipType     string // 授权类型：0免费，1VIP
	IsOpen      string // 是否开放浏览：0开放，1关闭
	Price       string // 价格（VIP）
	DesignBean  string // 设计豆（虚拟货币）
	DownloadNum string // 下载数量
	Browse      string // 浏览量
	BuyNum      string // 购买量
	DesignType  string // 设计类型：0智能设计，1手动设计，2主动设计
	ChannelId   string // 渠道来源ID
	CreatorId   string // 创建人ID
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// spaceImagesColumns holds the columns for table is_space_images.
var spaceImagesColumns = SpaceImagesColumns{
	Id:          "id",
	SpaceName:   "space_name",
	ImageUrl:    "image_url",
	VipType:     "vip_type",
	IsOpen:      "is_open",
	Price:       "price",
	DesignBean:  "design_bean",
	DownloadNum: "download_num",
	Browse:      "browse",
	BuyNum:      "buy_num",
	DesignType:  "design_type",
	ChannelId:   "channel_id",
	CreatorId:   "creator_id",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewSpaceImagesDao creates and returns a new DAO object for table data access.
func NewSpaceImagesDao() *SpaceImagesDao {
	return &SpaceImagesDao{
		group:   "default",
		table:   "is_space_images",
		columns: spaceImagesColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SpaceImagesDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SpaceImagesDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SpaceImagesDao) Columns() SpaceImagesColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SpaceImagesDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SpaceImagesDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SpaceImagesDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
