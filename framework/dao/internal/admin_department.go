// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// AdminDepartmentDao is the data access object for table is_admin_department.
type AdminDepartmentDao struct {
	table   string                 // table is the underlying table name of the DAO.
	group   string                 // group is the database configuration group name of current DAO.
	columns AdminDepartmentColumns // columns contains all the column names of Table for convenient usage.
}

// AdminDepartmentColumns defines and stores column names for table is_admin_department.
type AdminDepartmentColumns struct {
	Id       string // 管理员部门ID
	AdminId  string // 管理员ID，非用户ID
	DepartId string // 部门ID
	UpdateAt string // 更新时间
	CreateAt string // 创建时间
}

// adminDepartmentColumns holds the columns for table is_admin_department.
var adminDepartmentColumns = AdminDepartmentColumns{
	Id:       "id",
	AdminId:  "admin_id",
	DepartId: "depart_id",
	UpdateAt: "update_at",
	CreateAt: "create_at",
}

// NewAdminDepartmentDao creates and returns a new DAO object for table data access.
func NewAdminDepartmentDao() *AdminDepartmentDao {
	return &AdminDepartmentDao{
		group:   "default",
		table:   "is_admin_department",
		columns: adminDepartmentColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *AdminDepartmentDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *AdminDepartmentDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *AdminDepartmentDao) Columns() AdminDepartmentColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *AdminDepartmentDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *AdminDepartmentDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *AdminDepartmentDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
