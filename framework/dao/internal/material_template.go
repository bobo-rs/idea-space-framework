// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// MaterialTemplateDao is the data access object for table is_material_template.
type MaterialTemplateDao struct {
	table   string                  // table is the underlying table name of the DAO.
	group   string                  // group is the database configuration group name of current DAO.
	columns MaterialTemplateColumns // columns contains all the column names of Table for convenient usage.
}

// MaterialTemplateColumns defines and stores column names for table is_material_template.
type MaterialTemplateColumns struct {
	Id           string // 模板ID
	TemplateName string // 模板名
	LabelValues  string // 标签值（渠道-行业-营销）
	CutWordsNum  string // 切词卖点数量
	SellingLen   string // 卖点长度（容错匹配，当切词卖点检索不到模板，直接卖点长度匹配，就近原则）
	IdxStatus    string // 标签索引状态：0创建中、1已完成、2中止失败
	Status       string // 模板状态：0待审核，1正常，2下架
	UseNum       string // 使用量
	MatchNum     string // 匹配数量
	VipType      string // VIP类型：0免费，1VIP
	ChannelId    string // 渠道ID
	ExampleImg   string // 示例模板图片地址
	Creator      string // 创建人
	CreatorId    string // 创建人ID
	Modified     string // 修改人
	UpdateAt     string // 更新时间
	CreateAt     string // 创建时间
}

// materialTemplateColumns holds the columns for table is_material_template.
var materialTemplateColumns = MaterialTemplateColumns{
	Id:           "id",
	TemplateName: "template_name",
	LabelValues:  "label_values",
	CutWordsNum:  "cut_words_num",
	SellingLen:   "selling_len",
	IdxStatus:    "idx_status",
	Status:       "status",
	UseNum:       "use_num",
	MatchNum:     "match_num",
	VipType:      "vip_type",
	ChannelId:    "channel_id",
	ExampleImg:   "example_img",
	Creator:      "creator",
	CreatorId:    "creator_id",
	Modified:     "modified",
	UpdateAt:     "update_at",
	CreateAt:     "create_at",
}

// NewMaterialTemplateDao creates and returns a new DAO object for table data access.
func NewMaterialTemplateDao() *MaterialTemplateDao {
	return &MaterialTemplateDao{
		group:   "default",
		table:   "is_material_template",
		columns: materialTemplateColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *MaterialTemplateDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *MaterialTemplateDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *MaterialTemplateDao) Columns() MaterialTemplateColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *MaterialTemplateDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *MaterialTemplateDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *MaterialTemplateDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
