// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SpaceImagesOcrDao is the data access object for table is_space_images_ocr.
type SpaceImagesOcrDao struct {
	table   string                // table is the underlying table name of the DAO.
	group   string                // group is the database configuration group name of current DAO.
	columns SpaceImagesOcrColumns // columns contains all the column names of Table for convenient usage.
}

// SpaceImagesOcrColumns defines and stores column names for table is_space_images_ocr.
type SpaceImagesOcrColumns struct {
	Id         string // 文案OCRID
	ImageId    string // 图片ID
	WordsValue string // 文案值
	WordsFont  string // 文案字号大小
	WordsColor string // 文案颜色
	WordsX     string // 文案坐标X
	WordsY     string // 文案坐标Y
	WordsLen   string // 文案长度
	Width      string // 宽度
	Height     string // 高度
	IsOrigin   string // 是否原文：0原文，1翻译
	Lang       string // 语言：zh-CN简体中文
	WordsHash  string // Hash值（文案内容+是否主体+语言类型）
	UpdateAt   string // 更新时间
	CreateAt   string // 创建时间
}

// spaceImagesOcrColumns holds the columns for table is_space_images_ocr.
var spaceImagesOcrColumns = SpaceImagesOcrColumns{
	Id:         "id",
	ImageId:    "image_id",
	WordsValue: "words_value",
	WordsFont:  "words_font",
	WordsColor: "words_color",
	WordsX:     "words_x",
	WordsY:     "words_y",
	WordsLen:   "words_len",
	Width:      "width",
	Height:     "height",
	IsOrigin:   "is_origin",
	Lang:       "lang",
	WordsHash:  "words_hash",
	UpdateAt:   "update_at",
	CreateAt:   "create_at",
}

// NewSpaceImagesOcrDao creates and returns a new DAO object for table data access.
func NewSpaceImagesOcrDao() *SpaceImagesOcrDao {
	return &SpaceImagesOcrDao{
		group:   "default",
		table:   "is_space_images_ocr",
		columns: spaceImagesOcrColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SpaceImagesOcrDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SpaceImagesOcrDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SpaceImagesOcrDao) Columns() SpaceImagesOcrColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SpaceImagesOcrDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SpaceImagesOcrDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SpaceImagesOcrDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
