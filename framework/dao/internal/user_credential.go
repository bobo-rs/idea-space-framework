// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// UserCredentialDao is the data access object for table is_user_credential.
type UserCredentialDao struct {
	table   string                // table is the underlying table name of the DAO.
	group   string                // group is the database configuration group name of current DAO.
	columns UserCredentialColumns // columns contains all the column names of Table for convenient usage.
}

// UserCredentialColumns defines and stores column names for table is_user_credential.
type UserCredentialColumns struct {
	Id              string // 用户证件ID
	UserId          string // 用户ID
	IdCard          string // 证件编号
	JustUrl         string // 证件正面URL地址
	OppositeUrl     string // 证件反面URL地址
	Type            string // 证件类型：0身份证
	ExpireType      string // 证件效期类型：0有效期，1长期
	ExpireStartDate string // 证件注册时间
	ExpireEndDate   string // 证件到期时间
	UpdateAt        string // 更新时间
	CreateAt        string // 创建时间
}

// userCredentialColumns holds the columns for table is_user_credential.
var userCredentialColumns = UserCredentialColumns{
	Id:              "id",
	UserId:          "user_id",
	IdCard:          "id_card",
	JustUrl:         "just_url",
	OppositeUrl:     "opposite_url",
	Type:            "type",
	ExpireType:      "expire_type",
	ExpireStartDate: "expire_start_date",
	ExpireEndDate:   "expire_end_date",
	UpdateAt:        "update_at",
	CreateAt:        "create_at",
}

// NewUserCredentialDao creates and returns a new DAO object for table data access.
func NewUserCredentialDao() *UserCredentialDao {
	return &UserCredentialDao{
		group:   "default",
		table:   "is_user_credential",
		columns: userCredentialColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *UserCredentialDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *UserCredentialDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *UserCredentialDao) Columns() UserCredentialColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *UserCredentialDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *UserCredentialDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *UserCredentialDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
