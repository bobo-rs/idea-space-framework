// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// AttachmentDao is the data access object for table is_attachment.
type AttachmentDao struct {
	table   string            // table is the underlying table name of the DAO.
	group   string            // group is the database configuration group name of current DAO.
	columns AttachmentColumns // columns contains all the column names of Table for convenient usage.
}

// AttachmentColumns defines and stores column names for table is_attachment.
type AttachmentColumns struct {
	Id          string // 附件ID
	CatId       string // 附件分类
	Name        string // 附件名
	RealName    string // 附件原名
	AttSize     string // 附件尺寸大小
	AttPath     string // 附件路径
	AttUrl      string // 附件地址
	AttType     string // 附件类型：image/png
	Position    string // 排序位置：从小到大
	ChannelId   string // 渠道ID
	StorageType string // 存储类型：local本地，qiniu七牛等
	Hasher      string // Hash值
	AttachId    string // 附件ID
	Width       string // 宽度
	Height      string // 高度
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// attachmentColumns holds the columns for table is_attachment.
var attachmentColumns = AttachmentColumns{
	Id:          "id",
	CatId:       "cat_id",
	Name:        "name",
	RealName:    "real_name",
	AttSize:     "att_size",
	AttPath:     "att_path",
	AttUrl:      "att_url",
	AttType:     "att_type",
	Position:    "position",
	ChannelId:   "channel_id",
	StorageType: "storage_type",
	Hasher:      "hasher",
	AttachId:    "attach_id",
	Width:       "width",
	Height:      "height",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewAttachmentDao creates and returns a new DAO object for table data access.
func NewAttachmentDao() *AttachmentDao {
	return &AttachmentDao{
		group:   "default",
		table:   "is_attachment",
		columns: attachmentColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *AttachmentDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *AttachmentDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *AttachmentDao) Columns() AttachmentColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *AttachmentDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *AttachmentDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *AttachmentDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
