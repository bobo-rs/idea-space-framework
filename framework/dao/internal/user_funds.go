// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// UserFundsDao is the data access object for table is_user_funds.
type UserFundsDao struct {
	table   string           // table is the underlying table name of the DAO.
	group   string           // group is the database configuration group name of current DAO.
	columns UserFundsColumns // columns contains all the column names of Table for convenient usage.
}

// UserFundsColumns defines and stores column names for table is_user_funds.
type UserFundsColumns struct {
	UserId                string // 用户ID
	RechargeTotalFee      string // 充值总金额
	AvailableFee          string // 可用金额
	FreezeFee             string // 冻结金额
	DesignTotalNum        string // 设计币总数
	AvailableDesignNum    string // 可用设计币
	FreezeDesignNum       string // 冻结设计币
	ConsumeTotalDesignNum string // 消费总设计币
	ConsumeTotalFee       string // 消费总金额
	UpdateAt              string // 更新时间
}

// userFundsColumns holds the columns for table is_user_funds.
var userFundsColumns = UserFundsColumns{
	UserId:                "user_id",
	RechargeTotalFee:      "recharge_total_fee",
	AvailableFee:          "available_fee",
	FreezeFee:             "freeze_fee",
	DesignTotalNum:        "design_total_num",
	AvailableDesignNum:    "available_design_num",
	FreezeDesignNum:       "freeze_design_num",
	ConsumeTotalDesignNum: "consume_total_design_num",
	ConsumeTotalFee:       "consume_total_fee",
	UpdateAt:              "update_at",
}

// NewUserFundsDao creates and returns a new DAO object for table data access.
func NewUserFundsDao() *UserFundsDao {
	return &UserFundsDao{
		group:   "default",
		table:   "is_user_funds",
		columns: userFundsColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *UserFundsDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *UserFundsDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *UserFundsDao) Columns() UserFundsColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *UserFundsDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *UserFundsDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *UserFundsDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
