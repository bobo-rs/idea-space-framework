// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// UserDeviceDao is the data access object for table is_user_device.
type UserDeviceDao struct {
	table   string            // table is the underlying table name of the DAO.
	group   string            // group is the database configuration group name of current DAO.
	columns UserDeviceColumns // columns contains all the column names of Table for convenient usage.
}

// UserDeviceColumns defines and stores column names for table is_user_device.
type UserDeviceColumns struct {
	Id          string // 用户设备ID
	UserId      string // 用户ID
	DeviceName  string // 设备名称
	DeviceType  string // 设备类型：iOS，安卓，H5，PC
	Mac         string // MAC地址
	DeviceHash  string // 设备Hash值[用户ID+设备名+类型+MAC]
	LastLoginAt string // 最后登录时间
	IsDisabled  string // 是否禁用：0正常，1禁用
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// userDeviceColumns holds the columns for table is_user_device.
var userDeviceColumns = UserDeviceColumns{
	Id:          "id",
	UserId:      "user_id",
	DeviceName:  "device_name",
	DeviceType:  "device_type",
	Mac:         "mac",
	DeviceHash:  "device_hash",
	LastLoginAt: "last_login_at",
	IsDisabled:  "is_disabled",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewUserDeviceDao creates and returns a new DAO object for table data access.
func NewUserDeviceDao() *UserDeviceDao {
	return &UserDeviceDao{
		group:   "default",
		table:   "is_user_device",
		columns: userDeviceColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *UserDeviceDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *UserDeviceDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *UserDeviceDao) Columns() UserDeviceColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *UserDeviceDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *UserDeviceDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *UserDeviceDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
