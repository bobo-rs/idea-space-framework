// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// PermissionsDao is the data access object for table is_permissions.
type PermissionsDao struct {
	table   string             // table is the underlying table name of the DAO.
	group   string             // group is the database configuration group name of current DAO.
	columns PermissionsColumns // columns contains all the column names of Table for convenient usage.
}

// PermissionsColumns defines and stores column names for table is_permissions.
type PermissionsColumns struct {
	Id            string // 权限ID
	Name          string // 权限名
	Description   string // 权限详情
	MenuType      string // 权限类型：0主菜单，1子菜单，2页面操作，3数据授权
	Icon          string // Icon图
	Path          string // 页面地址
	ApiPath       string // 接口地址
	Pid           string // 父级权限ID
	IsShow        string // 菜单是否显示：0显示，1隐藏
	IsDisabled    string // 是否禁用：0正常，1禁用
	PermCode      string // 操作权限码
	DisableColumn string // 禁用字段（3.数据授权类型）
	Sort          string // 排序值0-255
	UpdateAt      string // 更新时间
	CreateAt      string // 创建时间
}

// permissionsColumns holds the columns for table is_permissions.
var permissionsColumns = PermissionsColumns{
	Id:            "id",
	Name:          "name",
	Description:   "description",
	MenuType:      "menu_type",
	Icon:          "icon",
	Path:          "path",
	ApiPath:       "api_path",
	Pid:           "pid",
	IsShow:        "is_show",
	IsDisabled:    "is_disabled",
	PermCode:      "perm_code",
	DisableColumn: "disable_column",
	Sort:          "sort",
	UpdateAt:      "update_at",
	CreateAt:      "create_at",
}

// NewPermissionsDao creates and returns a new DAO object for table data access.
func NewPermissionsDao() *PermissionsDao {
	return &PermissionsDao{
		group:   "default",
		table:   "is_permissions",
		columns: permissionsColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *PermissionsDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *PermissionsDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *PermissionsDao) Columns() PermissionsColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *PermissionsDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *PermissionsDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *PermissionsDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
