package attachment

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
	"github.com/gogf/gf/v2/errors/gerror"
)

// SaveAttachment 保存文件信息
func (a *sAttachment) SaveAttachment(ctx context.Context, attachs ...entity.Attachment) (attachId uint, err error) {
	if len(attachs) == 0 {
		return 0, gerror.New(`缺少附件信息`)
	}
	r, err := dao.Attachment.Ctx(ctx).OmitEmpty().Save(attachs)
	if err != nil {
		return 0, gerror.Newf(`保存附件失败%s`, err.Error())
	}
	id, _ := r.LastInsertId()
	return uint(id), nil
}
