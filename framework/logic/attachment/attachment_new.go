package attachment

import "gitee.com/bobo-rs/idea-space-framework/framework/service"

type sAttachment struct {
}

func init() {
	service.RegisterAttachment(New())
}

func New() *sAttachment {
	return &sAttachment{}
}
