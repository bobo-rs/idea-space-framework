package bizctx

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"github.com/gogf/gf/v2/net/ghttp"
)

type sBizCtx struct {
}

func init() {
	service.RegisterBizCtx(New())
}

func New() *sBizCtx {
	return &sBizCtx{}
}

// Init 初始化自定义上下文
func (c sBizCtx) Init(r *ghttp.Request, customCtx *model.Context) {
	r.SetCtxVar(enums.ContextDataBasicUser, customCtx)
}

// Get 获取自定义上下文
func (c *sBizCtx) Get(ctx context.Context) *model.Context {
	value := ctx.Value(enums.ContextDataBasicUser)
	if value == nil {
		return nil
	}
	// 断言类型
	if detail, ok := value.(*model.Context); ok {
		return detail
	}
	return nil
}

// SetUser 设置登录用户信息
func (c *sBizCtx) SetUser(ctx context.Context, user *model.UserAccountItem) {
	c.Get(ctx).User = user
}

// SetData 设置其他参数
func (c *sBizCtx) SetData(ctx context.Context, data map[enums.ContextDataKey]interface{}) {
	c.Get(ctx).Data = data
}

// SetDataValue 设置自定义参数值
func (c *sBizCtx) SetDataValue(ctx context.Context, key enums.ContextDataKey, value interface{}) {
	data := c.GetData(ctx)
	// 设置数据
	data[key] = value
	c.SetData(ctx, data)
}

// GetUser 获取用户信息
func (c *sBizCtx) GetUser(ctx context.Context) *model.UserAccountItem {
	return c.Get(ctx).User
}

// GetData 获取自定义数据
func (c *sBizCtx) GetData(ctx context.Context) map[enums.ContextDataKey]interface{} {
	return c.Get(ctx).Data
}

// GetUid 获取当前用户ID
func (c *sBizCtx) GetUid(ctx context.Context) uint {
	user := c.GetUser(ctx)
	if user == nil {
		return 0
	}
	return user.Uid
}

// GetValue 获取自定义参数值
func (c *sBizCtx) GetValue(ctx context.Context, key enums.ContextDataKey) interface{} {
	if v, ok := c.GetData(ctx)[key]; ok {
		return v
	}
	return nil
}
