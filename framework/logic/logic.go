package logic

import (
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/attachment"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/bizctx"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/common"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/config"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/cutout"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/design"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/label"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/material"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/middleware"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/rbac"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/smstpl"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/space"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/syslog"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/upload"
	_ "gitee.com/bobo-rs/idea-space-framework/framework/logic/user"
)
