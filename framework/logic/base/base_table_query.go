package base

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/pkg/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// ListAndTotal 获取数据列表和数量
func (s *TblBaseService) ListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, pointer interface{}, total *int) error {
	err := s.WhereErr(in.Where)
	if err != nil {
		return err
	}
	// 查询数据
	return g.Model(s.Table).
		Ctx(ctx).
		Where(in.Where).
		Page(in.Page, in.Size).
		Order(in.Sort).
		ScanAndCount(pointer, total, true)
}

// Scan 获取并扫描数据
func (s *TblBaseService) Scan(ctx context.Context, where, pointer interface{}) error {
	err := s.WhereErr(where)
	if err != nil {
		return err
	}
	return g.Model(s.Table).Ctx(ctx).Where(where).Scan(pointer)
}

// ScanOmitEmpty 过滤条件为空并获取数据
func (s *TblBaseService) ScanOmitEmpty(ctx context.Context, where, pointer interface{}) error {
	err := s.WhereErr(where)
	if err != nil {
		return err
	}
	return g.Model(s.Table).
		Ctx(ctx).
		OmitEmpty().
		Where(where).
		Scan(pointer)
}

// Total 获取数据总数
func (s *TblBaseService) Total(ctx context.Context, where interface{}) (int, error) {
	err := s.WhereErr(where)
	if err != nil {
		return 0, err
	}
	return g.Model(s.Table).Ctx(ctx).Where(where).Count()
}

// Exists 检测数据是否存在
func (s *TblBaseService) Exists(ctx context.Context, where interface{}) (bool, error) {
	total, err := s.Total(ctx, where)
	if err != nil {
		return false, err
	}
	return total > 0, nil
}

// UpdatePri 根据主键ID更新数据
func (s *TblBaseService) UpdatePri(ctx context.Context, id uint, column string, value interface{}) error {
	if id == 0 {
		return exception.New(`主键ID不能为空`)
	}
	_, err := g.Model(s.Table).
		Ctx(ctx).
		WherePri(id).
		Data(column, value).
		Update()
	if err != nil {
		return exception.New(`更新失败，请重试`)
	}
	return nil
}
