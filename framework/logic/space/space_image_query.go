package space

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessSpaceImageListAndTotal 获取设计空间图片列表和总数
func (s *sSpace) ProcessSpaceImageListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, list interface{}, total *int) error {
	if in.Where == nil || g.IsNil(in.Where) {
		return gerror.New(`缺少查询参数`)
	}
	var orm = dao.SpaceImages.Ctx(ctx).Where(in.Where).Page(in.Page, in.Size)
	if len(in.Sort) > 0 {
		orm = orm.Order(in.Sort)
	}
	return orm.ScanAndCount(list, total, true)
}

// ProcessSpaceImageScan 获取设计空间图片扫描数据
func (s *sSpace) ProcessSpaceImageScan(ctx context.Context, where, scan interface{}) error {
	if where == nil || g.IsNil(where) {
		return gerror.New(`缺少查询参数`)
	}
	return dao.SpaceImages.Ctx(ctx).Where(where).Scan(scan)
}

// ProcessSpaceImageList 获取设计空间图片列表
func (s *sSpace) ProcessSpaceImageList(ctx context.Context, where, list interface{}) error {
	return s.ProcessSpaceImageScan(ctx, where, list)
}

// ProcessSpaceImageDetail 获取设计空间图片详情
func (s *sSpace) ProcessSpaceImageDetail(ctx context.Context, where, detail interface{}) error {
	return s.ProcessSpaceImageScan(ctx, where, detail)
}

// ProcessSpaceImageDetailById 根据DI获取设计空间图片详情
func (s *sSpace) ProcessSpaceImageDetailById(ctx context.Context, id uint, detail interface{}) error {
	if id == 0 {
		return gerror.New(`缺少设计空间图片ID`)
	}
	return s.ProcessSpaceImageDetail(ctx, g.Map{dao.SpaceImages.Columns().Id: id}, detail)
}

// ProcessSpaceImageDetailWith 设计空间图片详情-模型关联
func (s *sSpace) ProcessSpaceImageDetailWith(ctx context.Context, where interface{}, item *model.SpaceDetailItem) error {
	item = &model.SpaceDetailItem{}
	// 设计图片空间基础信息
	err := s.ProcessSpaceImageDetail(
		ctx, where, &item,
	)

	if err != nil {
		return gerror.Wrapf(err, `查询设计空间图片失败%s`, err.Error())
	}
	// 设计空间图片详情
	err = dao.SpaceImagesDetail.Ctx(ctx).Scan(
		&item.Detail, dao.SpaceImagesDetail.Columns().SpaceId, item.Id,
	)

	// 设计空间图片属性
	err = dao.SpaceImagesAttr.Ctx(ctx).Scan(
		&item.Attr, dao.SpaceImagesAttr.Columns().SpaceId, item.Id,
	)
	return err
}

// CheckSpaceImageExists 检测图片设计基础谢谢是否存在
func (s *sSpace) CheckSpaceImageExists(ctx context.Context, where interface{}) (bool, error) {
	if where == nil || g.IsNil(where) {
		return false, gerror.New(`缺少请求参数`)
	}
	total, err := dao.SpaceImages.Ctx(ctx).Where(where).Count()
	if err != nil {
		return false, err
	}
	return total > 0, nil
}

// CheckSpaceImageExistsById 根据图片ID检测图片设计基础谢谢是否存在
func (s *sSpace) CheckSpaceImageExistsById(ctx context.Context, id uint) bool {
	r, _ := s.CheckSpaceImageExists(ctx, g.Map{
		dao.SpaceImages.Columns().Id: id,
	})
	return r
}
