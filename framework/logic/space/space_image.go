package space

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"github.com/gogf/gf/v2/frame/g"
)

// GetSpaceImageList 获取设计空间图片列表
func (s *sSpace) GetSpaceImageList(ctx context.Context, in model.SpaceListInput) (out *model.SpaceListOutput, err error) {
	out = &model.SpaceListOutput{}
	// 获取数据
	err = s.ProcessSpaceImageListAndTotal(
		ctx, model.CommonListAndTotalInput{
			Where:                s.FormatSpaceImageWhere(ctx, in.SpaceListWhereItem),
			CommonPaginationItem: in.CommonPaginationItem,
		}, &out.Rows, &out.Total,
	)
	if err != nil {
		return nil, err
	}
	// 数据为空
	if len(out.Rows) == 0 {
		return
	}
	// 格式化数据
	for key, row := range out.Rows {
		s.FormatSpaceDetail(&row)
		out.Rows[key] = row
	}
	return
}

// GetSpaceImageDetail 获取设计空间图片详情
func (s *sSpace) GetSpaceImageDetail(ctx context.Context, spaceId uint) (out *model.SpaceDetailOutput, err error) {
	out = &model.SpaceDetailOutput{}
	// 获取数据
	err = s.ProcessSpaceImageDetailWith(
		ctx, g.Map{dao.SpaceImages.Columns().Id: spaceId}, &out.SpaceDetailItem,
	)
	if err != nil {
		return nil, err
	}
	// 格式化详情
	s.FormatSpaceDetail(&out.SpaceImageItem)
	return
}

// GetSpaceDesignList 获取设计空间列表
func (s *sSpace) GetSpaceDesignList(ctx context.Context, in model.SpaceDesignListInput) (out *model.SpaceDesignListOutput, err error) {
	out = &model.SpaceDesignListOutput{}
	// 获取数据列表
	err = s.ProcessSpaceImageListAndTotal(ctx, model.CommonListAndTotalInput{
		CommonPaginationItem: in.CommonPaginationItem,
		Where: s.FormatSpaceImageWhere(ctx, model.SpaceListWhereItem{
			DesignType: in.DesignType,
			VipType:    in.VipType,
			Values:     in.Values,
			IsOpen:     in.IsOpen,
			ChannelId:  in.ChannelId,
			CreatorId:  in.CreatorId,
		}),
		Sort: in.SortType.Fmt(),
	}, &out.Rows, &out.Total)
	if err != nil {
		return nil, err
	}
	// 数据为空
	if len(out.Rows) == 0 {
		return out, nil
	}
	// 格式化属性
	for key, item := range out.Rows {
		s.FormatSpaceDetail(&item)
		out.Rows[key] = item
	}
	return
}

// GetSpaceDesignDetail 获取设计空间详情
func (s *sSpace) GetSpaceDesignDetail(ctx context.Context, spaceId uint) (out *model.SpaceDetailItem, err error) {
	out = &model.SpaceDetailItem{}
	// 获取基础信息
	err = s.ProcessSpaceImageDetailById(ctx, spaceId, &out.SpaceImageItem)
	if err != nil {
		return nil, err
	}
	// 基础详情
	err = s.ProcessDetailDetailById(ctx, spaceId, &out.Detail)
	if err != nil {
		return nil, err
	}
	// 基础属性
	err = s.ProcessAttrBySid(ctx, spaceId, &out.Attr)
	if err != nil {
		return nil, err
	}
	// 格式化数据
	s.FormatSpaceDetail(&out.SpaceImageItem)
	// 统计浏览量
	_ = s.IncSpaceDesignBrowse(ctx, spaceId)
	return out, nil
}
