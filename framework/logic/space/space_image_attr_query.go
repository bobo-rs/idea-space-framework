package space

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessAttrScan 扫描图片空间属性数据
func (s *sSpace) ProcessAttrScan(ctx context.Context, where, scan interface{}) error {
	if where == nil || g.IsNil(where) {
		return gerror.New(`缺少请求参数`)
	}
	return dao.SpaceImagesAttr.Ctx(ctx).Where(where).Scan(scan)
}

// ProcessAttrByAttachId 通过附件ID获取设计图片数据
func (s *sSpace) ProcessAttrByAttachId(ctx context.Context, attachId string, detail interface{}) error {
	return s.ProcessAttrScan(ctx, g.Map{dao.SpaceImagesAttr.Columns().AttachId: attachId}, detail)
}

// ProcessAttrBySid 通过设计图片ID获取设计图片属性
func (s *sSpace) ProcessAttrBySid(ctx context.Context, sid uint, detail interface{}) error {
	return s.ProcessAttrScan(ctx, g.Map{dao.SpaceImagesAttr.Columns().SpaceId: sid}, detail)
}
