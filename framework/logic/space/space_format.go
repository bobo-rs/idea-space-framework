package space

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"gitee.com/bobo-rs/idea-space-framework/maps"
	"github.com/gogf/gf/v2/frame/g"
)

// FormatSpaceDetail 格式化设计空间详情
func (s *sSpace) FormatSpaceDetail(detail *model.SpaceImageItem) {
	if v, ok := maps.VipType[detail.VipType]; ok {
		detail.FmtVip = v
	}
	if o, ok := maps.SpaceImageIsOpen[detail.IsOpen]; ok {
		detail.FmtOpen = o
	}
	if d, ok := maps.SpaceImageDesignType[detail.DesignType]; ok {
		detail.FmtDesign = d
	}
	if c, ok := maps.ChannelId[detail.ChannelId]; ok {
		detail.FmtChannel = c
	}
}

// FormatSpaceImageWhere 格式化设计空间查询条件
func (s *sSpace) FormatSpaceImageWhere(ctx context.Context, in model.SpaceListWhereItem) (where map[string]interface{}) {
	where = make(map[string]interface{})
	var err error
	defer func() {
		if err != nil {
			g.Log().Debug(ctx, `设计空间图片查询条件`, err)
		}
	}()
	// VIP类型
	if _, ok := maps.VipType[in.VipType]; ok {
		where[dao.SpaceImages.Columns().VipType] = in.VipType
	}
	if _, ok := maps.SpaceImageIsOpen[in.IsOpen]; ok {
		where[dao.SpaceImages.Columns().IsOpen] = in.IsOpen
	}
	if _, ok := maps.SpaceImageDesignType[in.DesignType]; ok {
		where[dao.SpaceImages.Columns().DesignType] = in.DesignType
	}
	if _, ok := maps.ChannelId[in.ChannelId]; ok {
		where[dao.SpaceImages.Columns().ChannelId] = in.ChannelId
	}
	// 标签值
	if len(in.Values) > 0 {
		where[dao.SpaceImages.Columns().Id], err = service.Label().ProcessIdxTypeAndHashByCid(
			ctx, enums.CorrelateTypeSpace, in.Values,
		)
	}
	return where
}
