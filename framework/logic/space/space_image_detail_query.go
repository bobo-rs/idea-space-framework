package space

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// CheckSpaceImageDetailExists 检测图片设计详情是否存在
func (s *sSpace) CheckSpaceImageDetailExists(ctx context.Context, where interface{}) (bool, error) {
	if where == nil || g.IsNil(where) {
		return false, gerror.New(`缺少请求参数`)
	}
	total, err := dao.SpaceImagesDetail.Ctx(ctx).Where(where).Count()
	if err != nil {
		return false, err
	}
	return total > 0, nil
}

// CheckSpaceImageDetailExistsBySpaceId 根据图片ID检测图片设计详情是否存在
func (s *sSpace) CheckSpaceImageDetailExistsBySpaceId(ctx context.Context, spaceId uint) bool {
	r, _ := s.CheckSpaceImageDetailExists(ctx, g.Map{
		dao.SpaceImagesDetail.Columns().SpaceId: spaceId,
	})
	return r
}

// CheckSpaceImageDetailExistsByTid 根据素材模板ID检测图片设计详情是否存在
func (s *sSpace) CheckSpaceImageDetailExistsByTid(ctx context.Context, tid uint) bool {
	r, _ := s.CheckSpaceImageDetailExists(ctx, g.Map{
		dao.SpaceImagesDetail.Columns().TemplateId: tid,
	})
	return r
}

// ProcessSpaceImageDetailScan 扫描设计图片空间数据
func (s *sSpace) ProcessSpaceImageDetailScan(ctx context.Context, where, scan interface{}) error {
	if where == nil || g.IsNil(where) {
		return gerror.New(`缺少请求参数`)
	}
	return dao.SpaceImagesDetail.Ctx(ctx).Where(where).Scan(scan)
}

// ProcessSpaceImageDetailDetailByTid 根据模板ID获取图片详情
func (s *sSpace) ProcessSpaceImageDetailDetailByTid(ctx context.Context, tid uint, detail interface{}) error {
	return s.ProcessSpaceImageDetailScan(ctx, g.Map{dao.SpaceImagesDetail.Columns().TemplateId: tid}, detail)
}

// ProcessDetailDetailById 根据设计空间ID获取详情
func (s sSpace) ProcessDetailDetailById(ctx context.Context, id uint, detail interface{}) error {
	return s.ProcessSpaceImageDetailScan(ctx, g.Map{dao.SpaceImagesDetail.Columns().SpaceId: id}, detail)
}
