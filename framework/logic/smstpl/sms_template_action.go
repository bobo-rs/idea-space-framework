package smstpl

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
	"gitee.com/bobo-rs/idea-space-framework/pkg/utils"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// GetTemplateList 获取SMS模板列表
func (t *sSmsTpl) GetTemplateList(ctx context.Context, in model.SmsTemplateListInput) (out *model.SmsTemplateListOutput, err error) {
	out = &model.SmsTemplateListOutput{}
	// 获取数据
	err = t.TemplateModel().ListAndTotal(ctx, model.CommonListAndTotalInput{
		CommonPaginationItem: in.CommonPaginationItem,
		Where:                t.TemplateWhere(in.SmsTemplateWhereItem),
		Sort:                 fmt.Sprintf(`%s desc,%s desc`, dao.SmsTemplate.Columns().Status, dao.SmsTemplate.Columns().Id),
	}, &out.Rows, &out.Total)
	if err != nil {
		return nil, err
	}

	// 获取当前外部SMS平台属性
	varAlias := utils.NewArray(out.Rows).ArrayColumnsUniqueString(dao.SmsTemplate.Columns().VarAlias)
	outMap, _ := t.CurrTemplateOutMapByAlias(ctx, varAlias...)

	// 迭代处理数据
	for key, item := range out.Rows {
		// 外部平台属性
		if tmplOut, ok := outMap[item.VarAlias]; ok {
			item.TmplOut = model.SmsOutItem{
				TemplateId:    tmplOut.TemplateId,
				FromType:      tmplOut.FromType,
				OutTmplStatus: tmplOut.OutTmplStatus,
			}
		}
		// 格式化数据
		t.FormatTemplate(&item)
		out.Rows[key] = item
	}
	return
}

// GetTemplateDetail 获取SMS短信模板详情
func (t *sSmsTpl) GetTemplateDetail(ctx context.Context, id uint) (out *model.SmsTemplateDetailOutput, err error) {
	out = &model.SmsTemplateDetailOutput{}
	// 获取SMS模板详情数据
	detail, err := t.ProcessTemplateDetailById(ctx, id)
	if err != nil {
		return nil, err
	}

	// 获取外部平台SMS模板属性
	outMap, _ := t.CurrTemplateOutMapByAlias(ctx, detail.VarAlias)
	if tmplOut, ok := outMap[detail.VarAlias]; ok {
		out.TmplOut = model.SmsOutItem{
			TemplateId:    tmplOut.TemplateId,
			FromType:      tmplOut.FromType,
			OutTmplStatus: tmplOut.OutTmplStatus,
		}
	}
	// 格式化数据
	out.SmsTemplate = *detail
	t.FormatTemplate(&out.SmsTemplateItem)

	return
}

// SaveTemplate 保存短信模板数据
func (t *sSmsTpl) SaveTemplate(ctx context.Context, tmpl entity.SmsTemplate) error {
	// 保存数据
	r, err := dao.SmsTemplate.Ctx(ctx).Save(tmpl)
	if err != nil {
		return gerror.Wrapf(err, `保存SMS模板失败%s`, err.Error())
	}

	// 记录日志
	defer func() {
		if err != nil {
			g.Log().Debug(ctx, `发布模板到外部平台`, err)
		}
	}()

	// 添加模板-发布模板到外部平台数据
	if tmpl.Id == 0 {
		tid, _ := r.LastInsertId()
		err = t.CurrApplyOutFromTemplate(ctx, uint(tid))
	}
	return nil
}

// DeleteTemplate 通过模板ID删除短信模板数据
func (t *sSmsTpl) DeleteTemplate(ctx context.Context, tid uint) error {
	detail, err := t.ProcessTemplateDetailById(ctx, tid)
	if err != nil {
		return err
	}
	// 是否关闭
	if detail.Status == uint(enums.SmsStatusOk) {
		return gerror.New(`正在使用的SMS模板不允许删除`)
	}

	// 删除SMS模板
	return dao.SmsTemplate.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err = tx.Delete(dao.SmsTemplate.Table(), g.Map{
			dao.SmsTemplate.Columns().Id: tid,
		})
		if err != nil {
			return gerror.Wrapf(err, `删除SMS模板失败%s`, err.Error())
		}
		// 删除外部平台关联模板
		return t.DeleteTemplateOutByVarAlias(ctx, enums.SmsAlias(detail.VarAlias))
	})
}
