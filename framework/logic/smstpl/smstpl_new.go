package smstpl

import "gitee.com/bobo-rs/idea-space-framework/framework/service"

type sSmsTpl struct {
}

func init() {
	service.RegisterSmsTpl(New())
}

func New() *sSmsTpl {
	return &sSmsTpl{}
}
