package smstpl

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessTemplateDetailById 通过模板ID获取SMS模板详情
func (t *sSmsTpl) ProcessTemplateDetailById(ctx context.Context, id uint) (*entity.SmsTemplate, error) {
	if id == 0 {
		return nil, gerror.New(`SMS模板ID不能为0`)
	}
	item := &entity.SmsTemplate{}
	err := t.TemplateModel().Scan(ctx, g.Map{
		dao.SmsTemplate.Columns().Id: id,
	}, &item)
	if err != nil {
		return nil, gerror.Wrapf(err, `SMS模板数据不存在%s`, err.Error())
	}
	// 是否为空
	if item == nil {
		return nil, gerror.New(`SMS模板不存在`)
	}
	return item, nil
}

// CheckTemplateExistsById 通过模板ID检测模板是否存在
func (t *sSmsTpl) CheckTemplateExistsById(ctx context.Context, tid uint) (bool, error) {
	return t.TemplateModel().Exists(ctx, g.Map{
		dao.SmsTemplate.Columns().Id: tid,
	})
}
