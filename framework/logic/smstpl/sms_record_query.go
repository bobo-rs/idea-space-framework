package smstpl

import (
	"context"
	"errors"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessRecordScanByMobiles 通过手机号获取短信记录
func (t *sSmsTpl) ProcessRecordScanByMobiles(ctx context.Context, mobile []string, scan interface{}) error {
	if len(mobile) == 0 {
		return errors.New(`请输入手机号`)
	}
	return t.RecordModel().Scan(ctx, g.Map{
		dao.SmsRecord.Columns().Mobile: mobile,
	}, scan)
}

// ProcessRecordNewByMobile 通过手机号获取最新发送记录
func (t *sSmsTpl) ProcessRecordNewByMobile(ctx context.Context, mobile string) (*model.SmsSendRecordNewItem, error) {
	item := &model.SmsSendRecordNewItem{}
	if len(mobile) == 0 {
		return nil, errors.New(`缺少手机号`)
	}
	err := dao.SmsRecord.Ctx(ctx).
		Where(dao.SmsRecord.Columns().Mobile, mobile).
		OrderDesc(dao.SmsRecord.Columns().Id).
		Scan(&item)
	return item, err
}
