package smstpl

import (
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/maps"
	"gitee.com/bobo-rs/idea-space-framework/pkg/utils"
)

// TemplateWhere 短信模板查询条件
func (t *sSmsTpl) TemplateWhere(in model.SmsTemplateWhereItem) map[string]interface{} {
	var (
		columns = dao.SmsTemplate.Columns()
		where   = make(map[string]interface{})
	)

	// 模板名
	if len(in.Name) > 0 {
		where[dao.SmsTemplate.Columns().Name] = in.Name
	}

	// 别名
	if _, ok := maps.SmsTemplateVarAlias[enums.SmsAlias(in.Alias)]; ok {
		where[columns.VarAlias] = in.Alias
	}

	// 状态
	if _, ok := maps.SmsTemplateSmsType[enums.SmsType(in.SmsType)]; ok {
		where[columns.SmsType] = in.SmsType
	}

	// 状态
	if _, ok := maps.SmsTemplateStatus[enums.SmsStatus(in.Status)]; ok {
		where[columns.Status] = in.Status
	}
	return where
}

// RecordWhere 短信记录查询条件
func (t *sSmsTpl) RecordWhere(in model.SmsRecordWhereItem) map[string]interface{} {
	var (
		columns = dao.SmsRecord.Columns()
		where   = make(map[string]interface{})
	)
	// 手机号
	if len(in.Mobile) > 0 {
		where[columns.Mobile] = utils.SeparatorStringToArray(in.Mobile)
	}
	// 内容
	if len(in.Content) > 0 {
		where[columns.Content+` like?`] = `%` + in.Content + `%`
	}
	if _, ok := maps.SmsTemplateSmsType[enums.SmsType(in.SmsType)]; ok {
		where[columns.SmsType] = in.SmsType
	}
	return where
}

// FormatTemplate 格式化短信模板
func (t *sSmsTpl) FormatTemplate(detail *model.SmsTemplateItem) {
	// 别名
	detail.FmtAlias = maps.SmsTemplateVarAlias[enums.SmsAlias(detail.VarAlias)]
	detail.FmtSmsType = maps.SmsTemplateSmsType[enums.SmsType(detail.SmsType)]
	detail.FmtStatus = maps.SmsTemplateStatus[enums.SmsStatus(detail.Status)]
}

// FormatRecord 格式化短信记录
func (t *sSmsTpl) FormatRecord(detail *model.SmsRecordDetailItem) {
	// 类型
	detail.FmtSmsType = maps.SmsTemplateSmsType[enums.SmsType(detail.SmsType)]
}
