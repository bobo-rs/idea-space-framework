package smstpl

import (
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/logic/base"
)

// RecordModel 短信记录model查询
func (t *sSmsTpl) RecordModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.SmsRecord.Table(),
	}
}

// TemplateModel 短信模板model查询
func (t *sSmsTpl) TemplateModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.SmsTemplate.Table(),
	}
}

// OutModel 外部短信模板Model查询
func (t *sSmsTpl) OutModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.SmsTemplateOut.Table(),
	}
}
