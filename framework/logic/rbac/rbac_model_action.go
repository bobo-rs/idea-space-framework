package rbac

import (
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/logic/base"
)

// PermissionsModel 权限模型
func (r *sRbac) PermissionsModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.Permissions.Table(),
	}
}

// AssocModel 权限关联角色模型
func (r *sRbac) AssocModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.PermissonsAssoc.Table(),
	}
}

// RolesModel 角色模型
func (r *sRbac) RolesModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.Roles.Table(),
	}
}

// DepartmentRoleModel 部门关联角色Model
func (r *sRbac) DepartmentRoleModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.DepartmentRoles.Table(),
	}
}

// DepartmentModel 部门Model
func (r *sRbac) DepartmentModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.Department.Table(),
	}
}

// AdminDepartmentModel 管理员部门关联Model
func (r *sRbac) AdminDepartmentModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.AdminDepartment.Table(),
	}
}

// UserAdminModel 管理员用户Model
func (r *sRbac) UserAdminModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.UserAdmin.Table(),
	}
}
