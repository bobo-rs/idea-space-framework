package rbac

import "gitee.com/bobo-rs/idea-space-framework/framework/service"

func init() {
	service.RegisterRbac(New())
}
