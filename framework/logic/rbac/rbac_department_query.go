package rbac

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/pkg/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessDepartmentList 处理并获取部门列表
func (r *sRbac) ProcessDepartmentList(ctx context.Context, where interface{}) ([]model.DepartmentTreeItem, error) {
	// 获取部门树形列表
	var (
		departRows []model.DepartmentTreeItem
	)
	err := dao.Department.Ctx(ctx).
		Where(where).
		OrderAsc(dao.Department.Columns().Sort).
		Scan(&departRows)
	if err != nil {
		return nil, exception.New(`暂无部门数据`)
	}
	return departRows, nil
}

// CheckChildDepartByDepartId 通过部门ID检查子级部门是否存在
func (r *sRbac) CheckChildDepartByDepartId(ctx context.Context, departId uint) bool {
	if departId == 0 {
		return false
	}
	// 是否存在子级部门
	b, _ := r.DepartmentModel().Exists(ctx, g.Map{
		dao.Department.Columns().Pid: departId,
	})
	return b
}

// CheckDepartmentById 通过部门ID检测部门是否存在
func (r *sRbac) CheckDepartmentById(ctx context.Context, id uint) bool {
	if id == 0 {
		return false
	}
	// 是否存在
	b, _ := r.DepartmentModel().Exists(ctx, g.Map{
		dao.Department.Columns().Id: id,
	})
	return b
}

// DepartmentTotalById 通过部门ID获取部门数量
func (r *sRbac) DepartmentTotalById(ctx context.Context, id ...uint) int {
	if len(id) == 0 {
		return 0
	}
	total, _ := r.DepartmentModel().Total(ctx, g.Map{
		dao.Department.Columns().Id: id,
	})
	return total
}

// CheckDepartmentIsDisableById 通过部门ID检测部门是否被禁用：true正常，false禁用
func (r *sRbac) CheckDepartmentIsDisableById(ctx context.Context, id uint) bool {
	b, _ := r.DepartmentModel().Exists(ctx, g.Map{
		dao.Department.Columns().Id:     id,
		dao.Department.Columns().Status: enums.DepartStatusOk,
	})
	return b
}
