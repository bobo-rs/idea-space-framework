package rbac

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/consts"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"gitee.com/bobo-rs/idea-space-framework/pkg/exception"
	"gitee.com/bobo-rs/idea-space-framework/pkg/sredis"
	"gitee.com/bobo-rs/idea-space-framework/pkg/utils"
)

// AuthAdminMenu 获取管理员授权权限菜单列表
func (r *sRbac) AuthAdminMenu(ctx context.Context) (treeList []map[string]interface{}, err error) {
	var (
		admAuth   = service.User().GetAdminUser(ctx)
		userAuth  = service.BizCtx().GetUser(ctx)
		cacheId   = utils.CacheKey(consts.LoginAdminAuthPermMenu, userAuth.AccountToken)
		cacheFunc = func(ctx context.Context) (interface{}, error) {
			// 获取权限ID
			var permId []uint
			if enums.AdminIsSuperManage(admAuth.IsSuperManage) == enums.AdminIsSuperMangeOrdinary {
				permId, err = r.AuthAdminPermId(ctx)
				if err != nil {
					return nil, err
				}
			}

			// 获取权限列表
			return r.GetPermMenuTreeList(ctx, permId...)
		}
	)

	// 缓存权限数据
	res, err := sredis.NewCache().GetOrSetFunc(ctx, cacheId, cacheFunc, consts.LoginAuthExpire)
	if err != nil {
		return nil, err
	}

	// 转换为MAP列表
	return res.Maps(), nil
}

// AuthAdminMenuActionCode 获取管理员授权菜单权限码
func (r *sRbac) AuthAdminMenuActionCode(ctx context.Context) (out *model.PermCodeActionItem, err error) {
	var (
		admAuth   = service.User().GetAdminUser(ctx)
		userAuth  = service.BizCtx().GetUser(ctx)
		cacheId   = utils.CacheKey(consts.LoginAdminAuthPermAction, userAuth.AccountToken)
		cacheFunc = func(ctx context.Context) (interface{}, error) {
			// 获取权限ID
			var permId []uint
			if enums.AdminIsSuperManage(admAuth.IsSuperManage) == enums.AdminIsSuperMangeOrdinary {
				permId, err = r.AuthAdminPermId(ctx)
				if err != nil {
					return nil, err
				}
			}
			// 获取菜单权限码
			return r.GetMenuActionCode(ctx, permId...)
		}
	)

	// 缓存菜单权限码
	res, err := sredis.NewCache().GetOrSetFunc(ctx, cacheId, cacheFunc, consts.LoginAuthExpire)
	if err != nil {
		return nil, err
	}

	// 转换格式
	out = &model.PermCodeActionItem{}
	if err = res.Struct(&out); err != nil {
		return nil, exception.New(`转换菜单权限码格式错误`)
	}
	return
}

// AuthAdminGetDetail 获取鉴权管理员详情
func (r *sRbac) AuthAdminGetDetail(ctx context.Context) (out *model.UserAdminAuthDetailOutput) {
	out = &model.UserAdminAuthDetailOutput{}
	// 获取鉴权管理员
	admAuth := service.User().GetAdminUser(ctx)
	// 用户鉴权详情
	userDetail := service.User().GetUserAuthDetail(ctx)
	if admAuth == nil || userDetail == nil {
		return nil
	}
	return &model.UserAdminAuthDetailOutput{
		UserAdminDetailItem: *admAuth,
		Detail:              *userDetail,
	}
}

// AuthAdminPermId 获取管理员授权权限ID集合
func (r *sRbac) AuthAdminPermId(ctx context.Context) ([]uint, error) {
	var (
		admAuth   = service.User().GetAdminUser(ctx)
		userAuth  = service.BizCtx().GetUser(ctx)
		cacheId   = utils.CacheKey(consts.LoginAdminAuthPermId, userAuth.AccountToken)
		cacheFunc = func(ctx context.Context) (interface{}, error) {
			// 获取部门ID
			departId, err := r.GetDepartmentIdByAid(ctx, admAuth.Id)
			if err != nil {
				return nil, err
			}

			// 获取权限ID集合
			permId, err := r.GetPermissionsIdByDepartId(ctx, departId...)
			if err != nil {
				return nil, err
			}

			// 获取指定权限ID
			customPermId, _ := r.GetPermissionsIdByAdminId(ctx, admAuth.Id)
			if len(customPermId) > 0 {
				permId = append(permId, customPermId...)
			}
			return permId, nil
		}
	)
	// 缓存权限ID
	res, err := sredis.NewCache().GetOrSetFunc(ctx, cacheId, cacheFunc, consts.LoginAuthExpire)
	if err != nil {
		return nil, err
	}
	return res.Uints(), nil
}
