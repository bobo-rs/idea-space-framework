package rbac

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"github.com/gogf/gf/v2/frame/g"
)

// CheckPermissionsExistsById 通过权限ID检测权限是否存在
func (r *sRbac) CheckPermissionsExistsById(ctx context.Context, id uint) bool {
	b, _ := r.PermissionsModel().Exists(ctx, g.Map{
		dao.Permissions.Columns().Id: id,
	})
	return b
}

// CheckPermissionsExistsByPid 通过父级ID检测子级权限是否存在
func (r *sRbac) CheckPermissionsExistsByPid(ctx context.Context, pid uint) bool {
	b, _ := r.PermissionsModel().Exists(ctx, g.Map{
		dao.Permissions.Columns().Pid: pid,
	})
	return b
}

// GetPermissionsTotalByPid 通过父级ID获取子权限数量
func (r *sRbac) GetPermissionsTotalByPid(ctx context.Context, pid uint) int {
	t, _ := r.PermissionsModel().Total(ctx, g.Map{
		dao.Permissions.Columns().Pid: pid,
	})
	return t
}
