package rbac

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/pkg/exception"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SaveAdminUser 保存管理员信息
func (r *sRbac) SaveAdminUser(ctx context.Context, user model.UserAdminSaveItem) error {
	// 保存管理员信息
	return dao.UserAdmin.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		// 保存管理员信息
		res, err := dao.UserAdmin.Ctx(ctx).OmitEmpty().Save(user.UserAdmin)
		if err != nil {
			return exception.New(`保存管理员信息失败`)
		}

		// 管理员ID
		adminId, err := res.LastInsertId()
		if err != nil {
			return exception.New(`管理员保存失败`)
		}

		// 保存部门
		if len(user.DepartId) > 0 {
			err = r.SaveAdminDepartment(ctx, uint(adminId), user.DepartId...)
			if err != nil {
				return err
			}
		}

		// 保存自定义权限
		if len(user.PermId) > 0 {
			err = r.SavePermAssoc(ctx, uint(adminId), enums.PermAssocTypeUser, user.PermId...)
		}
		return err
	})
}

// GetAdminUserList 获取管理员列表
func (r *sRbac) GetAdminUserList(ctx context.Context, in model.UserAdminGetListInput) (out *model.UserAdminGetListOutput, err error) {
	out = &model.UserAdminGetListOutput{}

	// 获取管理员列表
	err = r.UserAdminModel().ListAndTotal(ctx, model.CommonListAndTotalInput{
		Where:                r.UserAdminWhere(in.UserAdminWhereItem),
		CommonPaginationItem: in.CommonPaginationItem,
	}, &out.Rows, &out.Total)
	if err != nil {
		return nil, err
	}

	// 到底了
	if len(out.Rows) == 0 {
		return out, nil
	}

	// 格式化
	for _, item := range out.Rows {
		r.FormatUserAdmin(&item)
	}
	return
}

// GetUserAdminDetail 获取管理员详情
func (r *sRbac) GetUserAdminDetail(ctx context.Context, id uint) (detail *model.UserAdminGetDetailOutput, err error) {
	detail = &model.UserAdminGetDetailOutput{}
	// 获取管理员详情
	if err = r.ProcessAdminDetailByAid(ctx, id, &detail); err != nil {
		return nil, exception.New(`管理员信息不存在`)
	}

	// 获取部门ID
	detail.DepartId, _ = r.GetDepartmentIdByAid(ctx, id)
	if len(detail.DepartId) > 0 {
		detail.DepartList, _ = r.GetDepartNameByDepartId(ctx, detail.DepartId...)
	}

	// 获取权限列表
	detail.PermId, _ = r.GetPermissionsIdByAdminId(ctx, id)
	return
}

// RemoveUserAdmin 移除管理员信息
func (r *sRbac) RemoveUserAdmin(ctx context.Context, id uint) error {
	if id == 0 {
		return exception.New(`缺少管理员ID`)
	}
	// 移除管理员
	return dao.UserAdmin.Transaction(ctx, func(ctx context.Context, tx gdb.TX) error {
		_, err := tx.Ctx(ctx).Delete(dao.UserAdmin.Table(), g.Map{
			dao.UserAdmin.Columns().Id: id,
		})
		if err != nil {
			return exception.New(`移除管理员失败，请重试`)
		}

		// 移除关联部门
		err = r.RemoveAdminDepartByAid(ctx, id)
		if err != nil {
			return err
		}

		// 移除关联权限
		if err = r.RemovePermAssocByAssocId(ctx, enums.PermAssocTypeUser, id); err != nil {
			return err
		}
		return nil
	})
}
