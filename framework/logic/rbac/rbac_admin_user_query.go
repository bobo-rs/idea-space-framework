package rbac

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/pkg/exception"
	"gitee.com/bobo-rs/idea-space-framework/pkg/utils"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessAdminDetail 获取管理员详情
func (r *sRbac) ProcessAdminDetail(ctx context.Context, where, pointer interface{}) error {
	return r.UserAdminModel().Scan(ctx, where, pointer)
}

// ProcessAdminDetailByAid 通过管理员ID获取管理员信息
func (r *sRbac) ProcessAdminDetailByAid(ctx context.Context, aid uint, pointer interface{}) error {
	return r.ProcessAdminDetail(ctx, g.Map{
		dao.UserAdmin.Columns().Id: aid,
	}, pointer)
}

// CheckAdminExistsId 检测管理员是否存在
func (r *sRbac) CheckAdminExistsId(ctx context.Context, aid uint) bool {
	b, _ := r.UserAdminModel().Exists(ctx, g.Map{
		dao.UserAdmin.Columns().Id: aid,
	})
	return b
}

// GetUidByAid 通过管理员ID获取用户ID
func (r *sRbac) GetUidByAid(ctx context.Context, id ...uint) ([]uint, error) {
	if len(id) == 0 {
		return nil, exception.New(`缺少管理员ID`)
	}
	var rows []model.UidByAidItem
	err := r.UserAdminModel().Scan(ctx, g.Map{
		dao.UserAdmin.Columns().Id: id,
	}, &rows)
	if err != nil {
		return nil, exception.New(`暂无数据`)
	}

	// 获取管理员用户ID
	return utils.NewArray(rows).ArrayColumnsUniqueUnit(dao.UserAdmin.Columns().UserId), nil
}
