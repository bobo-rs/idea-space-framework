package user

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
	"gitee.com/bobo-rs/idea-space-framework/pkg/exception"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessDetailByMobile 通过手机号获取用户详情
func (u *sUser) ProcessDetailByMobile(ctx context.Context, mobile string, pointer interface{}) error {
	if len(mobile) == 0 {
		return gerror.New(`缺少手机号`)
	}
	return u.UserModel().Scan(ctx, g.Map{
		dao.User.Columns().Mobile: mobile,
	}, pointer)
}

// ProcessDetailByUid 通过用户ID获取用户详情
func (u *sUser) ProcessDetailByUid(ctx context.Context, uid uint, pointer interface{}) error {
	if uid == 0 {
		return gerror.New(`用户ID不能为空`)
	}
	return u.UserModel().Scan(ctx, g.Map{
		dao.User.Columns().Id: uid,
	}, pointer)
}

// ProcessDetailByAccount 通过用户账户获取用户详情
func (u *sUser) ProcessDetailByAccount(ctx context.Context, account string, pointer interface{}) error {
	if len(account) == 0 {
		return gerror.New(`缺少账户`)
	}
	return u.UserModel().Scan(ctx, g.Map{
		dao.User.Columns().Account: account,
	}, pointer)
}

// CheckUserExistsByMobile 通过手机号-检测用户是否存在
func (u *sUser) CheckUserExistsByMobile(ctx context.Context, mobile string) bool {
	b, _ := u.UserModel().Exists(ctx, g.Map{
		dao.User.Columns().Mobile: mobile,
	})
	return b
}

// CheckUserExistsByAccount 通过账号-检查用户是否存在
func (u *sUser) CheckUserExistsByAccount(ctx context.Context, account string) bool {
	b, _ := u.UserModel().Exists(ctx, g.Map{
		dao.User.Columns().Account: account,
	})
	return b
}

// CheckUserExistsByUid 通过用户ID检测用户是否存在
func (u *sUser) CheckUserExistsByUid(ctx context.Context, uid uint) bool {
	b, _ := u.UserModel().Exists(ctx, g.Map{
		dao.User.Columns().Id: uid,
	})
	return b
}

// ProcessAccountNameByUid 通过用户ID获取账户名
func (u *sUser) ProcessAccountNameByUid(ctx context.Context, uid uint) (*model.UserAccountNameItem, error) {
	var detail model.UserAccountNameItem
	err := u.ProcessDetailByUid(ctx, uid, &detail)
	return &detail, err
}

// ProcessUserByUid 通过用户ID获取用户信息
func (u *sUser) ProcessUserByUid(ctx context.Context, uid uint) (*entity.User, error) {
	detail := &entity.User{}
	err := u.ProcessDetailByUid(ctx, uid, detail)
	if err != nil {
		return nil, exception.New(`用户信息不存在`)
	}
	return detail, nil
}
