package user

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessAdminDetail 获取管理员详情
func (u *sUser) ProcessAdminDetail(ctx context.Context, where interface{}, detail *model.UserAdminDetailItem) error {
	err := u.AdminModel().Scan(ctx, where, detail)
	if err != nil {
		return err
	}
	// 是否查询到数据
	if detail == nil {
		return gerror.New(`暂无数据`)
	}
	return nil
}

// ProcessAdminDetailByUid 通过用户ID获取管理员信息
func (u *sUser) ProcessAdminDetailByUid(ctx context.Context, uid uint) (detail *model.UserAdminDetailItem, err error) {
	detail = &model.UserAdminDetailItem{}
	err = u.ProcessAdminDetail(ctx, g.Map{
		dao.UserAdmin.Columns().UserId: uid,
	}, detail)
	return
}

// ProcessAdminDetailById 通过管理员ID获取管理员信息
func (u *sUser) ProcessAdminDetailById(ctx context.Context, adminId uint) (detail *model.UserAdminDetailItem, err error) {
	detail = &model.UserAdminDetailItem{}
	err = u.ProcessAdminDetail(ctx, g.Map{
		dao.UserAdmin.Columns().Id: adminId,
	}, detail)
	return
}
