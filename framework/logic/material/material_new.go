package material

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
)

// GetMaterialList 获取素材列表
func (m *sMaterial) GetMaterialList(ctx context.Context, in model.MaterialListInput) (out *model.MaterialListOutput, err error) {
	out = &model.MaterialListOutput{}
	// 获取数据列表
	err = m.ProcessMaterialListAndTotal(
		ctx,
		model.CommonListAndTotalInput{
			CommonPaginationItem: in.CommonPaginationItem,
			Where:                in.MaterialListWhereItem,
		},
		&out.Rows,
		&out.Total,
	)
	if err != nil {
		return nil, err
	}
	// 数据是否存在
	if len(out.Rows) == 0 {
		return
	}
	// 处理数据
	for key, row := range out.Rows {
		m.FormatMaterialDetail(&row)
		out.Rows[key] = row
	}
	return
}

// GetMaterialDetail 获取素材详情
func (m *sMaterial) GetMaterialDetail(ctx context.Context, materialId uint) (out *model.MaterialDetailOutput, err error) {
	out = &model.MaterialDetailOutput{}
	// 获取数据
	err = m.ProcessMaterialDetailById(
		ctx, materialId, &out,
	)
	if err != nil {
		return nil, err
	}
	// 格式化处理数据
	m.FormatMaterialDetail(&out.MaterialDetailItem)
	return
}
