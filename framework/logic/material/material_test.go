package material

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	_ "github.com/gogf/gf/contrib/drivers/mysql/v2"
	"github.com/gogf/gf/v2/frame/g"
	"regexp"
	"testing"
)

var (
	Ctx = context.Background()
)

func TestMatchMaterial(t *testing.T) {
	re := regexp.MustCompile(`^#+[a-z0-9]{3,6}`)
	fmt.Println(re.MatchString(`#fff000`))

	fmt.Println(re.MatchString(`fff000`))
	fmt.Println(re.MatchString(`#fff`))
	fmt.Println(re.MatchString(`#000`))
}

func TestSMaterial_ProcessTemplateWordsWithList(t *testing.T) {
	var words []model.MaterialTemplateWordsDetailItem
	fmt.Println(New().ProcessTemplateWordsWithList(Ctx, g.Map{`template_id`: 1}))
	fmt.Println(words)
}

func TestSMaterial_RefreshMaterialTemplateExampleImg(t *testing.T) {
	fmt.Println(New().RefreshMaterialTemplateExampleImg(Ctx, 3))
}
