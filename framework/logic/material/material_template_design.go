package material

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/consts"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/models"
	"gitee.com/bobo-rs/idea-space-framework/pkg/encrypt"
	"gitee.com/bobo-rs/idea-space-framework/pkg/utils"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gcache"
	"github.com/gogf/gf/v2/os/gtime"
)

const (
	CacheDesignMatchExpire = gtime.M * 30
)

// GetMatchTemplateList 获取匹配成功模板列表
func (m *sMaterial) GetMatchTemplateList(ctx context.Context, tid ...uint) (out []model.MatchDesignTemplateItem, err error) {
	if len(tid) == 0 {
		return nil, gerror.New(`缺少模板ID`)
	}
	out = []model.MatchDesignTemplateItem{}
	// 获取模板列表
	err = m.ProcessMaterialTemplateScan(ctx, g.Map{
		dao.MaterialTemplate.Columns().Id: tid,
	}, &out)
	if err != nil {
		return nil, err
	}
	return
}

// DesignTemplateMatchCacheList 设计模板匹配列表
func (m *sMaterial) DesignTemplateMatchCacheList(ctx context.Context, in model.DesignTemplateMatchCacheListInput) (out *model.DesignTemplateMatchGetListOutput, err error) {
	out = &model.DesignTemplateMatchGetListOutput{}
	var (
		cacheKey  = utils.CacheKey(consts.CacheDesignTemplateMatchLabelHash, encrypt.NewM().MustEncryptString(in.LabelValues))
		cacheFunc = func(ctx context.Context) (interface{}, error) {
			where := g.Map{
				dao.MaterialTemplate.Columns().Status:    1,
				dao.MaterialTemplate.Columns().IdxStatus: 1,
			}
			where[dao.MaterialTemplate.Columns().Id], err = service.Label().ProcessIdxTypeAndHashByCid(
				ctx, enums.CorrelateTypeTemp, in.LabelValues,
			)
			if err != nil {
				return nil, err
			}
			// 获取素材模板列表
			err = m.ProcessMaterialTemplateScan(
				ctx, where, &out.List, fmt.Sprintf(`%s desc`, dao.MaterialTemplate.Columns().MatchNum),
			)
			// 模板列表
			if len(out.List) == 0 {
				return nil, gerror.New(`未匹配到可用模板，请更换行业标签`)
			}
			return out.List, err
		}
	)
	// 缓存数据
	r, err := gcache.GetOrSetFunc(ctx, cacheKey, cacheFunc, CacheDesignMatchExpire)
	if err != nil {
		return nil, err
	}
	if err = r.Structs(&out.List); err != nil {
		return nil, gerror.Wrapf(err, `扫描设计模板匹配列表到DesignTemplateMatchGetListOutput失败，%s`, err.Error())
	}
	for _, item := range out.List {
		// 统计切词数量
		out.CutWordsList = append(out.CutWordsList, models.DesignTemplateIdAndNumItem{
			Id:  item.Id,
			Num: item.CutWordsNum,
		})
		// 卖点长度
		out.SellingList = append(out.SellingList, models.DesignTemplateIdAndNumItem{
			Id:  item.Id,
			Num: item.SellingLen,
		})
	}
	return
}

// IncMaterialTemplateUseNum 模板使用量叠加
func (m *sMaterial) IncMaterialTemplateUseNum(ctx context.Context, tid []uint, amount ...uint) error {
	if len(tid) == 0 {
		return gerror.New(`缺少模板ID`)
	}
	var num uint = 1
	if len(amount) > 0 {
		num = amount[0]
	}
	// 使用量加一
	_, err := dao.MaterialTemplate.Ctx(ctx).
		Where(dao.MaterialTemplate.Columns().Id, tid).
		Increment(dao.MaterialTemplate.Columns().UseNum, num)
	return err
}

// IncMaterialTemplateMatchNum 模板匹配量叠加
func (m *sMaterial) IncMaterialTemplateMatchNum(ctx context.Context, tid []uint, amount ...uint) error {
	if len(tid) == 0 {
		return gerror.New(`缺少模板ID`)
	}
	var num uint = 1
	if len(amount) > 0 {
		num = amount[0]
	}
	// 匹配量加一
	_, err := dao.MaterialTemplate.Ctx(ctx).
		Where(dao.MaterialTemplate.Columns().Id, tid).
		Increment(dao.MaterialTemplate.Columns().MatchNum, num)
	return err
}
