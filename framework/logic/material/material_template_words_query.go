package material

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/models"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessTemplateWordsList 获取素材模板列表
func (m *sMaterial) ProcessTemplateWordsList(ctx context.Context, where, list interface{}) error {
	if where == nil || g.IsNil(where) {
		return gerror.New(`缺少请求参数`)
	}
	return dao.MaterialTemplateWords.Ctx(ctx).
		OmitEmptyWhere().
		Where(where).
		OrderAsc(dao.MaterialTemplateWords.Columns().LayerPosition).
		Scan(list)
}

// ProcessTemplateWordsListByTid 根据模板ID获取模板文案列表
func (m *sMaterial) ProcessTemplateWordsListByTid(ctx context.Context, tid uint, list interface{}) error {
	if tid == 0 {
		return gerror.New(`缺少素材模板ID`)
	}
	return m.ProcessTemplateWordsList(
		ctx, g.Map{dao.MaterialTemplateWords.Columns().TemplateId: tid}, list,
	)
}

// ProcessTemplateWordsDetail 获取模板文案详情
func (m *sMaterial) ProcessTemplateWordsDetail(ctx context.Context, where, detail interface{}) error {
	if where == nil || g.IsNil(where) {
		return gerror.New(`缺少请求参数`)
	}
	return dao.MaterialTemplateWords.Ctx(ctx).
		OmitEmptyWhere().
		Where(where).
		Scan(detail)
}

// ProcessTemplateWordsDetailById 根据文案ID获取模板文案详情
func (m *sMaterial) ProcessTemplateWordsDetailById(ctx context.Context, id uint, detail interface{}) error {
	if id == 0 {
		return gerror.New(`缺少素材模板文案ID`)
	}
	return m.ProcessTemplateWordsDetail(
		ctx, g.Map{dao.MaterialTemplateWords.Columns().Id: id}, detail,
	)
}

// CheckTemplateWordsExists 检测素材模板文案是否存在
func (m *sMaterial) CheckTemplateWordsExists(ctx context.Context, where interface{}) (bool, error) {
	if where == nil || g.IsNil(where) {
		return false, gerror.New(`缺少请求参数`)
	}
	total, err := dao.MaterialTemplateWords.Ctx(ctx).
		OmitEmpty().
		Where(where).
		Count()
	if err != nil {
		return false, err
	}
	return total > 0, nil
}

// CheckTemplateWordsExistsById 通过素材文案ID检测素材文案是否存在
func (m *sMaterial) CheckTemplateWordsExistsById(ctx context.Context, id uint) bool {
	res, err := m.CheckMaterialTemplateExists(
		ctx, g.Map{dao.MaterialTemplateWords.Columns().Id: id},
	)
	if err != nil {
		return false
	}
	return res
}

// CheckTemplateWordsExistsByTemplateId 通过素材模板ID检测素材文案是否存在
func (m *sMaterial) CheckTemplateWordsExistsByTemplateId(ctx context.Context, tid uint) bool {
	res, err := m.CheckMaterialTemplateExists(
		ctx, g.Map{dao.MaterialTemplateWords.Columns().TemplateId: tid},
	)
	if err != nil {
		return false
	}
	return res
}

// CheckTemplateWordsExistsByMaterialId 通过素材ID检测素材文案是否存在
func (m *sMaterial) CheckTemplateWordsExistsByMaterialId(ctx context.Context, mid uint) bool {
	res, err := m.CheckMaterialTemplateExists(
		ctx, g.Map{dao.MaterialTemplateWords.Columns().MaterialId: mid},
	)
	if err != nil {
		return false
	}
	return res
}

// ProcessTemplateWordsWithList 模板素材文案关联查询素材信息
func (m *sMaterial) ProcessTemplateWordsWithList(ctx context.Context, where interface{}) (out []model.MaterialTemplateWordsDetailItem, err error) {
	// 初始化
	out = []model.MaterialTemplateWordsDetailItem{}
	if where == nil || g.IsNil(where) {
		return nil, gerror.New(`缺少请求参数`)
	}
	// 查询数据
	err = dao.MaterialTemplateWords.Ctx(ctx).
		Where(where).
		Scan(&out)
	if err != nil {
		return nil, err
	}

	// 模型关联-素材
	err = dao.Material.Ctx(ctx).
		Where(dao.Material.Columns().Id, gdb.ListItemValuesUnique(out, `MaterialId`)).
		ScanList(&out, `Material`, "", "id:MaterialId")
	return out, err
}

// ProcessMaterialTemplateWordsScan 获取素材模板文本数据
func (m *sMaterial) ProcessMaterialTemplateWordsScan(ctx context.Context, where, scan interface{}) error {
	if where == nil || g.IsNil(where) {
		return gerror.New(`缺少请求参数`)
	}
	return dao.MaterialTemplateWords.Ctx(ctx).
		Where(where).
		OrderAsc(dao.MaterialTemplateWords.Columns().LayerPosition).
		Scan(scan)
}

// ProcessMaterialTemplateWordsListByTid 根据模板ID获取素材文本列表
func (m *sMaterial) ProcessMaterialTemplateWordsListByTid(ctx context.Context, tid uint, list interface{}) error {
	return m.ProcessMaterialTemplateWordsScan(
		ctx, g.Map{dao.MaterialTemplateWords.Columns().TemplateId: tid}, list,
	)
}

// ProcessMaterialTemplateWordsTotal 获取素材模板文本总数
func (m *sMaterial) ProcessMaterialTemplateWordsTotal(ctx context.Context, where interface{}) (int, error) {
	if where == nil || g.IsNil(where) {
		return 0, gerror.New(`缺少请求参数`)
	}
	return dao.MaterialTemplateWords.Ctx(ctx).Where(where).Count()
}

// CheckMaterialTemplateWordsExists 检查素材模板文本是否存在
func (m *sMaterial) CheckMaterialTemplateWordsExists(ctx context.Context, where interface{}) (bool, error) {
	total, err := m.ProcessMaterialTemplateWordsTotal(ctx, where)
	if err != nil {
		return false, err
	}
	return total > 0, nil
}

// CheckMaterialTemplateWordsExistsByTid 根据素材模板ID验证素材文本是否存在
func (m *sMaterial) CheckMaterialTemplateWordsExistsByTid(ctx context.Context, tid uint) bool {
	r, _ := m.CheckMaterialTemplateWordsExists(ctx, g.Map{dao.MaterialTemplateWords.Columns().TemplateId: tid})
	return r
}

// ProcessMaterialTemplateWordsMapsByTid 根据模板ID获取素材模板文本MAP
func (m *sMaterial) ProcessMaterialTemplateWordsMapsByTid(ctx context.Context, tid ...uint) (words map[uint]model.MatchTemplateWordsItem, err error) {
	if len(tid) == 0 {
		return nil, gerror.New(`缺少模板ID`)
	}
	var (
		list []model.MaterialTemplateWordsBasicItem
	)
	err = m.ProcessMaterialTemplateWordsScan(ctx, g.Map{
		dao.MaterialTemplateWords.Columns().TemplateId: tid,
	}, &list)
	if err != nil {
		return nil, err
	}
	words = map[uint]model.MatchTemplateWordsItem{}
	// 迭代处理
	for _, item := range list {
		// 断言是否初始化
		wordItem, ok := words[item.TemplateId]
		if !ok {
			wordItem = model.MatchTemplateWordsItem{}
		}
		// 初始化匹配文本词和文本数据
		wordItem.MatchWords = append(wordItem.MatchWords, models.DesignTemplateWordsItem{
			WordsId: item.Id,
			Num:     item.WordsNum,
		})
		wordItem.Words = append(wordItem.Words, item)
		words[item.TemplateId] = wordItem
	}
	return
}
