package material

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"gitee.com/bobo-rs/idea-space-framework/maps"
	"github.com/gogf/gf/v2/frame/g"
	"strings"
)

// FormatMaterialDetail 格式化素材详情
func (m *sMaterial) FormatMaterialDetail(detail *model.MaterialDetailItem) {
	// 素材类型
	if t, ok := maps.MaterialTypeMap[detail.MaterialType]; ok {
		detail.FmtMaterialType = t
	}
	// 渠道
	if c, ok := maps.ChannelId[detail.ChannelId]; ok {
		detail.FmtChannel = c
	}
	// 素材位置
	if p, ok := maps.MaterialPositionMap[detail.Position]; ok {
		detail.FmtPosition = p
	}
	// 素材状态
	if s, ok := maps.MaterialStatusMap[detail.Status]; ok {
		detail.FmtStatus = s
	}
}

// FormatMaterialTemplateDetail 格式化素材模板详情
func (m *sMaterial) FormatMaterialTemplateDetail(detail *model.MaterialTemplateItem) {
	if idx, ok := maps.MaterialTemplateIdxStatus[detail.IdxStatus]; ok {
		detail.FmtIdx = idx
	}
	if s, ok := maps.MaterialTemplateStatus[detail.Status]; ok {
		detail.FmtStatus = s
	}
	if v, ok := maps.VipType[detail.VipType]; ok {
		detail.FmtVip = v
	}
	if c, ok := maps.ChannelId[detail.ChannelId]; ok {
		detail.FmtChannel = c
	}
}

// FormatMaterialTemplateWhere 格式化素材模板基础查询条件
func (m *sMaterial) FormatMaterialTemplateWhere(ctx context.Context, in model.MaterialTemplateListWhereItem) (where map[string]interface{}) {
	var err error
	defer func() {
		if err != nil {
			g.Log().Debug(ctx, `素材模板搜索查询条件`, err)
		}
	}()
	// 初始化
	where = make(map[string]interface{})
	var (
		columns = dao.MaterialTemplate.Columns()
	)
	if _, ok := maps.MaterialTemplateIdxStatus[in.IdxStatus]; ok {
		where[columns.IdxStatus] = in.IdxStatus
	}
	if _, ok := maps.MaterialTemplateStatus[in.Status]; ok {
		where[columns.Status] = in.Status
	}
	if _, ok := maps.VipType[in.VipType]; ok {
		where[columns.VipType] = in.VipType
	}
	if _, ok := maps.ChannelId[in.ChannelId]; ok {
		where[columns.ChannelId] = in.ChannelId
	}
	if len(in.TemplateName) > 0 {
		where[columns.TemplateName] = strings.TrimSpace(in.TemplateName)
	}
	if len(in.LabelValues) > 0 {
		where[columns.Id], err = service.Label().ProcessIdxTypeAndHashByCid(
			ctx, enums.CorrelateTypeTemp, in.LabelValues,
		)
	}
	return where
}
