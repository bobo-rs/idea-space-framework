package material

import "gitee.com/bobo-rs/idea-space-framework/framework/service"

type sMaterial struct {
}

func init() {
	service.RegisterMaterial(New())
}

func New() *sMaterial {
	return &sMaterial{}
}
