package syslog

import (
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/logic/base"
)

// LogModel 系统日志Model
func (l *sSysLog) LogModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.SystemLog.Table(),
	}
}
