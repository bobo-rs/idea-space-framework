package syslog

import (
	"gitee.com/bobo-rs/idea-space-framework/consts"
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
)

// SystemLogWhere 系统日志条件
func (l *sSysLog) SystemLogWhere(in model.SystemLogWhereItem) map[string]interface{} {
	var (
		where   = make(map[string]interface{})
		columns = dao.SystemLog.Columns()
	)

	// 日志内容
	if len(in.Content) > 0 {
		where[columns.Content+consts.OrmWhereLike] = `%` + in.Content + `%`
	}

	// 日志模块
	if in.ModuleName != "" {
		where[columns.ModuleName] = in.ModuleName
	}

	// 日志类型
	if in.OperateType.Fmt() != "" {
		where[columns.OperateType] = in.OperateType
	}

	// 管理员名
	if in.ManageName != "" {
		where[columns.ManageName] = in.ManageName
	}

	// 创建时间
	if len(in.CreateAt) >= 2 {
		where[columns.CreateAt+consts.OrmWhereBetween] = in.CreateAt
	}
	return where
}

// FormatSystemLog 格式化系统日志
func (l *sSysLog) FormatSystemLog(detail *model.SystemLogDetailItem) {
	detail.FmtOperateType = enums.LogOperateType(detail.OperateType).Fmt()
}
