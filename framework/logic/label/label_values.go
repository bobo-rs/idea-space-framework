package label

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
	"strings"
)

// GetLabelValuesList 获取标签值列表
func (l *sLabel) GetLabelValuesList(ctx context.Context, in model.LabelValuesGetListInput) (out *model.LabelValuesGetListOutput, err error) {
	out = &model.LabelValuesGetListOutput{}
	out.LabelId = in.LabelId
	// 获取数据列表
	err = l.ProcessGetLabelValueList(ctx, in, &out.Rows)
	if err != nil {
		return nil, gerror.Wrapf(err, `获取标签值列表为空：`, err.Error())
	}
	// 数据为空
	if len(out.Rows) == 0 {
		return out, nil
	}
	// 处理数据
	values := make([]string, 0)
	for _, row := range out.Rows {
		values = append(values, row.Value)
	}
	out.Values = strings.Join(values, ",")
	return
}

// DeleteLabelValues 删除标签值(或恢复数据)
func (l *sLabel) DeleteLabelValues(ctx context.Context, in model.LabelValueDelInput) error {
	var (
		m           = dao.LabelValues.Ctx(ctx).Where(dao.LabelValues.Columns().Hash, in.Hash)
		isValueBind bool // 是否已绑定模板和设计图：false未绑定，true已绑定
		isDel       uint // 0正常，1软删除
	)

	// 检测标签值是否存在
	detail := model.LabelValueDetailItem{}
	err := l.ProcessLabelValueDetailByHash(ctx, in.Hash, &detail)
	if err != nil {
		return err
	}
	// 验证模板是否绑定模板和设计图
	switch isValueBind {
	case false:
		// 删除数据
		_, err = m.Delete()
	default:
		// 更新删除标签
		if detail.IsDel == 0 {
			isDel = 1
		}
		_, err = m.Update(g.Map{dao.LabelValues.Columns().IsDel: isDel})
	}
	if err != nil {
		return gerror.Wrapf(err, `删除失败`, err.Error())
	}
	return nil
}
