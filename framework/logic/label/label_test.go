package label

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	_ "github.com/gogf/gf/v2/errors/gerror"
	"testing"
)

var (
	Ctx = context.Background()
)

func TestSLabel_SaveLabelValues(t *testing.T) {
	rep := model.LabelValuesSaveInput{
		LabelId: 1,
		Values:  `淘宝天猫,京东,拼多多,美团`,
	}
	fmt.Println(New().SaveLabelValues(Ctx, rep))
}
