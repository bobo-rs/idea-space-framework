package label

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// GetLabelListAndTotal 获取标签列表和数量
func (l *sLabel) GetLabelListAndTotal(ctx context.Context, in model.LabelQueryListAndTotalInput, rows interface{}, total *int) error {
	if in.Where == nil {
		return gerror.New(`缺少查询条件`)
	}
	// 查询数据
	err := dao.Label.Ctx(ctx).
		Where(in.Where).
		Page(in.Page, in.Size).
		ScanAndCount(rows, total, true)
	if err != nil {
		return err
	}
	return nil
}

// GetLabelDetail 获取标签详情信息
func (l *sLabel) GetLabelDetail(ctx context.Context, where interface{}, detail interface{}) error {
	if where == nil {
		return gerror.New(`缺少查询条件`)
	}
	err := dao.Label.Ctx(ctx).
		Where(where).
		Scan(detail)
	if err != nil {
		return err
	}
	return nil
}

// ProcessLabelList 处理并获取标签列表
func (l *sLabel) ProcessLabelList(ctx context.Context, where, list interface{}) error {
	if where == nil || g.IsNil(where) {
		return gerror.New(`缺少查询条件`)
	}
	return dao.Label.Ctx(ctx).Where(where).Scan(list)
}

// GetLabelDetailById 通过标签ID查询获取标签详情
func (l *sLabel) GetLabelDetailById(ctx context.Context, labelId uint, detail interface{}) error {
	if labelId == 0 {
		return gerror.New(`标签ID不能为0`)
	}
	return l.GetLabelDetail(
		ctx,
		model.LabelQueryDetailByIdItem{Id: labelId},
		detail,
	)
}

// CheckLabelExists 检测标签是否存在
func (l *sLabel) CheckLabelExists(ctx context.Context, where interface{}) (bool, error) {
	if where == nil {
		return false, gerror.New(`缺少查询条件`)
	}
	total, err := dao.Label.Ctx(ctx).
		Where(where).
		Count()
	if err != nil {
		return false, err
	}
	return total > 0, nil
}

// ProcessLabelScan 扫描标签数据-单条|列表
func (l *sLabel) ProcessLabelScan(ctx context.Context, where, scan interface{}) error {
	if where == nil || g.IsNil(where) {
		return gerror.New(`缺少请求参数`)
	}
	return dao.Label.Ctx(ctx).Where(where).Scan(scan)
}

// GetLabelDetail 获取标签详情信息
func (l *sLabel) ProcessLabelDetail(ctx context.Context, where interface{}, detail interface{}) error {
	return l.ProcessLabelScan(ctx, where, detail)
}

// ProcessLabelDetailById 通过标签ID查询获取标签详情
func (l *sLabel) ProcessLabelDetailById(ctx context.Context, labelId uint, detail interface{}) error {
	if labelId == 0 {
		return gerror.New(`标签ID不能为0`)
	}
	return l.ProcessLabelDetail(
		ctx,
		g.Map{dao.Label.Columns().Id: labelId},
		detail,
	)
}
