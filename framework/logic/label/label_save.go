package label

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/dao"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"github.com/gogf/gf/v2/errors/gerror"
)

// LabelSave 保存标签库标签
func (l *sLabel) LabelSave(ctx context.Context, in model.LabelSaveInput) (out *model.LabelSaveOutput, err error) {
	out = &model.LabelSaveOutput{}
	// 保存数据
	r, err := dao.Label.Ctx(ctx).Save(in)
	if err != nil {
		return nil, err
	}
	out.Id, err = r.LastInsertId()
	if err != nil {
		return nil, gerror.Wrap(err, `获取最后写入ID错误`)
	}
	return
}
