package validate

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/gvalid"
)

// RuleRegisterMaterial 注册素材规则
func RuleRegisterMaterial() {
	gvalid.RegisterRuleByMap(map[string]gvalid.RuleFunc{
		`attach-id-exists`:            RuleAttachIdExists,
		`material-exists-id`:          RuleMaterialExistsById,
		`material-template-exists-id`: RuleMaterialTemplateExistsById,
	})
}

// RuleMaterialExistsById 通过素材ID验证素材是否存在
func RuleMaterialExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	var (
		id = in.Value.Uint()
	)
	if id == 0 {
		return gerror.New(`素材ID不能为0`)
	}
	byId, err := service.Material().CheckMaterialExistsById(ctx, id)
	if err != nil {
		return err
	}
	if byId == false {
		return gerror.Newf(`素材不存在，素材ID：%d`, id)
	}
	return nil
}
