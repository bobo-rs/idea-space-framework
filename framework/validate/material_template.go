package validate

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/gvalid"
)

// RuleMaterialTemplateExistsById 根据素材模板ID验证素材模板是否存在
func RuleMaterialTemplateExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	var (
		id = in.Value.Uint()
	)
	if id == 0 {
		return gerror.New(`缺少素材模板ID`)
	}
	byId := service.Material().CheckMaterialTemplateExistsById(ctx, id)
	if !byId {
		return gerror.New(`素材模板不存在`)
	}
	return nil
}
