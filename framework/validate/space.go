package validate

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/gvalid"
)

// RuleRegisterSpace 注册设计资源图片规则
func RuleRegisterSpace() {
	gvalid.RegisterRuleByMap(map[string]gvalid.RuleFunc{
		`space-image-exists-id`: RuleSpaceImageExistsById,
	})
}

// RuleSpaceImageExistsById 验证设计资源图片是否存在
func RuleSpaceImageExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	id := in.Value.Uint()
	if id == 0 {
		return gerror.New(`设计资源图片ID`)
	}
	if service.Space().CheckSpaceImageExistsById(ctx, id) == false {
		return gerror.New(`设计资源不存在`)
	}
	return nil
}
