package validate

import (
	"context"
	"gitee.com/bobo-rs/idea-space-framework/framework/model"
	"gitee.com/bobo-rs/idea-space-framework/framework/service"
	"gitee.com/bobo-rs/idea-space-framework/pkg/utils"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gvalid"
	"regexp"
	"strings"
)

// RuleLabelExists 验证标签是否存在
func RuleLabelExists(ctx context.Context, in gvalid.RuleFuncInput) error {
	var (
		req = model.LabelValuesSaveInput{}
	)
	if err := in.Data.Scan(&req); err != nil {
		return gerror.Wrap(err, `验证标签类是否存在扫描LabelValuesSaveInput错误`)
	}
	res, err := service.Label().CheckLabelExists(ctx, g.Map{`id`: req.LabelId})
	if err != nil {
		return err
	}
	// 是否存在
	if res == false {
		return gerror.New(`标签类不存在`)
	}
	return nil
}

// RuleLabelValueMatch 匹配标签值格
func RuleLabelValueMatch(ctx context.Context, in gvalid.RuleFuncInput) error {
	var (
		req = model.LabelValuesSaveInput{}
	)
	if err := in.Data.Scan(&req); err != nil {
		return gerror.Wrap(err, `验证标签值格式扫描LabelValuesSaveInput错误`)
	}
	// 格式匹配
	re := regexp.MustCompile(`^[\x{4e00}-\x{9fa5}0-9a-zA-Z,]+$`)
	if !re.MatchString(req.Values) {
		return gerror.New(`标签值格式错误，只能汉字、英文和数字以及英文逗号分隔符`)
	}
	values := utils.SeparatorStringToArray(req.Values)
	if len(values) == 0 {
		return gerror.New(`标签值过滤后，数字为空`)
	}
	return nil
}

// RuleLabelValueHashExists 验证标签Hash值是否存在
func RuleLabelValueHashExists(ctx context.Context, in gvalid.RuleFuncInput) error {
	var (
		req = model.LabelValueDelInput{}
	)
	if err := in.Data.Scan(&req); err != nil {
		return gerror.Wrap(err, `验证标签值是否存在扫描LabelValueDelInput错误`)
	}
	// 拆分标签值
	res, err := service.Label().CheckLabelValueExists(ctx, g.Map{
		`hash`: req.Hash,
	})
	if err != nil {
		return err
	}
	// 是否存在
	if res == false {
		return gerror.New(`标签值不存在`)
	}
	return nil
}

// RuleLabelValueHashMatch 匹配标签值Hash格式
func RuleLabelValueHashMatch(ctx context.Context, in gvalid.RuleFuncInput) error {
	var (
		req = model.LabelValueDelInput{}
	)
	if err := in.Data.Scan(&req); err != nil {
		return gerror.Wrap(err, `匹配标签值是格式扫描LabelValueDelInput错误`)
	}
	// 长度
	if len(req.Hash) != 32 {
		return gerror.New(`标签hash值长度错误`)
	}
	// 匹配格式
	if utils.AlphaNum(req.Hash) == false {
		return gerror.New(`标签Hash值格式错误`)
	}
	return nil
}

// RuleLabelValueExists 验证标签值是否存在
func RuleLabelValueExists(ctx context.Context, in gvalid.RuleFuncInput) error {
	var (
		values = strings.Split(in.Value.String(), `-`)
	)
	if len(values) == 0 {
		return gerror.New(`标签值不能为空`)
	}
	// 检测标签值是否存在
	total := service.Label().ProcessLabelValueCount(ctx, g.Map{
		`value`: values,
	})
	if total != len(values) {
		return gerror.New(`标签值不存在`)
	}
	return nil
}
