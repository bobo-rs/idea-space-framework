package model

type (
	// SpaceImageAttrItem 设计空间图片属性
	SpaceImageAttrItem struct {
		SpaceId    uint   `json:"space_id"     dc:"设计空间图片ID"`      // 设计空间图片ID
		AttachId   string `json:"attach_id"    dc:"附件ID"`          // 附件ID
		Size       uint   `json:"size"         dc:"内存大小"`          // 内存大小
		Width      uint   `json:"width"        dc:"宽度"`            // 宽度
		Height     uint   `json:"height"       dc:"高度"`            // 高度
		Precision  uint   `json:"precision"    dc:"精度：0-9，越高精度越高"` // 精度：0-9，越高精度越高
		RbgChannel string `json:"rbg_channel"  dc:"RBG通道"`         // RBG通道
	}
)
