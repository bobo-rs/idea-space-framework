package model

import "gitee.com/bobo-rs/idea-space-framework/framework/model/entity"

type (
	// LabelSaveInput 添加|编辑标签请求参数
	LabelSaveInput struct {
		Id         uint   `json:"id"`
		Name       string `json:"name"`
		IsRequired uint   `json:"is_required"`
	}

	// LabelSaveOutput 添加|编辑标签响应输出
	LabelSaveOutput struct {
		Id int64 `json:"id" dc:"标签ID"`
	}
)

type (
	// LabelGetListInput 获取标签列表请求参数
	LabelGetListInput struct {
		Name       string `json:"name"`
		IsRequired uint   `json:"is_required"`
		CommonPaginationItem
	}

	// LabelGetListOutput 获取标签列表响应结果输出
	LabelGetListOutput struct {
		CommonResponseItem
		Rows []LabelGetListItem `json:"rows" dc:"数据列表"`
	}

	// LabelGetDetailInput 获取标签详情请求参数
	LabelGetDetailInput struct {
		Id uint `json:"id"`
	}

	// LabelGetDetailOutput 获取标签详情响应结果输出
	LabelGetDetailOutput struct {
		LabelGetDetailItem
	}

	// LabelGetDetailItem 获取标签详情属性
	LabelGetDetailItem struct {
		entity.Label
	}

	// LabelGetListItem 获取标签列表属性
	LabelGetListItem struct {
		Id          uint   `json:"id" dc:"标签ID"`
		Name        string `json:"name" dc:"标签名"`
		IsRequired  uint   `json:"is_required" dc:"是否必须勾选"`
		FmtRequired string `json:"fmt_required" dc:"格式化是否必须勾选"`
		Values      string `json:"values" dc:"标签值"`
		Creator     string `json:"creator" dc:"创建人"`
		CreateAt    string `json:"create_at" dc:"创建时间"`
	}
)

type (
	// LabelQueryListAndTotalInput 标签查询列表和数量总数请求参数
	LabelQueryListAndTotalInput struct {
		CommonPaginationItem
		Where interface{} `json:"where"`
	}

	// LabelQueryDetailByIdItem 以标签ID查询获取标签详情
	LabelQueryDetailByIdItem struct {
		Id uint `json:"id"`
	}

	// LabelShowListItem 标签展示列表
	LabelShowListItem struct {
		Id         uint     `json:"id" dc:"标签ID"`
		Name       string   `json:"name" dc:"标签名"`
		IsRequired uint     `json:"is_required" dc:"是否必须勾选"`
		Values     []string `json:"values" dc:"标签值"`
	}
)
