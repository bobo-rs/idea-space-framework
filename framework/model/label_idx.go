package model

type (
	// LabelIdxSaveItem 保存标签索引请求属性
	LabelIdxSaveItem struct {
		LabelIdxCorrelateItem
		LabelIdxValueHashItem
	}

	// LabelIdxCorrelateItem 标签索引-关联属性
	LabelIdxCorrelateItem struct {
		CorrelateId   uint `json:"correlate_id" dc:"关联ID"`
		CorrelateType uint `json:"correlate_type" dc:"关联类型：0素材模板，1设计图"`
	}

	// LabelIdxValueHashItem 标签索引-标签值和Hash值
	LabelIdxValueHashItem struct {
		LabelValue string `json:"label_value" dc:"标签值"`
		Hash       string `json:"hash" dc:"标签Hash值"`
	}

	// LabelIdxHashAndIdItem 获取标签索引值和索引ID
	LabelIdxHashAndIdItem struct {
		Id        uint   `json:"Id"`
		LabelHash string `json:"label_hash"`
	}

	// LabelIdxCidAndHashItem 获取标签索引值和关联ID
	LabelIdxCidAndHashItem struct {
		CorrelateId uint   `json:"correlate_id"`
		LabelHash   string `json:"label_hash"`
	}

	// IdxValueHashItem 标签索引Hash值属性
	IdxValueHashItem struct {
		Value string `json:"value"`
		Hash  string `json:"hash"`
	}
)
