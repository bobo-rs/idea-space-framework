package model

type (
	// DepartmentSaveInput 保存部门数据请求参数
	DepartmentSaveInput struct {
		DepartmentItem
		RoleId []uint `json:"role_id" dc:"角色ID"`
	}
)

type (
	// DepartmentTreeItem 树形部门属性
	DepartmentTreeItem struct {
		DepartmentItem
		PeopleNum uint `json:"people_num" dc:"部门人数"`
	}

	// DepartmentDetailItem 部门详情属性
	DepartmentDetailItem struct {
		DepartmentItem
		RoleId   []uint              `json:"role_id" dc:"角色ID"`
		RoleList []RoleIdAndNameItem `json:"role_list" dc:"角色列表"`
	}

	// DepartmentItem 部门属性
	DepartmentItem struct {
		Id         uint   `json:"id" dc:"部门ID"`
		DepartName string `json:"depart_name" dc:"部门名称"`
		AbbrevName string `json:"abbrev_name" dc:"部门简写"`
		DepartNo   string `json:"depart_no" dc:"部门编号"`
		Pid        uint   `json:"pid" dc:"父级ID"`
		Status     uint   `json:"status" dc:"部门状态"`
		Sort       uint   `json:"sort" dc:"排序"`
	}

	// DepartmentNameItem 通过部门ID获取部门名称属性
	DepartmentNameItem struct {
		Id         uint   `json:"id" dc:"部门ID"`
		DepartName string `json:"depart_name" dc:"部门名"`
	}
)
