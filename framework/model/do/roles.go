// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Roles is the golang structure of table is_roles for DAO operations like Where/Data.
type Roles struct {
	g.Meta      `orm:"table:is_roles, do:true"`
	Id          interface{} // 角色ID
	Name        interface{} // 角色名
	Description interface{} // 详情
	RoleStatus  interface{} // 角色状态：0正常，1冻结
	Sort        interface{} // 排序：0-255，默认255
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
