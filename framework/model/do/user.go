// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// User is the golang structure of table is_user for DAO operations like Where/Data.
type User struct {
	g.Meta   `orm:"table:is_user, do:true"`
	Id       interface{} // 用户ID
	Account  interface{} // 账号
	Mobile   interface{} // 手机号（加密）
	Nickname interface{} // 昵称
	UniqueId interface{} // 对外身份ID
	Status   interface{} // 状态：0正常，1冻结，2待注销
	UpdateAt *gtime.Time // 更新时间
	CreateAt *gtime.Time // 注册时间
}
