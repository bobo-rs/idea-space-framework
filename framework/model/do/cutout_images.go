// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// CutoutImages is the golang structure of table is_cutout_images for DAO operations like Where/Data.
type CutoutImages struct {
	g.Meta       `orm:"table:is_cutout_images, do:true"`
	Id           interface{} // 抠图ID
	CutoutCateId interface{} // 抠图分类ID
	CutoutName   interface{} // 抠图名
	ImageUrl     interface{} // 图片地址
	DownloadNum  interface{} // 下载量
	BrowseNum    interface{} // 浏览数量
	ChannelId    interface{} // 渠道ID
	CreatorId    interface{} // 创建人ID
	Creator      interface{} // 创建人
	Modified     interface{} // 修改人
	UpdateAt     *gtime.Time // 更新时间
	CreateAt     *gtime.Time // 创建时间
}
