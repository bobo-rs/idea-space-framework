// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Config is the golang structure of table is_config for DAO operations like Where/Data.
type Config struct {
	g.Meta       `orm:"table:is_config, do:true"`
	Id           interface{} // 配置ID
	Name         interface{} // 配置名称
	Description  interface{} // 配置详情简介
	Value        interface{} // 配置值
	DefaultValue interface{} // 默认配置值
	GroupId      interface{} // 配置组ID
	GroupName    interface{} // 配置组名
	Remark       interface{} // 备注
	UpdateAt     *gtime.Time // 更新时间
	CreateAt     *gtime.Time // 创建时间
}
