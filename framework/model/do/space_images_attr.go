// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SpaceImagesAttr is the golang structure of table is_space_images_attr for DAO operations like Where/Data.
type SpaceImagesAttr struct {
	g.Meta      `orm:"table:is_space_images_attr, do:true"`
	SpaceId     interface{} // 设计空间图片ID
	StorageType interface{} // 存储类型：local本地，qiniu七牛等
	AttachId    interface{} // 附件ID
	Size        interface{} // 内存大小
	Width       interface{} // 宽度
	Height      interface{} // 高度
	Precision   interface{} // 精度：0-9，越高精度越高
	RbgChannel  interface{} // RBG通道
	UpdateAt    *gtime.Time // 更新时间
}
