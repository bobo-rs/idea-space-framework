// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SmsTemplateOut is the golang structure of table is_sms_template_out for DAO operations like Where/Data.
type SmsTemplateOut struct {
	g.Meta        `orm:"table:is_sms_template_out, do:true"`
	Id            interface{} // 短信模板关联外部平台ID
	VarAlias      interface{} // 系统模板变量
	TemplateId    interface{} // 外部平台ID
	FromType      interface{} // 平台类型：tencent腾讯，alipay阿里云[is_config,短信通道]
	OutTmplStatus interface{} // 外部模板状态
	UpdateAt      *gtime.Time // 更新时间
	CreateAt      *gtime.Time // 创建时间
}
