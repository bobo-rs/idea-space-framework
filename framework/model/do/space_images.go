// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SpaceImages is the golang structure of table is_space_images for DAO operations like Where/Data.
type SpaceImages struct {
	g.Meta      `orm:"table:is_space_images, do:true"`
	Id          interface{} // 空间图片ID
	SpaceName   interface{} // 空间图片名
	ImageUrl    interface{} // 图片URL
	VipType     interface{} // 授权类型：0免费，1VIP
	IsOpen      interface{} // 是否开放浏览：0开放，1关闭
	Price       interface{} // 价格（VIP）
	DesignBean  interface{} // 设计豆（虚拟货币）
	DownloadNum interface{} // 下载数量
	Browse      interface{} // 浏览量
	BuyNum      interface{} // 购买量
	DesignType  interface{} // 设计类型：0智能设计，1手动设计，2主动设计
	ChannelId   interface{} // 渠道来源ID
	CreatorId   interface{} // 创建人ID
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
