// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Department is the golang structure of table is_department for DAO operations like Where/Data.
type Department struct {
	g.Meta     `orm:"table:is_department, do:true"`
	Id         interface{} // 部门ID
	DepartName interface{} // 全称，例如：财务部
	AbbrevName interface{} // 简称，例如：财务
	DepartNo   interface{} // 部门编号
	Pid        interface{} // 父级ID
	Status     interface{} // 部门状态：0正常，1禁用
	Sort       interface{} // 排序：0-255，默认255
	UpdateAt   *gtime.Time // 更新时间
	CreateAt   *gtime.Time // 创建时间
}
