// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Material is the golang structure of table is_material for DAO operations like Where/Data.
type Material struct {
	g.Meta       `orm:"table:is_material, do:true"`
	Id           interface{} // 素材ID
	MaterialName interface{} // 素材名称
	MaterialType interface{} // 素材类型：0素材配图，1素材主图（未修饰过的图）
	ChannelId    interface{} // 素材渠道：0平台，1企业，2用户
	Position     interface{} // 素材位置：main主图，market营销活动卖点，top顶部卖点，btm-main底部主卖点，btm-second底部次卖点，btm-single底部单独卖点，detail详情图卖点
	Width        interface{} // 素材宽度
	Height       interface{} // 素材高度
	Url          interface{} // 素材地址
	AttachId     interface{} // 附件ID
	CreatorId    interface{} // 创建人ID
	Creator      interface{} // 创建人
	Modified     interface{} // 修改人
	Status       interface{} // 素材状态：0正常，1废弃
	UpdateAt     *gtime.Time // 更新时间
	CreateAt     *gtime.Time // 创建时间
}
