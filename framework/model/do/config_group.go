// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// ConfigGroup is the golang structure of table is_config_group for DAO operations like Where/Data.
type ConfigGroup struct {
	g.Meta      `orm:"table:is_config_group, do:true"`
	Id          interface{} // 配置组ID
	Name        interface{} // 配置组名
	GroupName   interface{} // 配置组关联名：basic基础配置
	Description interface{} // 详情
	Pid         interface{} // 父级ID
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
