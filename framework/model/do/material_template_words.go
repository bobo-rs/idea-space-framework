// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// MaterialTemplateWords is the golang structure of table is_material_template_words for DAO operations like Where/Data.
type MaterialTemplateWords struct {
	g.Meta         `orm:"table:is_material_template_words, do:true"`
	Id             interface{} // 素材模板文案ID
	MaterialId     interface{} // 素材ID
	TemplateId     interface{} // 素材模板ID
	ExampleWords   interface{} // 示例文案
	FontFamily     interface{} // 字体系列，例如：微软雅黑
	WordsType      interface{} // 文案类型：0空白，1文字，2.价格
	WordsFont      interface{} // 文案字体字号
	WordsFontColor interface{} // 文按字体颜色
	WordsX         interface{} // 切词文案卖点X坐标
	WordsY         interface{} // 切词文案卖点Y坐标
	LayerPosition  interface{} // 图层位置，切词优先词
	WordsNum       interface{} // 文本数量
	MaterialX      interface{} // 素材X坐标
	MaterialY      interface{} // 素材Y坐标
	MaterialColor  interface{} // 素材色系
	Url            interface{} // 素材URL
	UpdateAt       *gtime.Time // 更新时间
	CreateAt       *gtime.Time // 创建时间
}
