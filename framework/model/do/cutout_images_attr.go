// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// CutoutImagesAttr is the golang structure of table is_cutout_images_attr for DAO operations like Where/Data.
type CutoutImagesAttr struct {
	g.Meta      `orm:"table:is_cutout_images_attr, do:true"`
	CutImageId  interface{} // 抠图图片ID
	AttachId    interface{} // 附件ID
	ImageUrl    interface{} // 图片地址
	ImagePath   interface{} // 图片路径
	Filename    interface{} // 文件名
	Suffix      interface{} // 图片后缀
	StorageType interface{} // 存储类型：local本地存储，qiniu七牛
	Size        interface{} // 图片大小内存
	Width       interface{} // 图片宽度
	Height      interface{} // 图片高度
	UpdateAt    *gtime.Time // 更新时间
}
