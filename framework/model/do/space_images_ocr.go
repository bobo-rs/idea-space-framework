// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SpaceImagesOcr is the golang structure of table is_space_images_ocr for DAO operations like Where/Data.
type SpaceImagesOcr struct {
	g.Meta     `orm:"table:is_space_images_ocr, do:true"`
	Id         interface{} // 文案OCRID
	ImageId    interface{} // 图片ID
	WordsValue interface{} // 文案值
	WordsFont  interface{} // 文案字号大小
	WordsColor interface{} // 文案颜色
	WordsX     interface{} // 文案坐标X
	WordsY     interface{} // 文案坐标Y
	WordsLen   interface{} // 文案长度
	Width      interface{} // 宽度
	Height     interface{} // 高度
	IsOrigin   interface{} // 是否原文：0原文，1翻译
	Lang       interface{} // 语言：zh-CN简体中文
	WordsHash  interface{} // Hash值（文案内容+是否主体+语言类型）
	UpdateAt   *gtime.Time // 更新时间
	CreateAt   *gtime.Time // 创建时间
}
