// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// LabelValues is the golang structure of table is_label_values for DAO operations like Where/Data.
type LabelValues struct {
	g.Meta   `orm:"table:is_label_values, do:true"`
	Id       interface{} // 标签值ID
	LabelId  interface{} // 标签ID
	Value    interface{} // 标签值
	IsDel    interface{} // 是否删除：0正常，1删除
	Hash     interface{} // 标签值Hash
	UpdateAt *gtime.Time // 更新时间
	CreateAt *gtime.Time // 创建时间
}
