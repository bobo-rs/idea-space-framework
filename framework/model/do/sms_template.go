// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SmsTemplate is the golang structure of table is_sms_template for DAO operations like Where/Data.
type SmsTemplate struct {
	g.Meta   `orm:"table:is_sms_template, do:true"`
	Id       interface{} // 模板ID
	Name     interface{} // 模板名
	Content  interface{} // 模板内容
	VarAlias interface{} // 模板别名
	SmsType  interface{} // 短信类型：0验证码，1消息通知，2营销短信
	Status   interface{} // 模板状态：0关闭，1启用
	Remark   interface{} // 备注
	UpdateAt *gtime.Time // 更新时间
	CreateAt *gtime.Time // 创建时间
}
