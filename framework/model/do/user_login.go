// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// UserLogin is the golang structure of table is_user_login for DAO operations like Where/Data.
type UserLogin struct {
	g.Meta          `orm:"table:is_user_login, do:true"`
	Id              interface{} // 用户登录记录ID
	UserId          interface{} // 用户ID
	LoginIp         interface{} // 登录IP
	LoginIpLocation interface{} // 登录归属地
	Token           interface{} // 登录token
	ExpireTime      interface{} // 过期时间
	Status          interface{} // 状态：0在线，1下线，2超时下线
	IsUse           interface{} // 是否一直在使用；0是，1否
	DeviceType      interface{} // 设备类型
	DeviceContent   interface{} // 设备详情
	IsNewDevice     interface{} // 是否新设备：0是，1新设备
	Mac             interface{} // Mac地址
	UpdateAt        *gtime.Time // 更新时间
	CreateAt        *gtime.Time // 登录时间
}
