// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// UserDetail is the golang structure of table is_user_detail for DAO operations like Where/Data.
type UserDetail struct {
	g.Meta          `orm:"table:is_user_detail, do:true"`
	UserId          interface{} // 用户ID
	Sex             interface{} // 性别：0男，1女
	Birthday        *gtime.Time // 生日
	PwdHash         interface{} // 密码hash值
	PwdSalt         interface{} // 加密盐
	Avatar          interface{} // 头像
	AttachId        interface{} // 附件ID
	RegIp           interface{} // 注册IP
	RegIpLocation   interface{} // 注册归属地
	LoginIp         interface{} // 登录IP
	LoginIpLocation interface{} // 登录归属地
	LogoutAt        *gtime.Time // 注销时间
	SignNum         interface{} // 签到总数
	ConSign         interface{} // 连续签到时间
	UpdateAt        *gtime.Time // 更新时间
}
