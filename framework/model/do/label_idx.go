// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// LabelIdx is the golang structure of table is_label_idx for DAO operations like Where/Data.
type LabelIdx struct {
	g.Meta        `orm:"table:is_label_idx, do:true"`
	Id            interface{} // 标签索引ID
	CorrelateId   interface{} // 关联ID
	CorrelateType interface{} // 关联类型：0素材模板，1设计图
	LabelValue    interface{} // 标签值(例如：淘宝/天猫-美妆-双十一，淘宝/天猫-美妆-双十二等)
	LabelHash     interface{} // 标签Hash值(例如：淘宝/天猫-美妆-双十一，淘宝/天猫-美妆-双十二等，md5加密)
	UpdateAt      *gtime.Time // 更新时间
	CreateAt      *gtime.Time // 创建时间
}
