// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SpaceImagesDetail is the golang structure of table is_space_images_detail for DAO operations like Where/Data.
type SpaceImagesDetail struct {
	g.Meta       `orm:"table:is_space_images_detail, do:true"`
	SpaceId      interface{} // 设计空间图片ID
	TemplateId   interface{} // 模板ID
	Synopsis     interface{} // 图片简介
	LabelValues  interface{} // 标签值
	FullCutWords interface{} // 完整切词卖点
	Creator      interface{} // 创建人
	Modified     interface{} // 修改人
	UpdateAt     *gtime.Time // 更新时间
}
