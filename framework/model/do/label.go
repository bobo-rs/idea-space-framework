// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Label is the golang structure of table is_label for DAO operations like Where/Data.
type Label struct {
	g.Meta     `orm:"table:is_label, do:true"`
	Id         interface{} // 标签类ID
	Name       interface{} // 标签类名
	IsRequired interface{} // 是否必须勾选：0否，1是
	Creator    interface{} // 最先创建人
	Modified   interface{} // 最后修改人
	UpdateAt   *gtime.Time // 更新时间
	CreateAt   *gtime.Time // 创建时间
}
