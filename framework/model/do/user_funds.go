// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// UserFunds is the golang structure of table is_user_funds for DAO operations like Where/Data.
type UserFunds struct {
	g.Meta                `orm:"table:is_user_funds, do:true"`
	UserId                interface{} // 用户ID
	RechargeTotalFee      interface{} // 充值总金额
	AvailableFee          interface{} // 可用金额
	FreezeFee             interface{} // 冻结金额
	DesignTotalNum        interface{} // 设计币总数
	AvailableDesignNum    interface{} // 可用设计币
	FreezeDesignNum       interface{} // 冻结设计币
	ConsumeTotalDesignNum interface{} // 消费总设计币
	ConsumeTotalFee       interface{} // 消费总金额
	UpdateAt              *gtime.Time // 更新时间
}
