// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// CutoutCategory is the golang structure of table is_cutout_category for DAO operations like Where/Data.
type CutoutCategory struct {
	g.Meta      `orm:"table:is_cutout_category, do:true"`
	Id          interface{} // 抠图分类ID
	Name        interface{} // 抠图分类名
	BriefName   interface{} // 简短名
	Synopsis    interface{} // 简介内容
	AdvImageUrl interface{} // 广告图URL
	Rule        interface{} // 抠图规则
	IsPromotion interface{} // 是否推广：0否，1是
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
