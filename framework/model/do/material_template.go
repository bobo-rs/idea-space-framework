// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// MaterialTemplate is the golang structure of table is_material_template for DAO operations like Where/Data.
type MaterialTemplate struct {
	g.Meta       `orm:"table:is_material_template, do:true"`
	Id           interface{} // 模板ID
	TemplateName interface{} // 模板名
	LabelValues  interface{} // 标签值（渠道-行业-营销）
	CutWordsNum  interface{} // 切词卖点数量
	SellingLen   interface{} // 卖点长度（容错匹配，当切词卖点检索不到模板，直接卖点长度匹配，就近原则）
	IdxStatus    interface{} // 标签索引状态：0创建中、1已完成、2中止失败
	Status       interface{} // 模板状态：0待审核，1正常，2下架
	UseNum       interface{} // 使用量
	MatchNum     interface{} // 匹配数量
	VipType      interface{} // VIP类型：0免费，1VIP
	ChannelId    interface{} // 渠道ID
	ExampleImg   interface{} // 示例模板图片地址
	Creator      interface{} // 创建人
	CreatorId    interface{} // 创建人ID
	Modified     interface{} // 修改人
	UpdateAt     *gtime.Time // 更新时间
	CreateAt     *gtime.Time // 创建时间
}
