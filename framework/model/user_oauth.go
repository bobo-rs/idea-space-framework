package model

import (
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
	"gitee.com/bobo-rs/idea-space-framework/pkg/jwt"
)

type (
	// UserOauthLoginInput 用户认证登录授权请求参数
	UserOauthLoginInput struct {
		UserOauthAccountLoginItem
		UserOauthSmsLoginItem
		UserOauthLoginSignItem
		Oauth enums.UserOauth `json:"oauth" dc:"登录授权类型"`
	}

	// UserOauthLoginOutput 用户认证登录授权响应参数
	UserOauthLoginOutput struct {
		UserOauthLoginItem
	}

	// UserLoginSendSmsCaptchaInput 用户授权登录发送短信验证码
	UserLoginSendSmsCaptchaInput struct {
		UserOauthSendSmsCaptchaItem
		UserOauthLoginSignItem
	}
)

type (
	// UserOauthAccountLoginItem 用户认证账户密码登录属性
	UserOauthAccountLoginItem struct {
		Account string `json:"account" dc:"登录账户"`
		Pwd     string `json:"pwd" dc:"登录密码"`
	}

	// UserOauthSmsLoginItem 用户认证短信验证码登录
	UserOauthSmsLoginItem struct {
		UserOauthSendSmsCaptchaItem
		Code string `json:"code" dc:"短信验证码"`
	}

	// UserOauthSendSmsCaptchaItem 用户认证登录发送短信验证码
	UserOauthSendSmsCaptchaItem struct {
		Mobile string `json:"mobile" dc:"手机号-加密后"`
	}

	// UserOauthLoginSignItem 用户认证登录签名属性
	UserOauthLoginSignItem struct {
		Sign string `json:"sign" dc:"登录签名"`
	}

	// UserOauthLoginItem 用户认证登录授权属性
	UserOauthLoginItem struct {
		*jwt.TokenItem
		Status enums.UserLoginStatus `json:"status" dc:"登录状态：0正常，1待绑定账户，2待绑定手机号，3新设备"`
	}

	// UserOauthLoginDetailItem 用户认证授权登录基础详情属性
	UserOauthLoginDetailItem struct {
		*entity.User
		Status enums.UserLoginStatus `json:"status" dc:"登录状态：0正常，1待设置密码，2待绑定手机号，3新设备"`
	}

	// UserOauthAfterValidateItem 用户认证后置验证登录信息
	UserOauthAfterValidateItem struct {
		UserOauthLoginDetailItem
		UserDeviceLoginAfterItem
	}
)
