package model

type (
	// DepartmentRoleId 部门角色ID
	DepartmentRoleId struct {
		RoleId uint `json:"role_id" dc:"角色ID"`
	}
)
