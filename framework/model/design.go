package model

import (
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/models"
	"github.com/gogf/gf/v2/net/ghttp"
	"image"
)

type (
	// OverlayDesignImageInput 获取设计模板列表
	OverlayDesignImageInput struct {
		Values   string            `json:"values" dc:"标签值（淘宝-美妆-双十一）"`
		Selling  string            `json:"selling" dc:"切词卖点"`
		Price    string            `json:"price" dc:"价格"`
		File     *ghttp.UploadFile `json:"file" dc:"上传文件名"`
		SaveType byte              `json:"save_type" dc:"是否保存：0否，1保存"`
		Page     int               `json:"page" dc:"页码"`
	}

	// DesignDownloadInput 保存下载设计图
	DesignDownloadInput struct {
		TemplateId   uint                            `json:"template_id" dc:"模板ID"`
		Prefix       string                          `json:"prefix" dc:"文件前缀：data:image/png;base64"`
		Content      string                          `json:"content" dc:"文件内容（base64）"`
		LabelValues  string                          `json:"label_values" dc:"标签值"`
		SellingValue string                          `json:"selling_value" dc:"卖点值"`
		Price        string                          `json:"price" dc:"卖点价格"`
		DesignType   enums.SpaceImageDesignTypeEnums `json:"design_type" dc:"设计模式：0智能设计，1手动设计，2主动设计"`
	}

	// OverlayDesignImageOutput 智能设计合成图片响应参数
	OverlayDesignImageOutput struct {
		Total int                               `json:"total" dc:"匹配模板总数"`
		Size  int                               `json:"size" dc:"匹配数量"`
		Rows  []models.MaterialDesignResultItem `json:"rows" dc:"合成图片列表"`
	}

	// DesignDownloadOutput 设计图片保存输出
	DesignDownloadOutput struct {
		DesignDownloadItem
	}
)

type (
	// DesignImageDownloadItem 下载设计图片属性
	DesignImageDownloadItem struct {
		Src      string `json:"src" dc:"文件地址"`
		Filename string `json:"filename" dc:"文件名"`
		Width    int    `json:"width" dc:"宽度"`
		Height   int    `json:"height" dc:"高度"`
		Size     int    `json:"size" dc:"图片大小"`
		AttachId string `json:"attach_id" dc:"附件ID"`
	}

	// DesignDownloadItem 下载设计图片属性
	DesignDownloadItem struct {
		Src     string `json:"src" dc:"文件图片地址"`
		SpaceId uint   `json:"space_id" dc:"设计图片ID"`
	}

	// DesignOverlaysParamItem 设计合成参数数据
	DesignOverlaysParamItem struct {
		Price      string      `json:"price" dc:"价格"`
		UnderImage image.Image `json:"under_image" dc:"底图内容"`
		Filename   string      `json:"filename" dc:"图片名"`
		SaveType   byte        `json:"save_type" dc:"是否保存：0否，1保存"`
	}

	// DesignCutSellingItem 设计模板切词卖点数据
	DesignCutSellingItem struct {
		SellingValues []models.DesignTemplateWordsItem `json:"selling_values" dc:"处理后的切词列表"`
		CutTemplate   models.MatchDesignTemplateItem   `json:"cut_template" dc:"切词模板列表"`
		Total         int                              `json:"total" dc:"匹配模板总数"`
	}
)
