package model

import (
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/models"
)

type (
	// MaterialTemplateSaveInput 素材模板保存请求参数
	MaterialTemplateSaveInput struct {
		MaterialTemplateSaveItem
		Words []MaterialTemplateWordsSaveItem `json:"words" dc:"素材文案词"`
	}

	// 获取素材模板列表请求参数
	MaterialTemplateListInput struct {
		CommonPaginationItem
		MaterialTemplateListWhereItem
	}

	// MaterialTemplateListOutput 素材模板列表响应参数
	MaterialTemplateListOutput struct {
		CommonResponseItem
		Rows []MaterialTemplateItem `json:"rows"`
	}

	// MaterialTemplateDetailOutput 素材模板详情响应参数
	MaterialTemplateDetailOutput struct {
		MaterialTemplateDetailItem
	}

	// MaterialTemplateAuditOrOffInput 审核或关闭素材模板请求参数
	MaterialTemplateAuditOrOffInput struct {
		Id     uint                              `json:"id"`
		Status enums.MaterialTemplateStatusEnums `json:"status"`
	}
)

type (
	// MaterialTemplateDetailItem 素材模板详情信息
	MaterialTemplateDetailItem struct {
		MaterialTemplateItem
		Words []MaterialTemplateWordsDetailItem `json:"words" dc:"文案列表"`
	}

	// MaterialTemplateListWhereItem 获取素材模板列表查询条件
	MaterialTemplateListWhereItem struct {
		TemplateName string `json:"template_name" description:"模板名"`                    // 模板名
		LabelValues  string `json:"label_values"  description:"标签值（渠道-行业-营销）"`          // 标签值（渠道-行业-营销）
		IdxStatus    uint   `json:"idx_status"    description:"标签索引状态：0创建中、1已完成、2中止失败"` // 标签索引状态：0创建中、1已完成、2中止失败
		Status       uint   `json:"status"        description:"模板状态：0待审核，1正常，2下架"`      // 模板状态：0待审核，1正常，2下架
		VipType      uint   `json:"vip_type"      description:"VIP类型：0免费，1VIP"`         // VIP类型：0免费，1VIP
		ChannelId    uint   `json:"channel_id"    description:"渠道ID"`                   // 渠道ID
	}

	// MaterialTemplateItem 素材模板属性
	MaterialTemplateItem struct {
		entity.MaterialTemplate
		FmtVip     string `json:"fmt_vip" dc:"格式化VIP"`
		FmtChannel string `json:"fmt_channel" dc:"格式化渠道"`
		FmtIdx     string `json:"fmt_idx" dc:"格式化索引状态"`
		FmtStatus  string `json:"fmt_status" dc:"格式化状态"`
	}

	// MaterialTemplateSaveItem 素材模板保存属性
	MaterialTemplateSaveItem struct {
		Id           uint     `json:"id"            description:"模板ID"`                                 // 模板ID
		TemplateName string   `json:"template_name" description:"模板名"`                                  // 模板名
		Values       []string `json:"values"  description:"标签值（渠道-行业-营销）"`                              // 标签值（渠道-行业-营销）
		CutWordsNum  uint     `json:"cut_words_num" description:"切词卖点数量"`                               // 切词卖点数量
		SellingLen   uint     `json:"selling_len"   description:"卖点长度（容错匹配，当切词卖点检索不到模板，直接卖点长度匹配，就近原则）"` // 卖点长度（容错匹配，当切词卖点检索不到模板，直接卖点长度匹配，就近原则）
		VipType      uint     `json:"vip_type"      description:"VIP类型：0免费，1VIP"`                       // VIP类型：0免费，1VIP
	}

	// MaterialConfigItem 读取素材配置
	MaterialConfigItem struct {
		RootDir  string // 根目录
		Dir      string // 附件路径
		UnderImg string // 底图
		FontPath string // 字体路径
	}
)

type (

	// DesignTemplateMatchCacheListInput 设计模板缓存匹配模板列表请求参数
	DesignTemplateMatchCacheListInput struct {
		LabelValues string `json:"label_values" dc:"标签值（淘宝-美妆-双十一）"`
	}

	// DesignTemplateGetListOutput 获取设计模板列表响应参数
	DesignTemplateGetListOutput struct {
		Tid  []uint               `json:"tid" dc:"素材模板ID"`
		List []DesignTemplateItem `json:"list" dc:"素材模板列表"`
	}

	// DesignTemplateMatchGetListOutput 获取设计模板匹配列表输出结果
	DesignTemplateMatchGetListOutput struct {
		TidList      []uint                              `json:"tid_list" dc:"模板ID集合"`
		CutWordsList []models.DesignTemplateIdAndNumItem `json:"cut_words_list" dc:"卖点切词数量集合"`
		SellingList  []models.DesignTemplateIdAndNumItem `json:"selling_list" dc:"卖点长度数量集合"`
		List         []DesignTemplateMatchItem           `json:"list" dc:"模板匹配列表"`
	}

	// MatchDesignTemplateItem 获取模板匹配成功列表响应结果
	MatchDesignTemplateItem struct {
		DesignTemplateItem
		Words []MaterialTemplateWordsBasicItem `json:"words" dc:"模板文本列表"`
	}
)

type (

	// DesignTemplateItem 设计素材模板信息
	DesignTemplateItem struct {
		Id           uint   `json:"id" dc:"模板ID"`
		TemplateName string `json:"template_name" dc:"模板名"`
		LabelValues  string `json:"label_values" dc:"标签值"`
		CutWordsNum  uint   `json:"cut_words_num" dc:"切词数量"`
		SellingLen   uint   `json:"selling_len" dc:"卖点长度"`
		UseNum       uint   `json:"use_num" dc:"使用数量"`
		MatchNum     uint   `json:"match_num" dc:"匹配数量"`
		ExampleImg   string `json:"example_img" dc:"示例模板图片"`
	}

	// DesignTemplateMatchItem 匹配查询指定的设计模板属性
	DesignTemplateMatchItem struct {
		Id          uint // 模板ID
		CutWordsNum uint // 卖点切词数量
		SellingLen  uint // 卖点长度
	}

	// DesignTemplateIdAndNumItem 设计模板ID和数量
	DesignTemplateIdAndNumItem struct {
		Id  uint `json:"id" dc:"模板ID"`
		Num uint `json:"num" dc:"数量值（切词数量，卖点数量）"`
	}
)
