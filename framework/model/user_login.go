package model

import "gitee.com/bobo-rs/idea-space-framework/enums"

type (
	// CurrUserLoginDeviceListOutput 当前用户登陆设备列表属性
	CurrUserLoginDeviceListOutput struct {
		Rows []*UserLoginDeviceItem `json:"rows" dc:"数据"`
	}
)

type (

	// UserLoginDeviceItem 用户登陆设备属性
	UserLoginDeviceItem struct {
		UserLoginForceOfflineItem
		LoginDeviceAttrItem
	}

	// UserLoginAfterItem 用户认证-后置登录日志处理
	UserLoginAfterItem struct {
		UserOauthAfterValidateItem
		UserOauthLoginItem
	}

	// UserLoginCurrLoginItem 获取当前用户登录信息
	UserLoginCurrLoginItem struct {
		ExpireTime  uint `json:"expire_time"`
		Status      uint `json:"status"`
		IsUse       uint `json:"is_use"`
		IsNewDevice uint `json:"is_new_device"`
	}

	// UserLoginForceOfflineItem 强制登录账户下线
	UserLoginForceOfflineItem struct {
		Id     uint   `json:"id" dc:"设备ID"`
		UserId uint   `json:"user_id" dc:"用户ID"`
		Token  string `json:"token" dc:"登录Token"`
		UserLoginCurrLoginItem
	}

	// LoginAfterLogoutItem 用户退出登录后置处理
	LoginAfterLogoutItem struct {
		Uid          uint                   `json:"uid"`
		AccountToken string                 `json:"account_token"`
		LoginStatus  enums.LoginOauthStatus `json:"login_status"`
	}

	// LoginDeviceAttrItem 用户登陆设备属性
	LoginDeviceAttrItem struct {
		LoginIp         string `json:"login_ip" dc:"登录IP"`
		LoginIpLocation string `json:"login_ip_location" dc:"登录IP地址"`
		DeviceType      string `json:"device_type" dc:"设备类型"`
		DeviceContent   string `json:"device_content" dc:"设备详情"`
		Mac             string `json:"mac" dc:"MAC地址"`
		CreateAt        string `json:"create_at" dc:"创建时间"`
	}

	// LoginUserIdAndTokenItem 用户登录获取用户ID和Token
	LoginUserIdAndTokenItem struct {
		UserId uint   `json:"user_id" dc:"用户ID"`
		Token  string `json:"token" dc:"登录Token"`
	}
)
