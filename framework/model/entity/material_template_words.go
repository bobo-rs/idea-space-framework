// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// MaterialTemplateWords is the golang structure for table material_template_words.
type MaterialTemplateWords struct {
	Id             uint        `json:"id"               description:"素材模板文案ID"`          // 素材模板文案ID
	MaterialId     uint        `json:"material_id"      description:"素材ID"`              // 素材ID
	TemplateId     uint        `json:"template_id"      description:"素材模板ID"`            // 素材模板ID
	ExampleWords   string      `json:"example_words"    description:"示例文案"`              // 示例文案
	FontFamily     string      `json:"font_family"      description:"字体系列，例如：微软雅黑"`      // 字体系列，例如：微软雅黑
	WordsType      uint        `json:"words_type"       description:"文案类型：0空白，1文字，2.价格"` // 文案类型：0空白，1文字，2.价格
	WordsFont      float64     `json:"words_font"       description:"文案字体字号"`            // 文案字体字号
	WordsFontColor string      `json:"words_font_color" description:"文按字体颜色"`            // 文按字体颜色
	WordsX         int         `json:"words_x"          description:"切词文案卖点X坐标"`         // 切词文案卖点X坐标
	WordsY         int         `json:"words_y"          description:"切词文案卖点Y坐标"`         // 切词文案卖点Y坐标
	LayerPosition  uint        `json:"layer_position"   description:"图层位置，切词优先词"`        // 图层位置，切词优先词
	WordsNum       uint        `json:"words_num"        description:"文本数量"`              // 文本数量
	MaterialX      int         `json:"material_x"       description:"素材X坐标"`             // 素材X坐标
	MaterialY      int         `json:"material_y"       description:"素材Y坐标"`             // 素材Y坐标
	MaterialColor  string      `json:"material_color"   description:"素材色系"`              // 素材色系
	Url            string      `json:"url"              description:"素材URL"`             // 素材URL
	UpdateAt       *gtime.Time `json:"update_at"        description:"更新时间"`              // 更新时间
	CreateAt       *gtime.Time `json:"create_at"        description:"创建时间"`              // 创建时间
}
