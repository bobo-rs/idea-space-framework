// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// AdminDepartment is the golang structure for table admin_department.
type AdminDepartment struct {
	Id       uint        `json:"id"        description:"管理员部门ID"`     // 管理员部门ID
	AdminId  uint        `json:"admin_id"  description:"管理员ID，非用户ID"` // 管理员ID，非用户ID
	DepartId uint        `json:"depart_id" description:"部门ID"`        // 部门ID
	UpdateAt *gtime.Time `json:"update_at" description:"更新时间"`        // 更新时间
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`        // 创建时间
}
