// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// UserCredential is the golang structure for table user_credential.
type UserCredential struct {
	Id              uint        `json:"id"                description:"用户证件ID"`          // 用户证件ID
	UserId          uint        `json:"user_id"           description:"用户ID"`            // 用户ID
	IdCard          string      `json:"id_card"           description:"证件编号"`            // 证件编号
	JustUrl         string      `json:"just_url"          description:"证件正面URL地址"`       // 证件正面URL地址
	OppositeUrl     string      `json:"opposite_url"      description:"证件反面URL地址"`       // 证件反面URL地址
	Type            uint        `json:"type"              description:"证件类型：0身份证"`       // 证件类型：0身份证
	ExpireType      uint        `json:"expire_type"       description:"证件效期类型：0有效期，1长期"` // 证件效期类型：0有效期，1长期
	ExpireStartDate *gtime.Time `json:"expire_start_date" description:"证件注册时间"`          // 证件注册时间
	ExpireEndDate   *gtime.Time `json:"expire_end_date"   description:"证件到期时间"`          // 证件到期时间
	UpdateAt        *gtime.Time `json:"update_at"         description:"更新时间"`            // 更新时间
	CreateAt        *gtime.Time `json:"create_at"         description:"创建时间"`            // 创建时间
}
