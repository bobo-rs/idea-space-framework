// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Label is the golang structure for table label.
type Label struct {
	Id         uint        `json:"id"          description:"标签类ID"`        // 标签类ID
	Name       string      `json:"name"        description:"标签类名"`         // 标签类名
	IsRequired uint        `json:"is_required" description:"是否必须勾选：0否，1是"` // 是否必须勾选：0否，1是
	Creator    string      `json:"creator"     description:"最先创建人"`        // 最先创建人
	Modified   string      `json:"modified"    description:"最后修改人"`        // 最后修改人
	UpdateAt   *gtime.Time `json:"update_at"   description:"更新时间"`         // 更新时间
	CreateAt   *gtime.Time `json:"create_at"   description:"创建时间"`         // 创建时间
}
