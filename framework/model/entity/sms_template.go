// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SmsTemplate is the golang structure for table sms_template.
type SmsTemplate struct {
	Id       uint        `json:"id"        description:"模板ID"`                  // 模板ID
	Name     string      `json:"name"      description:"模板名"`                   // 模板名
	Content  string      `json:"content"   description:"模板内容"`                  // 模板内容
	VarAlias string      `json:"var_alias" description:"模板别名"`                  // 模板别名
	SmsType  uint        `json:"sms_type"  description:"短信类型：0验证码，1消息通知，2营销短信"` // 短信类型：0验证码，1消息通知，2营销短信
	Status   uint        `json:"status"    description:"模板状态：0关闭，1启用"`          // 模板状态：0关闭，1启用
	Remark   string      `json:"remark"    description:"备注"`                    // 备注
	UpdateAt *gtime.Time `json:"update_at" description:"更新时间"`                  // 更新时间
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`                  // 创建时间
}
