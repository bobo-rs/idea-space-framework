// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SpaceImagesDetail is the golang structure for table space_images_detail.
type SpaceImagesDetail struct {
	SpaceId      uint        `json:"space_id"       description:"设计空间图片ID"` // 设计空间图片ID
	TemplateId   uint        `json:"template_id"    description:"模板ID"`     // 模板ID
	Synopsis     string      `json:"synopsis"       description:"图片简介"`     // 图片简介
	LabelValues  string      `json:"label_values"   description:"标签值"`      // 标签值
	FullCutWords string      `json:"full_cut_words" description:"完整切词卖点"`   // 完整切词卖点
	Creator      string      `json:"creator"        description:"创建人"`      // 创建人
	Modified     string      `json:"modified"       description:"修改人"`      // 修改人
	UpdateAt     *gtime.Time `json:"update_at"      description:"更新时间"`     // 更新时间
}
