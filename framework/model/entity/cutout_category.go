// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// CutoutCategory is the golang structure for table cutout_category.
type CutoutCategory struct {
	Id          uint        `json:"id"            description:"抠图分类ID"`     // 抠图分类ID
	Name        string      `json:"name"          description:"抠图分类名"`      // 抠图分类名
	BriefName   string      `json:"brief_name"    description:"简短名"`        // 简短名
	Synopsis    string      `json:"synopsis"      description:"简介内容"`       // 简介内容
	AdvImageUrl string      `json:"adv_image_url" description:"广告图URL"`     // 广告图URL
	Rule        string      `json:"rule"          description:"抠图规则"`       // 抠图规则
	IsPromotion uint        `json:"is_promotion"  description:"是否推广：0否，1是"` // 是否推广：0否，1是
	UpdateAt    *gtime.Time `json:"update_at"     description:"更新时间"`       // 更新时间
	CreateAt    *gtime.Time `json:"create_at"     description:"创建时间"`       // 创建时间
}
