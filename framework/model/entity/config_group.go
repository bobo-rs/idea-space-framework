// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// ConfigGroup is the golang structure for table config_group.
type ConfigGroup struct {
	Id          uint        `json:"id"          description:"配置组ID"`            // 配置组ID
	Name        string      `json:"name"        description:"配置组名"`             // 配置组名
	GroupName   string      `json:"group_name"  description:"配置组关联名：basic基础配置"` // 配置组关联名：basic基础配置
	Description string      `json:"description" description:"详情"`               // 详情
	Pid         uint        `json:"pid"         description:"父级ID"`             // 父级ID
	UpdateAt    *gtime.Time `json:"update_at"   description:"更新时间"`             // 更新时间
	CreateAt    *gtime.Time `json:"create_at"   description:"创建时间"`             // 创建时间
}
