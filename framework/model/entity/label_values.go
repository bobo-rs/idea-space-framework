// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// LabelValues is the golang structure for table label_values.
type LabelValues struct {
	Id       uint        `json:"id"        description:"标签值ID"`        // 标签值ID
	LabelId  uint        `json:"label_id"  description:"标签ID"`         // 标签ID
	Value    string      `json:"value"     description:"标签值"`          // 标签值
	IsDel    uint        `json:"is_del"    description:"是否删除：0正常，1删除"` // 是否删除：0正常，1删除
	Hash     string      `json:"hash"      description:"标签值Hash"`      // 标签值Hash
	UpdateAt *gtime.Time `json:"update_at" description:"更新时间"`         // 更新时间
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`         // 创建时间
}
