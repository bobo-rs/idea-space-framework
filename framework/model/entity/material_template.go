// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// MaterialTemplate is the golang structure for table material_template.
type MaterialTemplate struct {
	Id           uint        `json:"id"            description:"模板ID"`                                 // 模板ID
	TemplateName string      `json:"template_name" description:"模板名"`                                  // 模板名
	LabelValues  string      `json:"label_values"  description:"标签值（渠道-行业-营销）"`                        // 标签值（渠道-行业-营销）
	CutWordsNum  uint        `json:"cut_words_num" description:"切词卖点数量"`                               // 切词卖点数量
	SellingLen   uint        `json:"selling_len"   description:"卖点长度（容错匹配，当切词卖点检索不到模板，直接卖点长度匹配，就近原则）"` // 卖点长度（容错匹配，当切词卖点检索不到模板，直接卖点长度匹配，就近原则）
	IdxStatus    uint        `json:"idx_status"    description:"标签索引状态：0创建中、1已完成、2中止失败"`               // 标签索引状态：0创建中、1已完成、2中止失败
	Status       uint        `json:"status"        description:"模板状态：0待审核，1正常，2下架"`                    // 模板状态：0待审核，1正常，2下架
	UseNum       uint        `json:"use_num"       description:"使用量"`                                  // 使用量
	MatchNum     uint        `json:"match_num"     description:"匹配数量"`                                 // 匹配数量
	VipType      uint        `json:"vip_type"      description:"VIP类型：0免费，1VIP"`                       // VIP类型：0免费，1VIP
	ChannelId    uint        `json:"channel_id"    description:"渠道ID"`                                 // 渠道ID
	ExampleImg   string      `json:"example_img"   description:"示例模板图片地址"`                             // 示例模板图片地址
	Creator      string      `json:"creator"       description:"创建人"`                                  // 创建人
	CreatorId    uint        `json:"creator_id"    description:"创建人ID"`                                // 创建人ID
	Modified     string      `json:"modified"      description:"修改人"`                                  // 修改人
	UpdateAt     *gtime.Time `json:"update_at"     description:"更新时间"`                                 // 更新时间
	CreateAt     *gtime.Time `json:"create_at"     description:"创建时间"`                                 // 创建时间
}
