// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// LabelIdx is the golang structure for table label_idx.
type LabelIdx struct {
	Id            uint        `json:"id"             description:"标签索引ID"`                                       // 标签索引ID
	CorrelateId   uint        `json:"correlate_id"   description:"关联ID"`                                         // 关联ID
	CorrelateType uint        `json:"correlate_type" description:"关联类型：0素材模板，1设计图"`                              // 关联类型：0素材模板，1设计图
	LabelValue    string      `json:"label_value"    description:"标签值(例如：淘宝/天猫-美妆-双十一，淘宝/天猫-美妆-双十二等)"`           // 标签值(例如：淘宝/天猫-美妆-双十一，淘宝/天猫-美妆-双十二等)
	LabelHash     string      `json:"label_hash"     description:"标签Hash值(例如：淘宝/天猫-美妆-双十一，淘宝/天猫-美妆-双十二等，md5加密)"` // 标签Hash值(例如：淘宝/天猫-美妆-双十一，淘宝/天猫-美妆-双十二等，md5加密)
	UpdateAt      *gtime.Time `json:"update_at"      description:"更新时间"`                                         // 更新时间
	CreateAt      *gtime.Time `json:"create_at"      description:"创建时间"`                                         // 创建时间
}
