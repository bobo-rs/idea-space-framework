// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// User is the golang structure for table user.
type User struct {
	Id       uint        `json:"id"        description:"用户ID"`            // 用户ID
	Account  string      `json:"account"   description:"账号"`              // 账号
	Mobile   string      `json:"mobile"    description:"手机号（加密）"`         // 手机号（加密）
	Nickname string      `json:"nickname"  description:"昵称"`              // 昵称
	UniqueId string      `json:"unique_id" description:"对外身份ID"`          // 对外身份ID
	Status   uint        `json:"status"    description:"状态：0正常，1冻结，2待注销"` // 状态：0正常，1冻结，2待注销
	UpdateAt *gtime.Time `json:"update_at" description:"更新时间"`            // 更新时间
	CreateAt *gtime.Time `json:"create_at" description:"注册时间"`            // 注册时间
}
