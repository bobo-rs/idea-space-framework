// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SmsRecord is the golang structure for table sms_record.
type SmsRecord struct {
	Id         uint        `json:"id"          description:"短信发送记录编号"`              // 短信发送记录编号
	Mobile     string      `json:"mobile"      description:"接受短信的手机号"`              // 接受短信的手机号
	Content    string      `json:"content"     description:"短信内容"`                  // 短信内容
	Code       string      `json:"code"        description:"验证码"`                   // 验证码
	Ip         string      `json:"ip"          description:"添加记录ip"`                // 添加记录ip
	TemplateId string      `json:"template_id" description:"短信模板ID"`                // 短信模板ID
	SmsType    uint        `json:"sms_type"    description:"短信类型：0验证码，1消息通知，2营销短信"` // 短信类型：0验证码，1消息通知，2营销短信
	SerialId   string      `json:"serial_id"   description:"发送记录id"`                // 发送记录id
	Results    string      `json:"results"     description:"发送结果"`                  // 发送结果
	UpdateAt   *gtime.Time `json:"update_at"   description:"更新时间"`                  // 更新时间
	CreateAt   *gtime.Time `json:"create_at"   description:"创建时间"`                  // 创建时间
}
