// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// UserFunds is the golang structure for table user_funds.
type UserFunds struct {
	UserId                uint        `json:"user_id"                  description:"用户ID"`   // 用户ID
	RechargeTotalFee      float64     `json:"recharge_total_fee"       description:"充值总金额"`  // 充值总金额
	AvailableFee          float64     `json:"available_fee"            description:"可用金额"`   // 可用金额
	FreezeFee             float64     `json:"freeze_fee"               description:"冻结金额"`   // 冻结金额
	DesignTotalNum        uint        `json:"design_total_num"         description:"设计币总数"`  // 设计币总数
	AvailableDesignNum    uint        `json:"available_design_num"     description:"可用设计币"`  // 可用设计币
	FreezeDesignNum       uint        `json:"freeze_design_num"        description:"冻结设计币"`  // 冻结设计币
	ConsumeTotalDesignNum float64     `json:"consume_total_design_num" description:"消费总设计币"` // 消费总设计币
	ConsumeTotalFee       float64     `json:"consume_total_fee"        description:"消费总金额"`  // 消费总金额
	UpdateAt              *gtime.Time `json:"update_at"                description:"更新时间"`   // 更新时间
}
