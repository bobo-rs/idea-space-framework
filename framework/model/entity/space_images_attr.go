// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SpaceImagesAttr is the golang structure for table space_images_attr.
type SpaceImagesAttr struct {
	SpaceId     uint        `json:"space_id"     description:"设计空间图片ID"`              // 设计空间图片ID
	StorageType string      `json:"storage_type" description:"存储类型：local本地，qiniu七牛等"` // 存储类型：local本地，qiniu七牛等
	AttachId    string      `json:"attach_id"    description:"附件ID"`                  // 附件ID
	Size        uint        `json:"size"         description:"内存大小"`                  // 内存大小
	Width       uint        `json:"width"        description:"宽度"`                    // 宽度
	Height      uint        `json:"height"       description:"高度"`                    // 高度
	Precision   uint        `json:"precision"    description:"精度：0-9，越高精度越高"`         // 精度：0-9，越高精度越高
	RbgChannel  string      `json:"rbg_channel"  description:"RBG通道"`                 // RBG通道
	UpdateAt    *gtime.Time `json:"update_at"    description:"更新时间"`                  // 更新时间
}
