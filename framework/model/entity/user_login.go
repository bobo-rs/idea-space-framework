// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// UserLogin is the golang structure for table user_login.
type UserLogin struct {
	Id              uint        `json:"id"                description:"用户登录记录ID"`         // 用户登录记录ID
	UserId          uint        `json:"user_id"           description:"用户ID"`             // 用户ID
	LoginIp         string      `json:"login_ip"          description:"登录IP"`             // 登录IP
	LoginIpLocation string      `json:"login_ip_location" description:"登录归属地"`            // 登录归属地
	Token           string      `json:"token"             description:"登录token"`          // 登录token
	ExpireTime      uint64      `json:"expire_time"       description:"过期时间"`             // 过期时间
	Status          int         `json:"status"            description:"状态：0在线，1下线，2超时下线"` // 状态：0在线，1下线，2超时下线
	IsUse           int         `json:"is_use"            description:"是否一直在使用；0是，1否"`    // 是否一直在使用；0是，1否
	DeviceType      string      `json:"device_type"       description:"设备类型"`             // 设备类型
	DeviceContent   string      `json:"device_content"    description:"设备详情"`             // 设备详情
	IsNewDevice     int         `json:"is_new_device"     description:"是否新设备：0是，1新设备"`    // 是否新设备：0是，1新设备
	Mac             string      `json:"mac"               description:"Mac地址"`            // Mac地址
	UpdateAt        *gtime.Time `json:"update_at"         description:"更新时间"`             // 更新时间
	CreateAt        *gtime.Time `json:"create_at"         description:"登录时间"`             // 登录时间
}
