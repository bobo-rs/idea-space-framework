// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Permissions is the golang structure for table permissions.
type Permissions struct {
	Id            uint        `json:"id"             description:"权限ID"`                       // 权限ID
	Name          string      `json:"name"           description:"权限名"`                        // 权限名
	Description   string      `json:"description"    description:"权限详情"`                       // 权限详情
	MenuType      uint        `json:"menu_type"      description:"权限类型：0主菜单，1子菜单，2页面操作，3数据授权"` // 权限类型：0主菜单，1子菜单，2页面操作，3数据授权
	Icon          string      `json:"icon"           description:"Icon图"`                      // Icon图
	Path          string      `json:"path"           description:"页面地址"`                       // 页面地址
	ApiPath       string      `json:"api_path"       description:"接口地址"`                       // 接口地址
	Pid           uint        `json:"pid"            description:"父级权限ID"`                     // 父级权限ID
	IsShow        uint        `json:"is_show"        description:"菜单是否显示：0显示，1隐藏"`             // 菜单是否显示：0显示，1隐藏
	IsDisabled    uint        `json:"is_disabled"    description:"是否禁用：0正常，1禁用"`               // 是否禁用：0正常，1禁用
	PermCode      string      `json:"perm_code"      description:"操作权限码"`                      // 操作权限码
	DisableColumn string      `json:"disable_column" description:"禁用字段（3.数据授权类型）"`             // 禁用字段（3.数据授权类型）
	Sort          uint        `json:"sort"           description:"排序值0-255"`                   // 排序值0-255
	UpdateAt      *gtime.Time `json:"update_at"      description:"更新时间"`                       // 更新时间
	CreateAt      *gtime.Time `json:"create_at"      description:"创建时间"`                       // 创建时间
}
