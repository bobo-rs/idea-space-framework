// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// UserAdmin is the golang structure for table user_admin.
type UserAdmin struct {
	Id            uint        `json:"id"              description:"管理员ID"`                     // 管理员ID
	UserId        uint        `json:"user_id"         description:"用户ID"`                      // 用户ID
	ManageName    string      `json:"manage_name"     description:"管理名称"`                      // 管理名称
	ManageNo      string      `json:"manage_no"       description:"管理员编号"`                     // 管理员编号
	IsSuperManage uint        `json:"is_super_manage" description:"是否超管：0普管，1超管"`              // 是否超管：0普管，1超管
	ManageStatus  uint        `json:"manage_status"   description:"管理员状态：0正常，1冻结，2离职，3违规，4注销"` // 管理员状态：0正常，1冻结，2离职，3违规，4注销
	LogoffAt      *gtime.Time `json:"logoff_at"       description:"注销时间"`                      // 注销时间
	UpdateAt      *gtime.Time `json:"update_at"       description:"更新时间"`                      // 更新时间
	CreateAt      *gtime.Time `json:"create_at"       description:"穿件时间"`                      // 穿件时间
}
