// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Material is the golang structure for table material.
type Material struct {
	Id           uint        `json:"id"            description:"素材ID"`                                                                                        // 素材ID
	MaterialName string      `json:"material_name" description:"素材名称"`                                                                                        // 素材名称
	MaterialType uint        `json:"material_type" description:"素材类型：0素材配图，1素材主图（未修饰过的图）"`                                                                    // 素材类型：0素材配图，1素材主图（未修饰过的图）
	ChannelId    uint        `json:"channel_id"    description:"素材渠道：0平台，1企业，2用户"`                                                                            // 素材渠道：0平台，1企业，2用户
	Position     string      `json:"position"      description:"素材位置：main主图，market营销活动卖点，top顶部卖点，btm-main底部主卖点，btm-second底部次卖点，btm-single底部单独卖点，detail详情图卖点"` // 素材位置：main主图，market营销活动卖点，top顶部卖点，btm-main底部主卖点，btm-second底部次卖点，btm-single底部单独卖点，detail详情图卖点
	Width        uint        `json:"width"         description:"素材宽度"`                                                                                        // 素材宽度
	Height       uint        `json:"height"        description:"素材高度"`                                                                                        // 素材高度
	Url          string      `json:"url"           description:"素材地址"`                                                                                        // 素材地址
	AttachId     string      `json:"attach_id"     description:"附件ID"`                                                                                        // 附件ID
	CreatorId    uint        `json:"creator_id"    description:"创建人ID"`                                                                                       // 创建人ID
	Creator      string      `json:"creator"       description:"创建人"`                                                                                         // 创建人
	Modified     string      `json:"modified"      description:"修改人"`                                                                                         // 修改人
	Status       uint        `json:"status"        description:"素材状态：0正常，1废弃"`                                                                                // 素材状态：0正常，1废弃
	UpdateAt     *gtime.Time `json:"update_at"     description:"更新时间"`                                                                                        // 更新时间
	CreateAt     *gtime.Time `json:"create_at"     description:"创建时间"`                                                                                        // 创建时间
}
