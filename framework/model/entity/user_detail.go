// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// UserDetail is the golang structure for table user_detail.
type UserDetail struct {
	UserId          uint        `json:"user_id"           description:"用户ID"`     // 用户ID
	Sex             uint        `json:"sex"               description:"性别：0男，1女"` // 性别：0男，1女
	Birthday        *gtime.Time `json:"birthday"          description:"生日"`       // 生日
	PwdHash         string      `json:"pwd_hash"          description:"密码hash值"`  // 密码hash值
	PwdSalt         string      `json:"pwd_salt"          description:"加密盐"`      // 加密盐
	Avatar          string      `json:"avatar"            description:"头像"`       // 头像
	AttachId        string      `json:"attach_id"         description:"附件ID"`     // 附件ID
	RegIp           string      `json:"reg_ip"            description:"注册IP"`     // 注册IP
	RegIpLocation   string      `json:"reg_ip_location"   description:"注册归属地"`    // 注册归属地
	LoginIp         string      `json:"login_ip"          description:"登录IP"`     // 登录IP
	LoginIpLocation string      `json:"login_ip_location" description:"登录归属地"`    // 登录归属地
	LogoutAt        *gtime.Time `json:"logout_at"         description:"注销时间"`     // 注销时间
	SignNum         uint        `json:"sign_num"          description:"签到总数"`     // 签到总数
	ConSign         uint        `json:"con_sign"          description:"连续签到时间"`   // 连续签到时间
	UpdateAt        *gtime.Time `json:"update_at"         description:"更新时间"`     // 更新时间
}
