// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// CutoutImages is the golang structure for table cutout_images.
type CutoutImages struct {
	Id           uint        `json:"id"             description:"抠图ID"`   // 抠图ID
	CutoutCateId int         `json:"cutout_cate_id" description:"抠图分类ID"` // 抠图分类ID
	CutoutName   string      `json:"cutout_name"    description:"抠图名"`    // 抠图名
	ImageUrl     string      `json:"image_url"      description:"图片地址"`   // 图片地址
	DownloadNum  uint        `json:"download_num"   description:"下载量"`    // 下载量
	BrowseNum    uint        `json:"browse_num"     description:"浏览数量"`   // 浏览数量
	ChannelId    uint        `json:"channel_id"     description:"渠道ID"`   // 渠道ID
	CreatorId    uint        `json:"creator_id"     description:"创建人ID"`  // 创建人ID
	Creator      string      `json:"creator"        description:"创建人"`    // 创建人
	Modified     string      `json:"modified"       description:"修改人"`    // 修改人
	UpdateAt     *gtime.Time `json:"update_at"      description:"更新时间"`   // 更新时间
	CreateAt     *gtime.Time `json:"create_at"      description:"创建时间"`   // 创建时间
}
