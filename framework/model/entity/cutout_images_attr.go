// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// CutoutImagesAttr is the golang structure for table cutout_images_attr.
type CutoutImagesAttr struct {
	CutImageId  uint        `json:"cut_image_id" description:"抠图图片ID"`                 // 抠图图片ID
	AttachId    string      `json:"attach_id"    description:"附件ID"`                   // 附件ID
	ImageUrl    string      `json:"image_url"    description:"图片地址"`                   // 图片地址
	ImagePath   string      `json:"image_path"   description:"图片路径"`                   // 图片路径
	Filename    string      `json:"filename"     description:"文件名"`                    // 文件名
	Suffix      string      `json:"suffix"       description:"图片后缀"`                   // 图片后缀
	StorageType string      `json:"storage_type" description:"存储类型：local本地存储，qiniu七牛"` // 存储类型：local本地存储，qiniu七牛
	Size        int         `json:"size"         description:"图片大小内存"`                 // 图片大小内存
	Width       int         `json:"width"        description:"图片宽度"`                   // 图片宽度
	Height      int         `json:"height"       description:"图片高度"`                   // 图片高度
	UpdateAt    *gtime.Time `json:"update_at"    description:"更新时间"`                   // 更新时间
}
