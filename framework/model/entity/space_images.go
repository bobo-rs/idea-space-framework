// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SpaceImages is the golang structure for table space_images.
type SpaceImages struct {
	Id          uint        `json:"id"           description:"空间图片ID"`                 // 空间图片ID
	SpaceName   string      `json:"space_name"   description:"空间图片名"`                  // 空间图片名
	ImageUrl    string      `json:"image_url"    description:"图片URL"`                  // 图片URL
	VipType     uint        `json:"vip_type"     description:"授权类型：0免费，1VIP"`          // 授权类型：0免费，1VIP
	IsOpen      uint        `json:"is_open"      description:"是否开放浏览：0开放，1关闭"`         // 是否开放浏览：0开放，1关闭
	Price       float64     `json:"price"        description:"价格（VIP）"`                // 价格（VIP）
	DesignBean  uint        `json:"design_bean"  description:"设计豆（虚拟货币）"`              // 设计豆（虚拟货币）
	DownloadNum uint        `json:"download_num" description:"下载数量"`                   // 下载数量
	Browse      uint        `json:"browse"       description:"浏览量"`                    // 浏览量
	BuyNum      uint        `json:"buy_num"      description:"购买量"`                    // 购买量
	DesignType  uint        `json:"design_type"  description:"设计类型：0智能设计，1手动设计，2主动设计"` // 设计类型：0智能设计，1手动设计，2主动设计
	ChannelId   uint        `json:"channel_id"   description:"渠道来源ID"`                 // 渠道来源ID
	CreatorId   uint        `json:"creator_id"   description:"创建人ID"`                  // 创建人ID
	UpdateAt    *gtime.Time `json:"update_at"    description:"更新时间"`                   // 更新时间
	CreateAt    *gtime.Time `json:"create_at"    description:"创建时间"`                   // 创建时间
}
