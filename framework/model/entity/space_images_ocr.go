// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-06-14 11:15:10
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SpaceImagesOcr is the golang structure for table space_images_ocr.
type SpaceImagesOcr struct {
	Id         uint        `json:"id"          description:"文案OCRID"`               // 文案OCRID
	ImageId    uint        `json:"image_id"    description:"图片ID"`                  // 图片ID
	WordsValue string      `json:"words_value" description:"文案值"`                   // 文案值
	WordsFont  int         `json:"words_font"  description:"文案字号大小"`                // 文案字号大小
	WordsColor string      `json:"words_color" description:"文案颜色"`                  // 文案颜色
	WordsX     float64     `json:"words_x"     description:"文案坐标X"`                 // 文案坐标X
	WordsY     float64     `json:"words_y"     description:"文案坐标Y"`                 // 文案坐标Y
	WordsLen   uint        `json:"words_len"   description:"文案长度"`                  // 文案长度
	Width      uint        `json:"width"       description:"宽度"`                    // 宽度
	Height     uint        `json:"height"      description:"高度"`                    // 高度
	IsOrigin   uint        `json:"is_origin"   description:"是否原文：0原文，1翻译"`          // 是否原文：0原文，1翻译
	Lang       string      `json:"lang"        description:"语言：zh-CN简体中文"`          // 语言：zh-CN简体中文
	WordsHash  string      `json:"words_hash"  description:"Hash值（文案内容+是否主体+语言类型）"` // Hash值（文案内容+是否主体+语言类型）
	UpdateAt   *gtime.Time `json:"update_at"   description:"更新时间"`                  // 更新时间
	CreateAt   *gtime.Time `json:"create_at"   description:"创建时间"`                  // 创建时间
}
