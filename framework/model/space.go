package model

import (
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
)

type (
	// SpaceListInput 设计空间图片列表请求参数
	SpaceListInput struct {
		CommonPaginationItem
		SpaceListWhereItem
	}

	// SpaceListOutput 设计空间图片列表响应结果
	SpaceListOutput struct {
		CommonResponseItem
		Rows []SpaceImageItem `json:"rows" dc:"数据结果集"`
	}

	// SpaceDetailOutput 设计空间图片详情响应结果
	SpaceDetailOutput struct {
		SpaceDetailItem
	}
)

type (
	// SpaceListWhereItem 设计空间图片搜索列表属性
	SpaceListWhereItem struct {
		VipType    uint   `json:"vip_type" dc:"授权类型：0免费，1VIP"`
		IsOpen     uint   `json:"is_open" dc:"是否开放浏览"`
		Values     string `json:"values" dc:"标签值（渠道-行业-营销）"`
		DesignType uint   `json:"design_type" dc:"设计类型"`
		ChannelId  uint   `json:"channel_id" dc:"渠道ID"`
		CreatorId  uint   `json:"creator_id" dc:"创建人ID"`
	}

	// SpaceDetailItem 设计图片空间详情属性
	SpaceDetailItem struct {
		SpaceImageItem
		Detail *SpaceImageDetailItem `json:"detail" dc:"图片详情信息"`
		Attr   *SpaceImageAttrItem   `json:"attr" dc:"图片属性信息"`
	}

	// SpaceImageItem 设计空间图片属性
	SpaceImageItem struct {
		entity.SpaceImages
		FmtVip     string `json:"fmt_vip" dc:"授权类型"`
		FmtOpen    string `json:"fmt_open" dc:"是否开放"`
		FmtDesign  string `json:"fmt_design" dc:"设计类型"`
		FmtChannel string `json:"fmt_channel" dc:"渠道"`
	}
)

type (
	// SpaceDesignListInput 设计空间图片列表请求参数
	SpaceDesignListInput struct {
		CommonPaginationItem
		SpaceListWhereItem
		SortType enums.SpaceImageSortTypeEnums `json:"sort_type" dc:"排序类型：0默认排序（使用量>浏览量>最新），1最新发布，2最多使用"`
	}

	// SpaceDesignListOutput 设计空间图片列表响应结果
	SpaceDesignListOutput struct {
		CommonResponseItem
		Rows []SpaceImageItem `json:"rows" dc:"数据结果集"`
	}
)

type (

	// SpaceImageFmtItem 设计空间格式化属性
	SpaceImageFmtItem struct {
		FmtVip     string `json:"fmt_vip" dc:"授权类型"`
		FmtOpen    string `json:"fmt_open" dc:"是否开放"`
		FmtDesign  string `json:"fmt_design" dc:"设计类型"`
		FmtChannel string `json:"fmt_channel" dc:"渠道"`
	}

	// 设计空间下载浏览量
)
