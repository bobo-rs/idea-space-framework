package model

import (
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
)

type (

	// MaterialSaveInput 保存素材图片请求参数
	MaterialSaveInput struct {
		Id           uint                        `json:"id"`
		MaterialType enums.MaterialTypeEnums     `json:"material_type"`
		ChannelId    uint                        `json:"channel_id"`
		Position     enums.MaterialPositionEnums `json:"position"`
		AttachId     string                      `json:"attach_id"`
	}

	// MaterialSaveOutput 保存素材图片响应参数
	MaterialSaveOutput struct {
		Id uint `json:"id" dc:"素材ID"`
	}

	// MaterialListInput 素材列表请求参数
	MaterialListInput struct {
		CommonPaginationItem
		MaterialListWhereItem
	}

	// MaterialListWhereItem 素材搜索条件
	MaterialListWhereItem struct {
		MaterialType uint   `json:"material_type"`
		ChannelId    uint   `json:"channel_id"`
		Position     string `json:"position"`
		Status       uint16 `json:"status"`
		MaterialName string `json:"material_name"`
	}

	// MaterialListOutput 素材列表响应输出
	MaterialListOutput struct {
		CommonResponseItem
		Rows []MaterialDetailItem `json:"rows"`
	}

	// MaterialDetailInput 获取素材详情请求参数
	MaterialDetailInput struct {
		Id uint `json:"id"`
	}

	// MaterialDetailOutput 素材详情结果响应
	MaterialDetailOutput struct {
		MaterialDetailItem
	}

	// MaterialDeleteInput 删除素材详情请求参数
	MaterialDeleteInput struct {
		Id uint `json:"id"`
	}
)

type (
	// MaterialAttachmentItem 获取附件详情
	MaterialAttachmentItem struct {
		Id       uint   `json:"id"`
		RealName string `json:"real_name"`
		AttUrl   string `json:"att_url"`
		Hasher   string `json:"hasher"`
		AttachId string `json:"attach_id"`
		Width    uint   `json:"width"`
		Height   uint   `json:"height"`
	}

	// MaterialDetailItem 素材详情属性
	MaterialDetailItem struct {
		entity.Material
		FmtPosition     string `json:"fmt_position" dc:"格式化位置"`
		FmtChannel      string `json:"fmt_channel" dc:"渠道"`
		FmtMaterialType string `json:"fmt_material_type" dc:"素材类型"`
		FmtStatus       string `json:"fmt_status" dc:"状态"`
	}

	// MaterialBasicItem 素材基础详情属性
	MaterialBasicItem struct {
		Id           uint   `json:"id"             description:"素材ID"`                                                                                        // 素材ID
		MaterialName string `json:"material_name"  description:"素材名称"`                                                                                        // 素材名称
		MaterialType uint   `json:"material_type"  description:"素材类型：0素材配图，1素材主图（未修饰过的图）"`                                                                    // 素材渠道：0平台，1企业，2用户
		Position     string `json:"position"       description:"素材位置：main主图，market营销活动卖点，top顶部卖点，btm-main底部主卖点，btm-second底部次卖点，btm-single底部单独卖点，detail详情图卖点"` // 素材位置：main主图，market营销活动卖点，top顶部卖点，btm-main底部主卖点，btm-second底部次卖点，btm-single底部单独卖点，detail详情图卖点
		Width        uint   `json:"width"          description:"素材宽度"`                                                                                        // 素材宽度
		Height       uint   `json:"height"         description:"素材高度"`                                                                                        // 图层位置
		Url          string `json:"url"            description:"素材地址"`                                                                                        // 素材地址
		AttachId     string `json:"attach_id"      description:"附件ID"`                                                                                        // 修改人
		Status       uint   `json:"status"         description:"素材状态：0正常，1废弃"`
	}
)
