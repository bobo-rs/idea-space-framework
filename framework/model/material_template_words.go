package model

import (
	"gitee.com/bobo-rs/idea-space-framework/enums"
	"gitee.com/bobo-rs/idea-space-framework/framework/model/entity"
	"gitee.com/bobo-rs/idea-space-framework/pkg/core/models"
)

type (
	// MaterialTemplateWordsSaveItem 素材模板文案保存属性
	MaterialTemplateWordsSaveItem struct {
		Id             uint                                      `json:"id"               dc:"素材模板文案ID"`          // 素材模板文案ID
		MaterialId     uint                                      `json:"material_id"      dc:"素材ID"`              // 素材ID
		TemplateId     uint                                      `json:"template_id"      dc:"素材模板ID"`            // 素材模板ID
		ExampleWords   string                                    `json:"example_words"    dc:"示例文案"`              // 示例文案
		FontFamily     string                                    `json:"font_family"      dc:"字体系列，例如：微软雅黑"`      // 字体系列，例如：微软雅黑
		WordsType      enums.MaterialTemplateWordsWordsTypeEnums `json:"words_type"       dc:"文案类型：0空白，1文字，2.价格"` // 文案类型：0空白，1文字，2.价格
		WordsFont      float64                                   `json:"words_font"       dc:"文案字体字号"`            // 文案字体字号
		WordsFontColor string                                    `json:"words_font_color" dc:"文按字体颜色"`            // 文按字体颜色
		WordsX         int                                       `json:"words_x"          dc:"切词文案卖点X坐标"`         // 切词文案卖点X坐标
		WordsY         int                                       `json:"words_y"          dc:"切词文案卖点Y坐标"`         // 切词文案卖点Y坐标
		LayerPosition  uint                                      `json:"layer_position"   dc:"图层位置，切词优先词"`        // 图层位置，切词优先词
		MaterialX      int                                       `json:"material_x"       dc:"素材X坐标"`             // 素材X坐标
		MaterialY      int                                       `json:"material_y"       dc:"素材Y坐标"`             // 素材Y坐标
		MaterialColor  string                                    `json:"material_color"   dc:"素材色系"`              // 素材色系
	}
)

type (

	// MaterialTemplateWordsDetailItem 素材模板详情属性
	MaterialTemplateWordsDetailItem struct {
		entity.MaterialTemplateWords
		Material *MaterialBasicItem `json:"material" dc:"素材详情" orm:"with:material_id=id"`
	}

	MatchTemplateWordsItem struct {
		Words      []MaterialTemplateWordsBasicItem `json:"words"`
		MatchWords []models.DesignTemplateWordsItem `json:"match_words"`
	}

	// MaterialTemplateWordsBasicItem 素材模板文本属性
	MaterialTemplateWordsBasicItem struct {
		Id             uint    `json:"id"               dc:"素材模板文案ID"`          // 素材模板文案ID
		MaterialId     uint    `json:"material_id"      dc:"素材ID"`              // 素材ID
		TemplateId     uint    `json:"template_id"      dc:"素材模板ID"`            // 素材模板ID
		ExampleWords   string  `json:"example_words"    dc:"示例文案"`              // 示例文案
		FontFamily     string  `json:"font_family"      dc:"字体系列，例如：微软雅黑"`      // 字体系列，例如：微软雅黑
		WordsType      uint    `json:"words_type"       dc:"文案类型：0空白，1文字，2.价格"` // 文案类型：0空白，1文字，2.价格
		WordsFont      float64 `json:"words_font"       dc:"文案字体字号"`            // 文案字体字号
		WordsFontColor string  `json:"words_font_color" dc:"文按字体颜色"`            // 文按字体颜色
		WordsX         int     `json:"words_x"          dc:"切词文案卖点X坐标"`         // 切词文案卖点X坐标
		WordsY         int     `json:"words_y"          dc:"切词文案卖点Y坐标"`         // 切词文案卖点Y坐标
		LayerPosition  uint    `json:"layer_position"   dc:"图层位置，切词优先词"`        // 图层位置，切词优先词
		MaterialX      int     `json:"material_x"       dc:"素材X坐标"`             // 素材X坐标
		MaterialY      int     `json:"material_y"       dc:"素材Y坐标"`             // 素材Y坐标
		MaterialColor  string  `json:"material_color"   dc:"素材色系"`              // 素材色系
		Url            string  `json:"url" dc:"素材地址"`
		WordsNum       uint    `json:"words_num" dc:"文本数量"`
	}
)
