package model

import "gitee.com/bobo-rs/idea-space-framework/enums"

type (
	// UserAccountItem 用户账户信息
	UserAccountItem struct {
		Uid          uint   `json:"uid" dc:"用户ID"`
		Account      string `json:"account" dc:"账户"`
		Nickname     string `json:"nickname" dc:"昵称"`
		IsAdmin      bool   `json:"is_admin" dc:"是否管理员：false否，true是"`
		IsSupper     bool   `json:"is_supper" dc:"是否超管：false否，true是"`
		AccountToken string `json:"account_token" dc:"SHA256加密登录Token后TOKEN"`
		//Token        string `json:"token" dc:"登录Token"`
	}

	// 自定义上下文
	Context struct {
		User *UserAccountItem
		Data map[enums.ContextDataKey]interface{}
	}
)
