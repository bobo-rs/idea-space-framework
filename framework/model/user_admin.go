package model

import "gitee.com/bobo-rs/idea-space-framework/framework/model/entity"

type (
	// UserAdminGetListInput 获取管理员请求参数
	UserAdminGetListInput struct {
		CommonPaginationItem
		UserAdminWhereItem
	}

	// UserAdminGetListOutput 获取管理员列表输出结果
	UserAdminGetListOutput struct {
		CommonResponseItem
		Rows []UserAdminGetDetailItem `json:"rows" dc:"结果集"`
	}

	// UserAdminGetDetailOutput 获取管理员用户详情响应结果
	UserAdminGetDetailOutput struct {
		UserAdminGetDetailItem
		DepartId   []uint               `json:"depart_id" dc:"部门ID集合"`
		DepartList []DepartmentNameItem `json:"depart_list" dc:"部门列表"`
		PermId     []uint               `json:"perm_id" dc:"权限ID集合"`
	}

	// UserAdminAuthDetailOutput 管理员鉴权详情
	UserAdminAuthDetailOutput struct {
		UserAdminDetailItem
		Detail UserBasicDetailItem `json:"detail" dc:"用户详情"`
	}
)

type (
	// UserAdminDetailItem 用户管理员详情
	UserAdminDetailItem struct {
		Id            uint   `json:"id" dc:"管理员ID"`
		UserId        uint   `json:"user_id" dc:"用户ID"`
		ManageName    string `json:"manage_name" dc:"管理员名"`
		ManageNo      string `json:"manage_no" dc:"管理员编号"`
		IsSuperManage uint   `json:"is_super_manage" dc:"是否超管"`
		ManageStatus  uint   `json:"manage_status" dc:"管理员状态"`
		LogoffAt      string `json:"logoff_at" dc:"注销时间"`
	}

	// UserAdminSaveItem 保存管理员用户信息
	UserAdminSaveItem struct {
		entity.UserAdmin
		DepartId []uint `json:"depart_id" dc:"部门ID"`
		PermId   []uint `json:"perm_id" dc:"权限ID"`
	}

	// UserAdminGetDetailItem 获取用户管理员信息
	UserAdminGetDetailItem struct {
		UserAdminDetailItem
		UserDetail UserBasicDetailItem `json:"user_detail" dc:"用户详情"`
		UserAdminFmtItem
	}

	// UserAdminWhereItem 管理员搜索条件属性
	UserAdminWhereItem struct {
		ManageName    string `json:"manage_name" dc:"管理员名"`
		ManageNo      string `json:"manage_no" dc:"管理员编号"`
		IsSuperManage int    `json:"is_super_manage" dc:"是否超管"`
		ManageStatus  int    `json:"manage_status" dc:"管理员状态"`
	}

	// UserAdminFmtItem 用户管理员格式化属性
	UserAdminFmtItem struct {
		FmtManageStatus  string `json:"fmt_manage_status" dc:"状态"`
		FmtIsSuperManage string `json:"fmt_is_super_manage" dc:"超管"`
	}

	// UidByAidItem 通过管理员ID获取用户ID
	UidByAidItem struct {
		UserId uint `json:"user_id" dc:"用户ID"`
	}
)
