package model

import "gitee.com/bobo-rs/idea-space-framework/enums"

type (
	// PermCustomUpdateColumnInput 权限自定义更新字段请求参数
	PermCustomUpdateColumnInput struct {
		PermId  uint                          `json:"perm_id" dc:"权限ID"`
		Columns enums.PermCustomUpdateColumns `json:"columns" dc:"字段"`
		Value   interface{}                   `json:"value" dc:"更新值"`
	}
)

type (

	// PermissionsListTreeItem 权限列表菜单树形属性
	PermissionsListTreeItem struct {
		PermissionsListItem
		Children []PermissionsListTreeItem `json:"children" dc:"子级"`
	}

	// PermissionsMenuTreeIem 权限菜单树形属性
	PermissionsMenuTreeIem struct {
		PermissionsTreeItem
		Children []PermissionsMenuTreeIem `json:"children" dc:"子级"`
	}

	// PermissionsListItem 权限列表菜单属性
	PermissionsListItem struct {
		PermissionsTreeItem
		IsDisabled uint   `json:"is_disabled" dc:"是否禁用:0正常,1禁用"`
		PermCode   string `json:"perm_code" dc:"权限码"`
		Sort       uint   `json:"sort" dc:"排序"`
	}

	// PermissionsTreeItem 树形权限结构属性
	PermissionsTreeItem struct {
		Id       uint   `json:"id" dc:"权限ID"`
		Name     string `json:"name" dc:"权限名"`
		MenuType uint   `json:"menu_type" dc:"权限类型"`
		Path     string `json:"path" dc:"页面地址"`
		Pid      uint   `json:"pid" dc:"父级ID"`
		IsShow   uint   `json:"is_show" dc:"菜单是否显示:0显示,1隐藏"`
	}

	// PermissionsPathCodeItem 权限路由权限码属性
	PermissionsPathCodeItem struct {
		Id       uint   `json:"id" dc:"权限ID"`
		Pid      uint   `json:"pid" dc:"父级ID"`
		Path     string `json:"path" dc:"页面地址"`
		ApiPath  string `json:"api_path" dc:"api地址"`
		PermCode string `json:"perm_code" dc:"权限码"`
		Name     string `json:"name" dc:"权限名"`
	}

	// PermCodeActionItem 权限码操作属性
	PermCodeActionItem struct {
		ActionCode map[string]map[string]string `json:"action_code" dc:"页面操作权限码"`
		ActionApi  map[string]string            `json:"action_api" dc:"操作接口码"`
	}

	// ActionCodeItem 页面操作权限码属性
	ActionCodeItem struct {
		Path          string            // 地址
		ActionCodeMap map[string]string // 操作码MAP
	}
)
