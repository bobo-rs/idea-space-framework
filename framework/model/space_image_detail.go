package model

type (
	// SpaceImageDetailItem 设计空间图片详情属性
	SpaceImageDetailItem struct {
		SpaceId      uint   `json:"space_id"       dc:"设计空间图片ID"` // 设计空间图片ID
		TemplateId   uint   `json:"template_id"    dc:"模板ID"`     // 模板ID
		Synopsis     string `json:"synopsis"       dc:"图片简介"`     // 图片简介
		LabelValues  string `json:"label_values"   dc:"标签值"`      // 标签值
		FullCutWords string `json:"full_cut_words" dc:"完整切词卖点"`   // 完整切词卖点
		Creator      string `json:"creator"        dc:"创建人"`      // 创建人
	}
)
