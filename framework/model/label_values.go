package model

type (
	// LabelValuesSaveInput 标签值请求参数
	LabelValuesSaveInput struct {
		LabelId uint   `json:"label_id"`
		Values  string `json:"values"`
	}

	// LabelValuesSaveOutput 保存标签值响应输出
	LabelValuesSaveOutput struct {
		Id uint `json:"id" dc:"标签值ID"`
	}

	// LabelValuesGetListInput 获取标签列表请求参数
	LabelValuesGetListInput struct {
		LabelId uint `json:"label_id" dc:"标签ID"`
	}

	// LabelValuesGetListOutput 获取标签值列表数据
	LabelValuesGetListOutput struct {
		LabelValuesGetListItem
	}

	// LabelValuesGetListItem 获取标签值列表数据属性
	LabelValuesGetListItem struct {
		LabelId uint                   `json:"label_id" dc:"标签ID"`
		Values  string                 `json:"values" dc:"标签值集合"`
		Rows    []LabelValueDetailItem `json:"rows" dc:"标签列表"`
	}

	// LabelValueDelInput 删除标签值信息
	LabelValueDelInput struct {
		Hash string `json:"hash" dc:"HASH值（label_id+value）"`
	}

	// LabelValuesSaveListItem 批量保存标签值列表
	LabelValuesSaveListItem []LabelValueSaveItem

	// LabelValueSaveItem 标签值详情属性
	LabelValueSaveItem struct {
		LabelValueItem
		Hash string `json:"hash" dc:"HASH值（label_id+value）"`
		Id   uint   `json:"id" dc:"标签值ID"`
	}

	// LabelValueListItem 标签值列表属性
	LabelValueListItem struct {
		LabelValueItem
		Hash        string `json:"hash" dc:"HASH值（label_id+value）"`
		TemplateNum uint   `json:"template_num" dc:"模板数量"`
	}

	// LabelValueItem 标签值属性
	LabelValueItem struct {
		LabelId uint   `json:"label_id" dc:"标签ID"`
		Value   string `json:"value" dc:"标签值"`
	}

	// LabelValueDetailItem 标签值详情属性
	LabelValueDetailItem struct {
		Id      uint   `json:"id" dc:"标签值ID"`
		LabelId uint   `json:"label_id" dc:"标签ID"`
		Value   string `json:"value" dc:"标签值"`
		Hash    string `json:"hash" dc:"HASH值（label_id+value）"`
		IsDel   uint   `json:"is_del" dc:"是否删除：0否，1已删除"`
	}
)
